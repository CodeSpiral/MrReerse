"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const net = require("net");
const _AppConfig = require("./config");
const Address6 = require('ip-address').Address6;
const _LogDetail = false;
const _Log = _LogDetail || _AppConfig.Socket5Log();
let s5IsLink = false;
exports.s5TunnelIsOKLink = function () {
    return s5IsLink;
};
exports.s5TunnelStart = function (socketServerPort, connectDomain, connectPort, s5Port) {
    let clientNum = 0;
    const socketServer = net.createServer((client) => {
        let clientIndex = clientNum++;
        _LogDetail && _Log && console.log(`[${clientIndex}]${'[s5隧道][客户端]'.bgCyan}接入.`);
        s5IsLink = true;
        let s5;
        client.once('data', (clientData) => {
            _LogDetail && _Log && console.log(`[${clientIndex}]${'[s5隧道][客户端]'.bgCyan}连接s5服务器.`);
            s5 = net.createConnection(s5Port, '127.0.0.1', function () {
                socks5AuthenticateWith(s5, undefined, undefined, function (err) {
                    if (err) {
                        s5.emit('error', err);
                        return;
                    }
                    socks5ConnectHost(s5, connectDomain, connectPort, function (err) {
                        if (err) {
                            s5.emit('error', err);
                            return;
                        }
                        s5.pipe(client);
                        s5.write(clientData, function () {
                            client.pipe(s5);
                            _Log && console.log(`[${clientIndex}]` + `[s5服务器]`.bgYellow + `握手完成`.green);
                        });
                    });
                });
            }).on('error', function (err) {
                console.log(`[${clientIndex}]` + '[s5服务器]'.bgYellow + `错误：${err.message}`.red);
                if (!client.destroyed)
                    client.destroy();
            }).on('close', function (has_err) {
                s5IsLink = false;
            });
        }).on('end', () => {
        }).on('close', function (had_err) {
            if (_LogDetail || had_err) {
                console.log(`[${clientIndex}]${'[s5隧道][客户端]'.blue}关闭。 应错误而关闭：${had_err ? '是' : '否'}`);
            }
            if (!client.destroyed)
                client.destroy();
        });
    });
    socketServer.on('error', (err) => {
        console.log(`${'[s5隧道]'.bgCyan}错误 ${err.message}`);
    });
    socketServer.listen(socketServerPort, () => {
        if (_LogDetail) {
            console.log(`${'[s5隧道]'.bgCyan}监听：${socketServerPort}， 发到端口：${s5Port}`);
        }
        else {
            console.log(`${'[s5隧道]'.bgCyan}数据转发到端口：${s5Port}`);
        }
    });
    return socketServer;
};
function parseIPv4(host, request) {
    var i, ip, groups = host.split('.');
    for (i = 0; i < 4; i++) {
        ip = parseInt(groups[i], 10);
        request.push(ip);
    }
}
function parseString(string, request) {
    var buffer = Buffer.from(string), i, l = buffer.length;
    request.push(l);
    for (i = 0; i < l; i++) {
        request.push(buffer[i]);
    }
}
function parseIPv6(host, request) {
    var i, b1, b2, part1, part2, address, groups;
    address = new Address6(host).canonicalForm();
    if (!address) {
        return false;
    }
    groups = address.split(':');
    for (i = 0; i < groups.length; i++) {
        part1 = groups[i].substr(0, 2);
        part2 = groups[i].substr(2, 2);
        b1 = parseInt(part1, 16);
        b2 = parseInt(part2, 16);
        request.push(b1);
        request.push(b2);
    }
    return true;
}
function getErrorMessage(code) {
    switch (code) {
        case 1:
            return 'General SOCKS server failure';
        case 2:
            return 'Connection not allowed by ruleset';
        case 3:
            return 'Network unreachable';
        case 4:
            return 'Host unreachable';
        case 5:
            return 'Connection refused';
        case 6:
            return 'TTL expired';
        case 7:
            return 'Command not supported';
        case 8:
            return 'Address type not supported';
        default:
            return 'Unknown status code ' + code;
    }
}
function socks5AuthenticateWith(socket, socksUsername, socksPassword, cb) {
    var authMethods, buffer;
    socket.once('data', function (data) {
        var error, request, buffer, i, l;
        if (2 !== data.length) {
            error = 'Unexpected number of bytes received.';
        }
        else if (0x05 !== data[0]) {
            error = 'Unexpected SOCKS version number: ' + data[0] + '.';
        }
        else if (0xFF === data[1]) {
            error = 'No acceptable authentication methods were offered.';
        }
        else if (!authMethods.includes(data[1])) {
            error = 'Unexpected SOCKS authentication method: ' + data[1] + '.';
        }
        if (error) {
            cb(new Error('SOCKS authentication failed. ' + error));
            return;
        }
        if (0x02 === data[1]) {
            socket.once('data', function (data) {
                var error;
                if (2 !== data.length) {
                    error = 'Unexpected number of bytes received.';
                }
                else if (0x01 !== data[0]) {
                    error = 'Unexpected authentication method code: ' + data[0] + '.';
                }
                else if (0x00 !== data[1]) {
                    error = 'Username and password authentication failure: ' + data[1] + '.';
                }
                if (error) {
                    cb(new Error('SOCKS authentication failed. ' + error));
                }
                else {
                    cb();
                }
            });
            request = [0x01];
            parseString(socksUsername, request);
            parseString(socksPassword, request);
            socket.write(Buffer.from(request));
        }
        else {
            cb();
        }
    });
    authMethods = [0x00];
    if (socksUsername) {
        authMethods.push(0x02);
    }
    buffer = Buffer.alloc(2 + authMethods.length);
    buffer[0] = 0x05;
    buffer[1] = authMethods.length;
    authMethods.forEach(function (authMethod, i) {
        buffer[2 + i] = authMethod;
    });
    socket.write(buffer);
}
function socks5ConnectHost(socket, host, port, cb) {
    var request, buffer;
    socket.once('data', function (data) {
        var error;
        if (data[0] !== 0x05) {
            error = 'Unexpected SOCKS version number: ' + data[0] + '.';
        }
        else if (data[1] !== 0x00) {
            error = getErrorMessage(data[1]) + '.';
        }
        else if (data[2] !== 0x00) {
            error = 'The reserved byte must be 0x00.';
        }
        if (error) {
            cb(new Error('SOCKS connection failed. ' + error));
            return;
        }
        cb();
    });
    request = [];
    request.push(0x05);
    request.push(0x01);
    request.push(0x00);
    switch (net.isIP(host)) {
        case 0:
            request.push(0x03);
            parseString(host, request);
            break;
        case 4:
            request.push(0x01);
            parseIPv4(host, request);
            break;
        case 6:
            request.push(0x04);
            if (parseIPv6(host, request) === false) {
                cb(new Error('IPv6 host parsing failed. Invalid address.'));
                return;
            }
            break;
    }
    request.length += 2;
    buffer = Buffer.from(request);
    buffer.writeUInt16BE(port, buffer.length - 2, true);
    socket.write(buffer);
}
