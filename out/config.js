"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Path = require("path");
const _Fs = require("fs");
const _Colors = require("colors");
const _Readline = require("readline");
const _ConfigFile = 'config.json';
const _ConfigPath = _Path.join(__dirname, '..');
const _IsDev = (process.argv[2] === 'dev') || (__dirname.lastIndexOf('app') === (__dirname.length - 3));
if (_IsDev)
    console.log(_Colors.bgCyan.blue(' 开发模式'));
let _C_S5Port = 0;
let _Dev_1 = false;
let _Dev_2 = false;
let _Dev_3 = false;
let _Dev_3_0 = false;
exports.Dev_1 = () => { return _Dev_1; };
exports.Dev_2 = () => { return _Dev_2; };
if (_IsDev) {
    let cmds = [];
    for (let index = 2; index < process.argv.length; index++) {
        const cmd = process.argv[index];
        if (cmd == 'dev') { }
        else {
            cmds.push(cmd);
        }
    }
    if (cmds && cmds.length) {
        if (cmds[0] && !isNaN(cmds[0])) {
            _C_S5Port = parseInt(cmds[0]);
            cmds.shift();
        }
    }
    for (let index = 0; index < cmds.length; index++) {
        const cmd = cmds[index];
        switch (cmd) {
            case 'dev-1':
                _Dev_1 = true;
                console.log(`DEV开启：`.bgRed.yellow, cmd);
                break;
            case 'dev-2':
                _Dev_2 = true;
                console.log(`DEV开启：`.bgRed.yellow, cmd);
                break;
            case 'dev-3':
                _Dev_3 = true;
                console.log(`DEV开启：`.bgRed.yellow, cmd);
                break;
            case 'dev-3-0':
                _Dev_3_0 = true;
                console.log(`DEV开启：`.bgRed.yellow, cmd);
                break;
            default:
                console.log('MISS DEV:'.bgRed.yellow, cmd);
                break;
        }
    }
}
let _Config = {
    ReerseServer: true,
    WebServer: true,
    CertFile: 'YuanHuanJiLu-pri',
    ReerseServerPort: 443,
    UseSocket5: false,
    Socket5Port: 8889,
    Socket5Log: true,
    MultiUserMode: false,
    ServerLog: false,
    NetworkAddressLog: true,
    ConfigLog: true,
    WebPort: 10010,
    SendToCollectionServer: '',
    CollectionServerPort: 18088,
    IsOnlyDoCollectionSend: false,
    CacheDownloadPack: false,
    TranslateAdvFile: false,
    SaveAllApiRequset: false,
    SaveAllApiResponse: false,
    PlayerCountry: false,
    SaveQuestBattle: false,
    SaveQuestBattleAutoSaveOne: true,
    SaveBattleResult: false,
    SaveBattleResultRequest: false,
    ArenaBattleInfoHelper: false,
    WebMemoriaImgResponse: true,
    ConsoleCmd: true,
};
if (_Fs.existsSync(_Path.join(_ConfigPath, _ConfigFile))) {
    const c = ReadJsonFileSync(_ConfigPath, _ConfigFile);
    if (c) {
        if (c.ReerseServer !== undefined)
            _Config.ReerseServer = (c.ReerseServer ? true : false);
        if (c.WebServer !== undefined)
            _Config.WebServer = (c.WebServer ? true : false);
        if (c.CertFile)
            _Config.CertFile = c.CertFile;
        if (c.ReerseServerPort !== undefined)
            _Config.ReerseServerPort = c.ReerseServerPort;
        if (c.UseSocket5 !== undefined)
            _Config.UseSocket5 = (c.UseSocket5 ? true : false);
        if (c.Socket5Port !== undefined)
            _Config.Socket5Port = c.Socket5Port;
        if (c.Socket5Log !== undefined)
            _Config.Socket5Log = (c.Socket5Log ? true : false);
        if (c.MultiUserMode !== undefined)
            _Config.MultiUserMode = (c.MultiUserMode ? true : false);
        if (c.ServerLog !== undefined)
            _Config.ServerLog = (c.ServerLog ? true : false);
        if (c.NetworkAddressLog !== undefined)
            _Config.NetworkAddressLog = (c.NetworkAddressLog ? true : false);
        if (c.ConfigLog !== undefined)
            _Config.ConfigLog = (c.ConfigLog ? true : false);
        if (c.WebPort !== undefined)
            _Config.WebPort = c.WebPort;
        if (c.SendToCollectionServer !== undefined)
            _Config.SendToCollectionServer = c.SendToCollectionServer;
        if (c.CollectionServerPort !== undefined)
            _Config.CollectionServerPort = c.CollectionServerPort;
        if (c.IsOnlyDoCollectionSend !== undefined)
            _Config.IsOnlyDoCollectionSend = (c.IsOnlyDoCollectionSend ? true : false);
        if (c.CacheDownloadPack !== undefined)
            _Config.CacheDownloadPack = (c.CacheDownloadPack ? true : false);
        if (c.TranslateAdvFile !== undefined)
            _Config.TranslateAdvFile = (c.TranslateAdvFile ? true : false);
        if (c.SaveAllApiRequset !== undefined)
            _Config.SaveAllApiRequset = (c.SaveAllApiRequset ? true : false);
        if (c.SaveAllApiResponse !== undefined)
            _Config.SaveAllApiResponse = (c.SaveAllApiResponse ? true : false);
        if (c.PlayerCountry !== undefined)
            _Config.PlayerCountry = (c.PlayerCountry ? true : false);
        if (c.SaveQuestBattle !== undefined)
            _Config.SaveQuestBattle = (c.SaveQuestBattle ? true : false);
        if (c.SaveQuestBattleAutoSaveOne !== undefined)
            _Config.SaveQuestBattleAutoSaveOne = (c.SaveQuestBattleAutoSaveOne ? true : false);
        if (c.SaveBattleResult !== undefined)
            _Config.SaveBattleResult = (c.SaveBattleResult ? true : false);
        if (c.SaveBattleResultRequest !== undefined)
            _Config.SaveBattleResultRequest = (c.SaveBattleResultRequest ? true : false);
        if (c.ArenaBattleInfoHelper !== undefined)
            _Config.ArenaBattleInfoHelper = (c.ArenaBattleInfoHelper ? true : false);
        if (c.WebMemoriaImgResponse !== undefined)
            _Config.WebMemoriaImgResponse = (c.WebMemoriaImgResponse ? true : false);
        if (c.ConsoleCmd !== undefined)
            _Config.ConsoleCmd = (c.ConsoleCmd ? true : false);
    }
}
else {
    WriteFileJson(_ConfigPath, _ConfigFile, _Config, false);
}
if (_C_S5Port) {
    _Config.UseSocket5 = true;
    _Config.Socket5Port = _C_S5Port;
    _Config.ReerseServer = true;
}
if (_Config.ConfigLog) {
    console.log('配置: '.cyan, _Config);
}
if (_Config.SendToCollectionServer) {
    if (_Dev_3) {
        _Config.SendToCollectionServer = '127.0.0.1';
    }
    if (_Dev_3_0) {
        _Config.SendToCollectionServer = '';
    }
    if (_Config.SendToCollectionServer) {
        console.log(`数据收集服务器: ${_Config.SendToCollectionServer}:${_Config.CollectionServerPort}`.bgBlue.yellow);
        setTimeout(function () {
            require('./sendCollection').Send_HI();
        }, 500);
    }
}
let _cert = _Path.join(__dirname, 'crt', (_Config.CertFile + '.crt'));
let _certKey = _Path.join(__dirname, 'crt', (_Config.CertFile + '.key'));
if (_Config.ReerseServer) {
    if (_Fs.existsSync(_cert) == false)
        console.log(`证书: Crt => ${_cert} 文件不存在！`.red.bgYellow);
    if (_Fs.existsSync(_certKey) == false)
        console.log(`证书: Key => ${_certKey} 文件不存在！`.red.bgYellow);
}
exports.IsDev = () => { return _IsDev; };
exports.ReerseServer = () => { return _Config.ReerseServer; };
exports.WebServer = () => { return _Config.WebServer; };
exports.Cert = () => { return _cert; };
exports.CertKey = () => { return _certKey; };
exports.ReerseServerPort = () => { return _Config.ReerseServerPort; };
exports.UseSocket5 = () => { return _Config.UseSocket5; };
exports.Socket5Port = () => { return _Config.Socket5Port; };
exports.Socket5Log = () => { return _Config.Socket5Log; };
exports.MultiUserMode = () => { return _Config.MultiUserMode; };
exports.ServerLog = () => { return _Config.ServerLog; };
exports.NetworkAddressLog = () => { return _Config.NetworkAddressLog; };
exports.ConfigLog = () => { return _Config.ConfigLog; };
exports.WebPort = () => { return _Config.WebPort; };
exports.SendToCollectionServer = () => { return _Config.SendToCollectionServer; };
exports.CollectionServerPort = () => { return _Config.CollectionServerPort; };
exports.IsOnlyDoCollectionSend = () => { return _Config.IsOnlyDoCollectionSend; };
exports.CacheDownloadPack = () => { return _Config.CacheDownloadPack; };
exports.TranslateAdvFile = () => { return _Config.TranslateAdvFile; };
exports.SaveAllApiRequset = () => { return _Config.SaveAllApiRequset; };
exports.SaveAllApiResponse = () => { return _Config.SaveAllApiResponse; };
exports.PlayerCountry = () => { return _Config.PlayerCountry; };
exports.SaveQuestBattle = () => { return _Config.SaveQuestBattle; };
exports.SaveQuestBattleAutoSaveOne = () => { return _Config.SaveQuestBattleAutoSaveOne; };
exports.SaveBattleResult = () => { return _Config.SaveBattleResult; };
exports.SaveBattleResultRequest = () => { return _Config.SaveBattleResultRequest; };
exports.ArenaBattleInfoHelper = () => { return _Config.ArenaBattleInfoHelper; };
exports.WebMemoriaImgResponse = () => { return _Config.WebMemoriaImgResponse; };
const _BaseHtml = _Path.join(__dirname, (_IsDev ? '../../_MS3/lWeb/' : '../lWeb/'));
const _BaseDevBase = _IsDev ? '../../' : '../';
const _Base = _Path.join(__dirname, _BaseDevBase);
const _BaseMagicaExtend = _Path.join(__dirname, _BaseDevBase, '/magicaExtend/');
const _BaseMagica = _Path.join(__dirname, _BaseDevBase, '/magica/');
exports.BasePath_Html = () => { return _BaseHtml; };
exports.BasePath_Base = () => { return _Base; };
exports.BasePath_MagicaExtend = () => { return _BaseMagicaExtend; };
exports.BasePath_Magica = () => { return _BaseMagica; };
if (_Config.IsOnlyDoCollectionSend) {
    console.log(`启用：只进行收集发送处理（会使其他部分配置无效化）`.blue);
    if (!_Config.SendToCollectionServer) {
        console.log(`但是并没有设置发送服务器！`.yellow);
    }
}
else {
    console.log(`文件根位置：${_Base}`);
}
function ReadJsonFileSync(dir, fileName) {
    let file = _Path.join(dir, fileName);
    if (_Fs.existsSync(file)) {
        try {
            let txt = _Fs.readFileSync(file, { encoding: 'utf8' });
            return JSON.parse(txt);
        }
        catch (error) {
            console.warn(`[读]JSON失败！: ${file}`.red, error);
            return undefined;
        }
    }
    return undefined;
}
function WriteFileJson(dir, fileName, obj, log) {
    WriteFile(dir, fileName, JSON.stringify(obj, null, 4), log);
}
function WriteFile(dir, fileName, data, log) {
    try {
        let file = _Path.join(dir, fileName);
        let ws = _Fs.createWriteStream(file).on('error', function (err) {
            console.warn('[写][Error]:'.red, err);
        }).end(data, function () {
            if (log)
                console.log('[写]: ' + file);
        });
    }
    catch (error) {
        console.warn('[写][Error]:'.red, error);
    }
}
if (_Config.ConsoleCmd) {
    const _Cmds = [
        {
            cmd: ['h', 'help'],
            txt: '显示Cmd命令信息。', func: CmdHelp,
        },
        {
            cmd: ['c', 'config'],
            txt: '当前配置信息。', func: () => {
                console.log('配置: '.cyan, _Config);
            },
        },
        {
            cmd: ['a1', 'DownloadPackMerge'],
            txt: '数据包文件整合', func: () => {
                require('./mgo').mgoDownloadPackMerge();
            },
        },
        {
            cmd: ['a2', 'DownloadPack'],
            txt: '下载数据包', func: () => {
                require('./mPack').mPackGetDownloadPack();
            },
        },
        {
            cmd: ['a3', 'DownloadPackCVer'],
            txt: '下载数据包（检测版本）', func: () => {
                require('./mPack').mPackGetDownloadPack(true);
            },
        },
        {
            cmd: ['s1a', 'SaveQuestBattle'],
            txt: '临时改变配置：SaveQuestBattle（记录战斗数据）', func: () => {
                _Config.SaveQuestBattle = !_Config.SaveQuestBattle;
                console.log(`  SaveQuestBattle:`, _Config.SaveQuestBattle);
            },
        },
        {
            cmd: ['s1', 'SaveQuestBattleAutoSaveOne'],
            txt: '临时改变配置：SaveQuestBattleAutoSaveOne （记录战斗数据，仅第一次）', func: () => {
                _Config.SaveQuestBattleAutoSaveOne = !_Config.SaveQuestBattleAutoSaveOne;
                console.log(`  SaveQuestBattleAutoSaveOne:`, _Config.SaveQuestBattleAutoSaveOne);
            },
        },
        {
            cmd: ['s3', 'sres', 'SaveAllApiResponse'],
            txt: '临时改变配置： SaveAllApiResponse （记录响应/返回数据）', func: () => {
                console.log(`  SaveAllApiResponse:`, (_Config.SaveAllApiResponse = !_Config.SaveAllApiResponse));
            },
        },
        {
            cmd: ['s4', 'sreq', 'SaveAllApiRequset'],
            txt: '临时改变配置： SaveAllApiRequset （记录请求数据）', func: () => {
                console.log(`  SaveAllApiRequset:`, (_Config.SaveAllApiRequset = !_Config.SaveAllApiRequset));
            },
        },
    ];
    function CmdHelp() {
        console.log('Cmd（区分大小写）:');
        _Cmds.forEach(function (item) {
            if (typeof (item.cmd) === 'string') {
                console.log(`  输入[ ${item.cmd} ]: ${item.txt}`);
            }
            else if (item.cmd instanceof Array) {
                console.log(`  输入[ ${item.cmd.join(', ')} ]: ${item.txt}`);
            }
        }, this);
    }
    const _ReadLineRun = _Readline.createInterface(process.stdin, process.stdout);
    _ReadLineRun.setPrompt('');
    _ReadLineRun.prompt();
    _ReadLineRun.on('line', CmdIn);
    _ReadLineRun.on('close', function () {
        console.log('程序关闭!');
        process.exit(0);
    });
    function CmdIn(line) {
        var inCmd = line.trim();
        if (!inCmd)
            return;
        let isNullCmd = true;
        for (var index = 0; index < _Cmds.length; index++) {
            var item = _Cmds[index];
            var cmd = item.cmd;
            var isThis = false;
            if (typeof (cmd) === 'string')
                isThis = (inCmd == cmd);
            else if (cmd instanceof Array)
                isThis = (cmd.indexOf(inCmd) >= 0);
            if (isThis) {
                isNullCmd = false;
                console.log(`> 运行：[ ${inCmd} ]=> ${item.txt}`);
                setTimeout(() => { item.func(); console.log(); }, 1);
                break;
            }
        }
        if (isNullCmd)
            console.log('无该命令。');
        setTimeout(() => { _ReadLineRun.prompt(); }, 1);
    }
}
