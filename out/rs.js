"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Http2_Experimental = require("http2");
const { HTTP2_HEADER_METHOD, HTTP2_HEADER_PATH, HTTP2_HEADER_STATUS, HTTP2_HEADER_CONTENT_TYPE, HTTP2_HEADER_AUTHORITY } = _Http2_Experimental.constants;
const _Fs = require("fs");
const _Colors = require("colors");
const _Zlib = require("zlib");
const _S5Tunnel = require("./s5Tunnel");
const _Mgo = require("./mgo");
const _AppConfig = require("./config");
const _UseS5 = _AppConfig.UseSocket5();
const _S5Port = _AppConfig.Socket5Port();
const _TunnelHost = 8443;
const _ServerUrlLog = _AppConfig.ServerLog();
const _IsDev = _AppConfig.IsDev();
const _LogDetail = false;
const _Port = _AppConfig.ReerseServerPort() || 443;
const _Protocol = 'https:';
const _Domain = 'android.magi-reco.com';
const _ProtocolHostname = `${_Protocol}//${_Domain}`;
const _C_CacheDownloadPack = _AppConfig.CacheDownloadPack();
const _C_LocalAdvFile = _AppConfig.TranslateAdvFile();
const _C_MultiUserMode = _AppConfig.MultiUserMode();
const _Server = _Http2_Experimental.createSecureServer({
    cert: _Fs.readFileSync(_AppConfig.Cert(), 'utf8'),
    key: _Fs.readFileSync(_AppConfig.CertKey(), 'utf8'),
});
if (_AppConfig.ReerseServer()) {
    let serverCountNum = 0;
    _Server.on('error', (err) => {
        console.error('[反代理]error'.red, err);
    }).on('socketError', (err) => {
        console.error('[反代理]socketError'.red, err);
    });
    _Server.on('stream', serverOnStream);
    _Server.listen(_Port, (err) => {
        if (err) {
            console.error(_Colors.red(err));
            return process.exit(1);
        }
        else {
            console.log(`监听 https://0.0.0.0:${_Port} (${_Domain})`.bgYellow.magenta);
        }
    });
    function serverOnStream(stream, headers) {
        const nowDoN = (++serverCountNum);
        const method = headers[HTTP2_HEADER_METHOD];
        const path = headers[HTTP2_HEADER_PATH];
        const authority = headers[HTTP2_HEADER_AUTHORITY];
        const url = `${_Protocol}//${authority}${path}`;
        if (!path || path === '/' || authority != 'android.magi-reco.com') {
            stream.end();
            console.log(`-> [${nowDoN}][${method}]不工作: ${url}`.red.bgYellow);
            return;
        }
        let dataBuf;
        let dataStr;
        stream.on('data', (chunk) => {
            if (typeof chunk === 'string') {
                dataStr = dataStr ? (dataStr + chunk) : chunk;
            }
            else {
                dataBuf = Buffer.concat([(dataBuf ? dataBuf : Buffer.alloc(0)), chunk]);
            }
        }).on('end', () => {
            let reqData = (dataBuf || dataStr);
            const reqDataLen = reqData ? reqData.length : 0;
            if (_ServerUrlLog) {
                if (method == 'GET') {
                    if (reqDataLen)
                        console.log(`-> [${nowDoN}][${method}][${reqDataLen}] ${url}`.green);
                    else
                        console.log(`-> [${nowDoN}][${method}] ${url}`.green);
                }
                else {
                    console.log(`-> [${nowDoN}][${method}][${reqDataLen.toString().bold}] ${url}`.red);
                }
            }
            if (authority.indexOf('192.168') >= 0 || authority.indexOf('127.0') >= 0) {
                console.log(`\r\n[ERROR]stop. \r\n`.red.bgWhite);
                stream.end();
                return;
            }
            if (_C_MultiUserMode && path === '/magica/index.html') {
                ClientHttp2Session_setUndefined();
            }
            let doIndexInJsMode = !!(_AppConfig.Dev_2());
            if (doIndexInJsMode && path === '/magica/api/test/logger/error') {
                stream.respond({
                    'content-type': 'application/json;charset=UTF-8',
                    'pragma': 'no-cache',
                    'cache-control': 'no-cache',
                });
                stream.end('{"resultCode":"success"}');
                console.log(`<- [${nowDoN}][拦截请求/直接返回][logger/error].`.red.bgYellow);
                if (reqData) {
                    console.log(reqData.toString());
                    console.log();
                }
                return;
            }
            if (_AppConfig.Dev_1()) {
                let buff = require('./mrsTestAdv').mrsInHtml(path, headers);
                if (buff) {
                    if (buff instanceof Buffer || typeof buff === 'string' || buff instanceof String) {
                        stream.respond({
                            'content-type': 'text/html',
                            'cache-control': 'no-cache',
                            'content-encoding': 'gzip',
                        });
                    }
                    else {
                        if (buff.header) {
                            let header = buff.header;
                            stream.respond(header);
                            let rBuff = buff.body;
                            if (rBuff)
                                rBuff = _Zlib.gzipSync(rBuff);
                            if (header[':status'] == 200 && rBuff) {
                                stream.end(rBuff, function () {
                                    console.log(`<- [${nowDoN}][直接返回]:[len:${rBuff.length}].`.blue);
                                });
                            }
                            else {
                                console.log(`<- [${nowDoN}][直接返回]:null.`.blue, header);
                                stream.end();
                            }
                            return;
                        }
                        else {
                            stream.respond({
                                'content-type': buff['content-type'] || 'text/html',
                                'cache-control': 'no-cache',
                                'content-encoding': 'gzip',
                            });
                        }
                        buff = buff.body;
                        if (buff == undefined) {
                            console.log('[buff.body]==undefined'.magenta.bgYellow);
                        }
                    }
                    let rBuff = ZlibGzipSync('gzip', buff);
                    stream.end(rBuff, function () {
                        console.log(`<- [${nowDoN}][直接返回]:[len:${rBuff.length}].`.blue);
                        buff = undefined;
                        rBuff = undefined;
                    });
                }
                else {
                    stream.respond({
                        ':status': 501,
                        'content-type': 'application/json;charset=UTF-8',
                        'pragma': 'no-cache',
                        'cache-control': 'no-cache',
                    });
                    stream.end('{}');
                    console.log(`<- [${nowDoN}][直接返回]:拦截.`.red.bgYellow);
                }
                return;
            }
            if (_C_CacheDownloadPack) {
                let buff = _Mgo.mgoDownloadPackGetLFlie(url);
                if (buff && buff instanceof Buffer) {
                    stream.respond({
                        'content-length': buff.length,
                        'accept-ranges': 'bytes'
                    });
                    stream.end(buff, function () {
                        console.log(`<- [${nowDoN}][直接返回缓存数据包]:[len:${buff.length}].`.cyan);
                        buff = undefined;
                    });
                    return;
                }
            }
            if (_AppConfig.IsOnlyDoCollectionSend() == false) {
                if (reqData)
                    _Mgo.mgoDoSomethingOnRequest(url, reqData);
            }
            let useLocalFile = false;
            let oldH2Client;
            let resHeaders;
            let resConLen;
            let resZlibType;
            let resHeadersObj;
            let doIndexInJs = false;
            httpsRequest_Experimental(_ProtocolHostname, path, method, headers, (dataBuf || dataStr), (resHeaders, statusCode) => {
                resHeadersObj = Object.assign({}, resHeaders);
                let outResHeaders = {};
                for (const name in resHeaders) {
                    if (name === 'content-length') {
                        if (_LogDetail)
                            console.log(`delete header: ${name}: ${resHeaders[name]}`);
                    }
                    else {
                        outResHeaders[name] = resHeaders[name];
                    }
                }
                if (stream.destroyed) {
                    console.log(`<-+ [${nowDoN}][${method}][stream.destroyed]客户端已关闭`.red.bgYellow);
                }
                else {
                    stream.respond(outResHeaders);
                }
                resConLen = parseInt(resHeaders['content-length']);
                resZlibType = resHeaders['content-encoding'] || '';
                if (statusCode == 200) {
                    if (_LogDetail && _ServerUrlLog) {
                        var contentType = resHeaders['content-type'] || '';
                        console.log(`<-+ [${nowDoN}][${statusCode}]${contentType}`.blue);
                    }
                }
                else {
                    var contentType = resHeaders['content-type'] || '';
                    if (statusCode == 304) {
                    }
                    else {
                        console.log(`<-+ [${nowDoN}][${statusCode}]${contentType}`.red.bgYellow);
                    }
                }
                if (doIndexInJsMode && path === '/magica/index.html') {
                    console.log('/magica/index.html');
                    doIndexInJs = true;
                }
            }, (rSrcData) => {
                if (doIndexInJsMode && doIndexInJs)
                    return;
                if (useLocalFile || stream.destroyed)
                    return;
                if (stream.writable)
                    stream.write(rSrcData);
            }, (rBody, zlibOk, bodyIsComplete, statusCode, bodyLen) => {
                if (_IsDev && doIndexInJsMode && doIndexInJs && statusCode == 200) {
                    doIndexInJs = false;
                    let zlibType = resHeadersObj['content-encoding'] || '';
                    if (path === '/magica/index.html') {
                        let jst = require('./mrsTest').mrsIndexHtmlJsInjection();
                        if (jst)
                            rBody = rBody.toString().replace('</body>', jst + '</body>');
                    }
                    let rDataBuf = rBody;
                    try {
                        switch (zlibType) {
                            case 'gzip':
                                rDataBuf = _Zlib.gzipSync(rBody);
                                break;
                            case 'deflate':
                                rDataBuf = _Zlib.deflateSync(rBody);
                                break;
                            default: rDataBuf = rBody;
                        }
                    }
                    catch (err) {
                        console.log(`<-+ [${nowDoN}][zlib][压缩]Error: ${err.message}`.cyan);
                    }
                    try {
                        stream.write(rDataBuf, function () {
                            console.log(`<-+ [${nowDoN}][响应]`.cyan, arguments);
                        });
                    }
                    catch (err) {
                        console.log(`<-+ [${nowDoN}][响应][error]: ${err.message}`.cyan);
                    }
                }
                let bodyLenStr = bodyLen;
                if (bodyLen > (1024 * 1024))
                    bodyLenStr = (bodyLen / (1024 * 1024)).toFixed(3) + 'MB';
                else if (bodyLen > 1024)
                    bodyLenStr = (bodyLen / 1024).toFixed(3) + 'KB';
                console.log(`<-+ [${nowDoN}][${statusCode}]`.yellow
                    + (resZlibType ? resZlibType + `(${zlibOk}) ` : '').yellow
                    + `Len:${bodyLenStr} ${(resConLen ? `(${bodyIsComplete})` : '?')}. Over`.yellow);
                if (!stream.aborted && !stream.destroyed && stream.writable)
                    stream.end();
                if (rBody) {
                    if (zlibOk && bodyIsComplete) {
                        _Mgo.mgoAutoSaveFile(url, resHeadersObj, rBody);
                        let body = (typeof rBody === 'string' ? rBody : rBody.toString());
                        _Mgo.mgoDoSomething(url, body);
                    }
                }
                if (_C_CacheDownloadPack)
                    _Mgo.mgoDownloadPack_AssetCache(url, rBody, statusCode);
            }, nowDoN);
        }).on('error', (err) => {
            console.error('[反代理]stream error'.red.bgYellow);
            console.error(err);
            ClientHttp2Session_setUndefined();
        });
    }
}
else {
    console.log(`不运行监听：${_Port}`.green);
}
let _Http2_Experimental_ClientHttp2Session;
function ClientHttp2Session_setUndefined() {
    if (_Http2_Experimental_ClientHttp2Session !== undefined) {
        let old = _Http2_Experimental_ClientHttp2Session;
        _Http2_Experimental_ClientHttp2Session = undefined;
        (typeof old.close === 'function') && old.close();
    }
}
function httpsRequest_Experimental(protocolHostname, path, method, headers, reqdata, cbFirst, cbOnData, cbOver, nowDoN) {
    const url = `${protocolHostname}${path}`;
    const opts = {
        rejectUnauthorized: true,
    };
    if (_UseS5) {
        opts.host = '127.0.0.1';
        opts.port = _TunnelHost;
        opts.rejectUnauthorized = false;
    }
    let clientHttp2Session;
    if (_C_MultiUserMode == false) {
        clientHttp2Session = _Http2_Experimental_ClientHttp2Session;
    }
    if (clientHttp2Session == undefined || clientHttp2Session.destroyed === true) {
        clientHttp2Session = _Http2_Experimental.connect(protocolHostname, opts);
        clientHttp2Session.on('error', (err) => {
            console.error(`[Client-Http2-Session]error: `.red, err);
            _C_MultiUserMode && ClientHttp2Session_setUndefined();
        });
        if (_C_MultiUserMode == false)
            _Http2_Experimental_ClientHttp2Session = clientHttp2Session;
    }
    let resConLen;
    let zlibType;
    let statusCode = headers[HTTP2_HEADER_STATUS];
    const req = clientHttp2Session.request(headers);
    req.on('response', (resHeaders, flags) => {
        statusCode = resHeaders[HTTP2_HEADER_STATUS];
        resConLen = resHeaders['content-length'];
        zlibType = resHeaders['content-encoding'] || '';
        cbFirst(resHeaders, statusCode);
    });
    let dataBuf;
    let dataStr;
    req.on('data', (chunk) => {
        cbOnData(chunk);
        if (typeof chunk === 'string') {
            dataStr = dataStr ? (dataStr + chunk) : chunk;
        }
        else {
            dataBuf = Buffer.concat([(dataBuf ? dataBuf : Buffer.alloc(0)), chunk]);
        }
    }).on('end', () => {
        let zlibOk = false;
        let bodyLen;
        if (dataBuf) {
            bodyLen = dataBuf.length;
            try {
                switch (zlibType) {
                    case 'gzip':
                        dataBuf = _Zlib.gunzipSync(dataBuf);
                        break;
                    case 'deflate':
                        dataBuf = _Zlib.inflateSync(dataBuf);
                        break;
                }
                zlibOk = true;
            }
            catch (err) {
                console.log(`<-| [${nowDoN}][zlib]Error: ${err.message}`.cyan);
            }
        }
        else {
            zlibOk = true;
            bodyLen = dataStr ? dataStr.length : '?';
        }
        let bodyIsComplete = resConLen ? (resConLen == bodyLen) : true;
        try {
            cbOver((dataBuf || dataStr), zlibOk, bodyIsComplete, statusCode, bodyLen);
            if (_C_MultiUserMode) {
                try {
                    clientHttp2Session && (typeof clientHttp2Session.close === 'function') && clientHttp2Session.close();
                }
                catch (error) {
                    console.log(`[clientHttp2Session.close]ERROR`, error);
                }
            }
        }
        catch (err) {
            console.log(`<-| [${nowDoN}][error]fnnc cbOver: ${err.message}`.magenta);
        }
    }).on('error', (e) => {
        ClientHttp2Session_setUndefined();
        console.log(`<-| [${nowDoN}]客户端请求[error]`.red.bgYellow, e);
    });
    if (reqdata || method != 'GET')
        req.end(reqdata);
    return req;
}
if (_UseS5 && _AppConfig.ReerseServer()) {
    _S5Tunnel.s5TunnelStart(_TunnelHost, _Domain, 443, _S5Port);
}
function ZlibGzipSync(zlibType, rBody) {
    switch (zlibType) {
        case 'gzip':
            return _Zlib.gzipSync(rBody);
        case 'deflate':
            return _Zlib.deflateSync(rBody);
    }
    return rBody;
}
function httpUtilIsHtml(res) {
    var contentType = res.headers['content-type'];
    return (typeof contentType != 'undefined') && /text\/html|application\/xhtml\+xml/.test(contentType);
}
function httpUtilIsJson(res) {
    var contentType = res.headers['content-type'];
    return (typeof contentType != 'undefined') && (contentType.indexOf('application/json') >= 0);
}
