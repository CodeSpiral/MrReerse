# 收集配置


## 掉落&关卡
```
# 匹配URL
/magica/api/quest/native/result/send

# 过滤配置
{
    gameUser: { inviteCode: '*' },
    userQuestBattleResultList: [
        {
            questBattleId: '*',
            battleType: re.compile("^(?!^ARENA$).*$"), # 除ARENA之外
            questBattleStatus: re.compile("^SUCCESSFUL$"), # 仅SUCCESSFUL
            dropRewardCodes: '*',
        }
    ],
    userSectionList: [
        { section : '*' }
    ],
    userQuestBattleList: [
        { questBattle : '*' }
    ],
}
```


## 关卡
```
# 匹配URL
/magica/api/page/[\w\d/]+\?value=[\w\d,]*(userSectionList|userQuestBattleList)
或
/magica/api/page/EventBranchTop

# 过滤配置
{
    userSectionList: [
        { section : '*' }
    ],
    userQuestBattleList: [
        { questBattle : '*' }
    ],
    userEventBranchPointList: [
        {
            "point": {
                "eventId": '*',
                "questBattleId": '*',
                "title": '*',
            },
        }
    ],
}
```


## 战斗数据
```
/magica/api/quest/native/get

{
    battleType: re.compile("^(?!^ARENA$).*$"), # 除ARENA之外
    scenario: '*',
    waveList: '*',
    artList: '*',
    memoriaList: '*',
    magiaList: '*',
    connectList: '*',
    doppelList: '*',
    webData: {
        resultCode: re.compile("^success$"), # 仅success
        gameUser: { inviteCode: '*' },
        userQuestBattleResultList: [
            { questBattleId: '*', }
        ]
    }
}
```


## 记忆&素材
```
/magica/api/page/TopPage\?value=[\w\d,]*pieceList

{
    pieceList: '*',
    giftList: '*',
}
```


## 角色
```
/magica/api/page/MyPage\?value=[\w\d,]*(userCharaList|userCardList|userPieceList)

{
    userPieceList: [
        { piece: '*' }
    ],
    userCharaList: [
        { chara: '*' }
    ],
    userCardList: [
        { card: '*' }
    ],
}
```


## 角色&关卡
```
/magica/api/page/CharaCollection

{
    "charaList": [
        {
            "chara": '*',
            "cardList": [
                { "card": '*', }
            ]
        }
    ],
    userSectionList: [
        { section : '*' }
    ],
}
```


## 抽卡
```
/magica/api/gacha/draw

{
    resultCode: "*",
    gameUser: { inviteCode: '*' },
    gachaAnimation: '*',
    userPieceList: [
        { piece: '*' }
    ],
    userCharaList: [
        { chara: '*' }
    ],
    userCardList: [
        { card: '*' }
    ],
}
```


## 记忆
```
/magica/api/page/PieceCollection

{
    userPieceCollectionList: [
        { piece: '*' }
    ]
}
```


## 支援
```
/magica/api/page/SupportSelect

{
    supportUserList: [
        {
            userCardList: [
                { card: '*' }
            ],
            userCharaList: [
                { chara: '*' }
            ],
            userPieceList: [
                { piece: '*' }
            ],
            inviteCode: "*",
        }
    ]
}
```


## 商店
```
/magica/api/page/ShopTop

{
    shopList: [
        {
            shopItemList: [
                {
                    card: '*',
                    chara: '*',
                    piece: '*',
                }
            ]
        }
    ]
}
```