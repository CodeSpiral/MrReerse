"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Http = require("http");
const _AppConfig = require("./config");
const _SendServer = _AppConfig.SendToCollectionServer();
const _CanSend = !!_SendServer;
function Send(urlPath, body) {
    if (_CanSend) {
        if (!urlPath) {
            console.log('发送到收集服务器的urlPath没有数据！');
        }
        try {
            let options = {
                hostname: _SendServer,
                port: _AppConfig.CollectionServerPort(),
                path: urlPath,
                method: 'POST',
            };
            let req = _Http.request(options, function (res) { res.destroy(); }).on('error', (e) => {
            });
            req.write(body);
            req.end(function () { req.destroy(undefined); });
        }
        catch (err) { }
    }
}
function Send_HI() {
    if (_CanSend) {
        try {
            let options = {
                hostname: _SendServer,
                port: _AppConfig.CollectionServerPort(),
                path: '/hi',
                method: 'POST',
            };
            let req = _Http.request(options, function (res) {
                let dataBuf;
                let dataStr;
                res.on('data', function (chunk) {
                    if (typeof chunk === 'string')
                        dataStr = dataStr ? (dataStr + chunk) : chunk;
                    else
                        dataBuf = Buffer.concat([(dataBuf ? dataBuf : Buffer.alloc(0)), chunk]);
                }).on('end', function () {
                    if (dataBuf)
                        dataStr = dataBuf.toString();
                    console.log(`[收集服务器HI]响应:`.yellow, dataStr);
                }).on('error', (e) => {
                    console.error(`[收集服务器HI]响应ERROR:`.red, e);
                });
            }).on('error', (e) => {
                console.error(`[收集服务器HI]ERROR:`.red, e);
            });
            req.end(function () { });
        }
        catch (err) { }
    }
}
exports.Send_HI = Send_HI;
function Send_TopPage(urlPath, json) {
    Send(urlPath, JSON.stringify(lib_filterObject({
        pieceList: '*',
        giftList: '*',
    }, json)));
}
exports.Send_TopPage = Send_TopPage;
function CheckSend_Something(urlParse, body) {
    if (urlParse.path.indexOf('/magica/api/page/MyPage?value=') === 0 && urlParse.query.indexOf('userCharaList,userCardList') > 0) {
        let json = JsonParse(body);
        if (json && typeof json === 'object') {
            Send(urlParse.path, JSON.stringify(lib_filterObject({
                "userCharaList": [
                    { "chara": '*' }
                ],
                "userCardList": [
                    { "card": '*' }
                ]
            }, json)));
        }
    }
    else if (urlParse.path.indexOf('/magica/api/page/CharaCollection') === 0) {
        let json = JsonParse(body);
        if (json && typeof json === 'object') {
            let sendData = lib_filterObject({
                "charaList": [
                    {
                        "charaId": '*',
                        "chara": '*',
                        "cardList": [
                            { "card": '*', }
                        ]
                    }
                ]
            }, json);
            Send(urlParse.path, JSON.stringify(sendData));
        }
    }
    else if (urlParse.path.indexOf('/magica/api/gacha/draw') === 0) {
        let json = JsonParse(body);
        if (json && typeof json === 'object') {
            let sendData = lib_filterObject({
                resultCode: "*",
                gameUser: {
                    inviteCode: "*",
                },
                gachaAnimation: '*',
            }, json);
            Send(urlParse.path, JSON.stringify(sendData));
        }
    }
}
exports.CheckSend_Something = CheckSend_Something;
const _FilterRule_BattleList = {
    userSectionList: [
        {
            section: ['-',
                'openEnemy',
                'openEnemyList',
                'openConditionSectionId',
                'openConditionSection',
                'openConditionCharaBondsPt',
                'openConditionMagiaLevel',
                'openConditionChapter',
                'openConditionRank',
                'message',
                'outline',
                'clearReward'
            ]
        },
    ],
    userQuestBattleList: [
        {
            questBattle: ['-',
                'bgm',
                'bossBgm'
            ]
        },
    ]
};
function Send_BattleList(urlPath, json) {
    Send(urlPath, JSON.stringify(lib_filterObject(_FilterRule_BattleList, json)));
}
exports.Send_BattleList = Send_BattleList;
const _FilterRule_BattleEnd = {
    gameUser: {
        inviteCode: '*'
    },
    userQuestBattleResultList: '*',
    userSectionList: [
        { section: '*' }
    ],
    userQuestBattleList: [
        { questBattle: '*' }
    ],
};
function Send_BattleEnd(urlPath, json) {
    Send(urlPath, JSON.stringify(lib_filterObject(_FilterRule_BattleEnd, json)));
}
exports.Send_BattleEnd = Send_BattleEnd;
const _FilterRule_BattleGet = {
    battleType: '*',
    scenario: '*',
    waveList: '*',
    artList: '*',
    memoriaList: '*',
    magiaList: '*',
    connectList: '*',
    doppelList: '*',
    webData: {
        resultCode: '*',
        gameUser: {
            inviteCode: '*',
        },
        userQuestBattleResultList: [
            { questBattleId: '*', }
        ]
    }
};
function Send_BattleCollection(urlPath, json) {
    let data = lib_filterObject(_FilterRule_BattleGet, json);
    Send(urlPath, JSON.stringify(data));
}
exports.Send_BattleCollection = Send_BattleCollection;
function JsonParse(str) {
    if (str) {
        try {
            return JSON.parse(str);
        }
        catch (err) {
            console.log(`json转换失败！`.red, err);
            return false;
        }
    }
    return undefined;
}
exports.JsonParse = JsonParse;
function lib_filterObject(filter, obj) {
    if (filter instanceof Array) {
        if (filter.length == 1 && obj instanceof Array) {
            let res = [];
            for (let index = 0; index < obj.length; index++) {
                res.push(lib_filterObject(filter[0], obj[index]));
            }
            return res;
        }
        else if (typeof obj === 'object') {
            if (filter[0] === '-') {
                for (let index = 0; index < filter.length; index++) {
                    delete obj[filter[index]];
                }
                return obj;
            }
            else {
                return `[ERROR-A(${filter[0]})->O]`;
            }
        }
        else {
            return `[ERROR-A]`;
        }
    }
    else if (typeof filter === 'object' && typeof obj === 'object') {
        let res = {};
        for (const key in filter) {
            if (!filter.hasOwnProperty(key))
                continue;
            if (!obj.hasOwnProperty(key))
                continue;
            if (obj[key] === undefined)
                continue;
            res[key] = (filter[key] == '*') ? obj[key] : lib_filterObject(filter[key], obj[key]);
        }
        return res;
    }
    else {
        return obj;
    }
}
