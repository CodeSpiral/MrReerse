"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Fs = require("fs");
const _Path = require("path");
const _Url = require("url");
const _Crypto = require("crypto");
const _AppConfig = require("./config");
const _Collection = require("./sendCollection");
const _C_PlayerCountry = _AppConfig.PlayerCountry();
const _C_SaveBattleResultRequest = _AppConfig.SaveBattleResultRequest();
const _C_ArenaBattleInfoHelper = _AppConfig.ArenaBattleInfoHelper();
const _C_IsOnlyDoCollectionSend = _AppConfig.IsOnlyDoCollectionSend();
const _LogDetail = false;
const _IsDev = _AppConfig.IsDev();
const _Base = _AppConfig.BasePath_Base();
const _BaseMagicaExtend = _AppConfig.BasePath_MagicaExtend();
const _LFileConf_RequsetResponseDir = _Path.join(_BaseMagicaExtend, 'requsetResponse');
const _LFileConf = [
    '/magica/resource/image_web/item/event/',
    '/magica/resource/image_web/item/main/',
    '/magica/resource/download/asset/master/resource/image_native/gift/',
    '/magica/resource/image_web/page/memoria/',
    '/magica/resource/image_web/page/announce/',
    '/magica/resource/download/asset/master/resource/image_native/memoria/',
    '/magica/resource/download/asset/master/resource/image_native/art/',
];
const _LFileConf_dev = [
    '/magica/js/',
    '/magica/css/',
    '/magica/template/',
    '/magica/resource/image_web/',
    '/magica/resource/download/asset/master',
    '/magica/resource/download/asset/master/resource/',
];
const _LFileConf_RuningCheck = {
    '/magica/js/system/replacement.js': true
};
const _SC_downloadPackUrl_S = '/magica/resource/download/asset/master/resource/';
const _SC_downloadPackUrl_L = '/magica/resource/resource/';
function mgo_SC_downloadPackUrl_L() { return _SC_downloadPackUrl_L; }
exports.mgo_SC_downloadPackUrl_L = mgo_SC_downloadPackUrl_L;
const _C_CacheDownloadPack = _AppConfig.CacheDownloadPack();
const _C_LocalAdvFile = _AppConfig.TranslateAdvFile();
const _LFileConf_adv = '/magica/resource/download/asset/master/resource/scenario/json/adv/';
const _LFileConf_translateAdvDir = _Path.join(_BaseMagicaExtend, 'adv-git');
const _ETag_DateSet = {};
exports.mgoUseLocalFile = function (url, headers) {
    try {
        const urlData = _Url.parse(url);
        const pathData = _Path.parse(urlData.pathname);
        const filePath = pathData.dir.replace(/\\/g, '/');
        if (_C_LocalAdvFile && urlData.pathname.indexOf(_LFileConf_adv) == 0) {
            let buf;
            let saveFilePath = filePath;
            if (filePath.indexOf(_SC_downloadPackUrl_S) === 0) {
                saveFilePath = filePath.replace(_SC_downloadPackUrl_S, _SC_downloadPackUrl_L);
            }
            if (_C_LocalAdvFile && urlData.pathname.indexOf(_LFileConf_adv) >= 0) {
                buf = ReadFileSync(_LFileConf_translateAdvDir, pathData.base);
                if (!buf) {
                    console.log(`[缓存文件]本地汉化文件不存在： ${pathData.base}`.magenta);
                }
                else {
                    return { type: 'adv', buff: buf };
                }
            }
            const tag = {
                'last-modified': headers['last-modified'],
                'etag': headers['etag']
            };
            let etagFile = `${pathData.name}_etag.json`;
            let oldEtag = _ETag_DateSet[saveFilePath + '/' + etagFile];
            if (oldEtag == undefined) {
                oldEtag = ReadJsonFileSync(_Path.join(_Base, saveFilePath), etagFile);
                _ETag_DateSet[saveFilePath + '/' + etagFile] = oldEtag;
            }
            if (typeof oldEtag === 'object' && oldEtag['etag'] === tag['etag'] && oldEtag['last-modified'] === tag['last-modified']) {
            }
            else {
                if (oldEtag) {
                    console.log(`[文件]在线文件已经更新。${saveFilePath}/${pathData.base}`.blue);
                    console.log('新（在线）'.cyan, tag ? JSON.stringify(tag) : 'unll');
                    console.log('旧（本地）'.cyan, oldEtag ? JSON.stringify(oldEtag) : 'unll');
                }
                else {
                }
                return undefined;
            }
            if (!buf) {
                buf = ReadFileSync(_Path.join(_Base, saveFilePath), pathData.base);
            }
            if (!buf) {
                console.log(`[缓存文件]文件不存在：${saveFilePath}/${pathData.base}`.magenta);
                return undefined;
            }
            else {
                return { type: 'file', buff: buf };
                ;
            }
        }
    }
    catch (err) {
        console.log(`[mr][是否使用本地文件]处理错误:${url}\r\n`.red, err);
        return undefined;
    }
};
const mgoCanSaveFile = function (urlData, pathData) {
    let dir = pathData.dir.replace(/\\/g, '/');
    if (_IsDev) {
        for (let index = 0; index < _LFileConf_dev.length; index++) {
            const key = _LFileConf_dev[index];
            if (dir.indexOf(key) === 0) {
                return true;
            }
        }
    }
    else {
        if (_C_CacheDownloadPack && dir.indexOf(_SC_downloadPackUrl_S) === 0) {
            return true;
        }
        for (let index = 0; index < _LFileConf.length; index++) {
            const key = _LFileConf[index];
            if (dir.indexOf(key) === 0) {
                return true;
            }
        }
    }
    return false;
};
exports.mgoAutoSaveFile = function (url, headers, body, isSync) {
    try {
        if (_C_IsOnlyDoCollectionSend)
            return;
        const urlData = _Url.parse(url);
        const pathData = _Path.parse(urlData.pathname);
        let saveFile = false;
        if (mgoCanSaveFile(urlData, pathData) === true) {
            saveFile = true;
        }
        if (saveFile === true) {
            var temp = _LFileConf_RuningCheck[urlData.pathname];
            if (temp !== undefined) {
                if (temp === true) {
                    _LFileConf_RuningCheck[urlData.pathname] = body;
                }
                else {
                    if (typeof body === typeof temp) { }
                    else {
                        console.log();
                        console.log(`[mr] ${urlData.pathname}  TODO  转成同类型`.yellow.bgRed);
                        console.log();
                        return;
                    }
                    if (typeof body === 'string') {
                        if (temp == body) {
                            console.log(`[mr]无变化，无需保存: ${urlData.pathname}`.yellow);
                            return;
                        }
                    }
                    else {
                        if (body.compare(temp) === 0) {
                            console.log(`[mr]无变化，无需保存: ${urlData.pathname}`.yellow);
                            return;
                        }
                    }
                }
            }
        }
        if (saveFile != true) {
            return;
        }
        let filePath = pathData.dir.replace(/\\/g, '/');
        let saveFilePath = filePath;
        if (filePath.indexOf(_SC_downloadPackUrl_S) === 0) {
            saveFilePath = filePath.replace(_SC_downloadPackUrl_S, _SC_downloadPackUrl_L);
        }
        if (saveFile === true) {
            let saveFile = pathData.base;
            let saveBody = body;
            if (pathData.base.indexOf('.json.gz') > 0) {
                saveFile = pathData.base.replace('.json.gz', '.json');
            }
            if (isSync)
                WriteFileSync(_Path.join(_Base, saveFilePath), saveFile, saveBody, true);
            else
                WriteFile(_Path.join(_Base, saveFilePath), saveFile, saveBody, true, true);
        }
    }
    catch (err) {
        console.log(`[mr][缓/保存]Error: \r\n    ${url}\r\n`.red, err);
    }
};
function ReadJsonFileSync(dir, fileName) {
    let file = _Path.join(dir, fileName);
    if (_Fs.existsSync(file)) {
        try {
            let txt = _Fs.readFileSync(file, { encoding: 'utf8' });
            return JSON.parse(txt);
        }
        catch (error) {
            console.warn(`[读]JSON失败！: ${file}`.red, error);
            return undefined;
        }
    }
    return undefined;
}
exports.mgo_ReadJsonFileSync = ReadJsonFileSync;
function ReadFileSync(dir, fileName) {
    let file = _Path.join(dir, fileName);
    if (_Fs.existsSync(file)) {
        try {
            let buffer = _Fs.readFileSync(file);
            return buffer;
        }
        catch (error) {
            console.warn(`[读]异常！: ${file}`.red, error);
            return undefined;
        }
    }
    return undefined;
}
const CreateDirsSync = function (dir) {
    let paths = [];
    while (!_Fs.existsSync(dir)) {
        paths.push(dir);
        dir = _Path.dirname(dir);
    }
    for (let index = paths.length - 1; index >= 0; index--) {
        let dir = paths[index];
        _Fs.mkdirSync(dir);
    }
};
exports.WriteFileJson = function (dir, fileName, obj, log) {
    WriteFile(dir, fileName, JSON.stringify(obj), log, true);
};
function WriteFileSync(dir, fileName, text, log) {
    if (typeof text !== 'string') {
        WriteBufferFileSync(dir, fileName, text, log);
        return;
    }
    try {
        CreateDirsSync(dir);
        let file = _Path.join(dir, fileName);
        _Fs.writeFileSync(file, text, { encoding: 'utf8' });
        if (log)
            console.log('[写][Sync][Ok]: ' + file.replace(_Base, ''));
        console.log(_Base);
    }
    catch (error) {
        console.warn('[写][Error]:'.red, error);
    }
}
function WriteBufferFileSync(dir, fileName, buf, log) {
    if (!buf || buf.length == 0) {
        console.log('[写][Error]: 写的内容为null'.red);
        return false;
    }
    try {
        CreateDirsSync(dir);
        let file = _Path.join(dir, fileName);
        _Fs.writeFileSync(file, buf);
        if (log)
            console.log('[写][Ok]: ' + file);
        return true;
    }
    catch (error) {
        console.warn('[写][Error]:'.red, error);
        return false;
    }
}
exports.mgoWriteBufferFileSync = WriteBufferFileSync;
function WriteFile(dir, fileName, data, log, checkOldFile) {
    try {
        CreateDirsSync(dir);
        let file = _Path.join(dir, fileName);
        if (checkOldFile) {
            if (data instanceof Buffer && _Fs.existsSync(file)) {
                var buf = _Fs.readFileSync(file);
                if (data.length == buf.length && buf.compare(data) === 0) {
                    console.log(`[写]:(没改变,跳过) ${file.replace(_Base, '')}`.yellow);
                    return;
                }
            }
        }
        let ws = _Fs.createWriteStream(file).on('error', function (err) {
            console.warn('[写][Error]:'.red, err);
        }).end(data, function () {
            if (log)
                console.log(`[写]: ${file}`.magenta.bgYellow);
        });
    }
    catch (error) {
        console.warn('[写][Error]:'.red, error);
    }
}
function WriteFileAdd(dir, fileName, data, log) {
    try {
        CreateDirsSync(dir);
        let file = _Path.join(dir, fileName);
        let ws = _Fs.createWriteStream(file, { flags: 'a', }).on('error', function (err) {
            console.warn('[+写][Error]:'.red, err);
        }).end(data, function () {
            if (log)
                console.log('[+写]: ' + file);
        });
    }
    catch (error) {
        console.warn('[+写][Error]:'.red, error);
    }
}
function WriteFileAddSync(dir, fileName, data, log) {
    try {
        CreateDirsSync(dir);
        let file = _Path.join(dir, fileName);
        _Fs.appendFileSync(file, data);
        if (log)
            console.log('[+写]: ' + file);
    }
    catch (error) {
        console.warn('[+写][Error]:'.red, error);
    }
}
exports.mgoDoSomething = function (url, body) {
    try {
        const urlData = _Url.parse(url);
        (function (tp_urlData, tp_bodyString) {
            setTimeout(function () {
                if (_C_IsOnlyDoCollectionSend == false) {
                    if (_AppConfig.SaveAllApiResponse())
                        mgoSaveAllApiResponse(tp_urlData, tp_bodyString);
                    if (_AppConfig.SaveBattleResult())
                        mgoWatchQuestBattleResult(tp_urlData, tp_bodyString);
                    if (_C_ArenaBattleInfoHelper)
                        mgoWatchArenaJJC(tp_urlData, tp_bodyString);
                }
                mgoWatchDrop(tp_urlData, tp_bodyString);
                mgoUserSQBL(tp_urlData, tp_bodyString);
                mgoUserTopPage(tp_urlData, tp_bodyString);
                _Collection.CheckSend_Something(urlData, body);
            }, 0);
        })(urlData, body);
    }
    catch (err) {
        console.log(`[mr][DoSomething]异常： `.red, err);
    }
};
function mgoSaveAllApiResponse(urlData, body) {
    try {
        if (urlData.pathname.indexOf('/magica/api/') === 0 && body) {
            var pathParse = _Path.parse(urlData.pathname);
            console.log(`[mr][保存Res]Api:${urlData.pathname}.`.cyan);
            WriteFile(_Path.join(_LFileConf_RequsetResponseDir, pathParse.dir), `${pathParse.name}${pathParse.ext || '.json'}`, body, _LogDetail);
        }
    }
    catch (err) {
        console.log(`[mr][保存Res]Error:\r\n`.red, err);
    }
}
const _Url_watchDrop_Send = '/magica/api/quest/native/result/send';
const _Url_watchDrop_Get = '/magica/api/quest/native/get';
const _Url_watchDrop_Path_Send = ['userQuestBattleResultList'];
const _Url_watchDrop_Path_Get = ['webData', 'userQuestBattleResultList'];
const _Url_watchDrop_Key_Drop = 'dropRewardCodes';
const _Url_watchDrop_Key_QuestBId = 'questBattleId';
const _LFileConf_watchDropDir = _Path.join(_BaseMagicaExtend, 'drop');
exports.LFileConf_watchDropOtherDir = () => { return 'dropOther'; };
const _LFileConf_battleDir = _Path.join(_BaseMagicaExtend, 'battle');
function mgoWatchDrop(urlData, body) {
    try {
        if (urlData.pathname == _Url_watchDrop_Get) {
            let battleJson;
            try {
                battleJson = JSON.parse(body);
            }
            catch (err) {
                console.log(`[mr][掉落监听]Error: json转换失败！`.red, err);
            }
            if (!battleJson || typeof battleJson !== 'object')
                return;
            const battleType = battleJson['battleType'];
            if (battleType == 'ARENA') {
                return;
            }
            let questBattleId = '获取ID失败';
            const userQuestBattleResultList = GetJsonKeyValue(battleJson, _Url_watchDrop_Path_Get);
            if (typeof userQuestBattleResultList === 'object') {
                for (let index = 0; index < userQuestBattleResultList.length; index++) {
                    const item = userQuestBattleResultList[index];
                    const qBid = item['questBattleId'];
                    const drop = item['dropRewardCodes'];
                    if (_C_IsOnlyDoCollectionSend == false) {
                        console.log(`[mr][掉落(不记录)][战前]:[${battleType}]${qBid}:${drop ? drop : '无'}`.cyan);
                    }
                    if (qBid)
                        questBattleId = qBid;
                }
            }
            else {
            }
            let canSaveQuestBattle = _AppConfig.SaveQuestBattle();
            if (_C_IsOnlyDoCollectionSend) {
                canSaveQuestBattle = true;
            }
            else if (canSaveQuestBattle == false && _AppConfig.SaveQuestBattleAutoSaveOne()) {
                canSaveQuestBattle = !_Fs.existsSync(_Path.join(_LFileConf_battleDir, `${questBattleId}.json`));
            }
            if (canSaveQuestBattle) {
                try {
                    _Collection.Send_BattleCollection(urlData.path, battleJson);
                    if (_C_IsOnlyDoCollectionSend == false) {
                        if (battleJson['webData']) {
                            if (battleJson['webData']['gameUser'])
                                delete battleJson['webData']['gameUser'];
                        }
                        console.log(`[mr][战斗数据]保存: ${_LFileConf_battleDir}/${questBattleId}.json`.cyan + '\r\n');
                        exports.WriteFileJson(_LFileConf_battleDir, `${questBattleId}.json`, battleJson, _LogDetail);
                    }
                }
                catch (err) {
                    console.log(`[mr][战斗数据]Error: 保存异常！`.red, err);
                }
            }
        }
        else if (urlData.pathname == _Url_watchDrop_Send) {
            let battleJson;
            try {
                battleJson = JSON.parse(body);
            }
            catch (err) {
                console.log(`[mr][掉落监听]Error: json转换失败！`.red, err);
            }
            if (!battleJson || typeof battleJson !== 'object')
                return;
            let questBattleId = '获取ID失败';
            const userQuestBattleResultList = GetJsonKeyValue(battleJson, _Url_watchDrop_Path_Send);
            if (typeof userQuestBattleResultList === 'object') {
                if (_C_IsOnlyDoCollectionSend == false) {
                    for (let index = 0; index < userQuestBattleResultList.length; index++) {
                        const item = userQuestBattleResultList[index];
                        const battleType = item['battleType'];
                        const qBid = item['questBattleId'];
                        const drop = item['dropRewardCodes'];
                        const time = new Date().toISOString();
                        if (battleType == 'ARENA') {
                            console.log(`[mr][掉落监听]: 战斗类型： ${battleType} 不记录。`.cyan);
                            return;
                        }
                        if (qBid)
                            questBattleId = qBid;
                        console.log('\r\n' + `[mr][掉落监听][战后]:[${battleType}](${time})${qBid}:${drop ? drop : '无'}`.cyan + '\r\n');
                        WriteFileAdd(_LFileConf_watchDropDir, `${qBid}.txt`, `${time}|${drop ? drop : '-'}\r\n`, _LogDetail);
                    }
                }
                _Collection.Send_BattleEnd(urlData.path, battleJson);
            }
            else {
                console.log('\r\n' + `[mr][掉落监听][战后]:获取数据失败`.cyan + '\r\n');
            }
        }
    }
    catch (err) {
        console.log(`[mr][掉落监听]Error: \r\n    ${urlData.pathname}\r\n`.red, err);
    }
}
const _Url_watchQuestBattleResult = '/magica/api/quest/native/result/send';
const _LFileConf_QuestBattleResultDir = _Path.join(_BaseMagicaExtend, 'battleResult');
function mgoWatchQuestBattleResult(urlData, body) {
    try {
        if (urlData.pathname == _Url_watchQuestBattleResult) {
            let battleJson;
            try {
                battleJson = JSON.parse(body);
            }
            catch (err) {
                console.log(`[mr][战斗结束数据]Error: json转换失败！`.red, err);
            }
            if (!battleJson || typeof battleJson !== 'object')
                return;
            let questBattleId = '获取ID失败';
            const userQuestBattleResultList = GetJsonKeyValue(battleJson, ['userQuestBattleResultList']);
            if (typeof userQuestBattleResultList === 'object') {
                for (let index = 0; index < userQuestBattleResultList.length; index++) {
                    const item = userQuestBattleResultList[index];
                    const qBid = item['questBattleId'];
                    const battleType = item['battleType'];
                    if (battleType == 'ARENA') {
                        console.log(`[mr][战斗结束数据]: 战斗类型： ${battleType} 不记录。`.cyan);
                        return;
                    }
                    if (qBid)
                        questBattleId = qBid;
                }
            }
            try {
                console.log('\r\n' + `[mr][战斗结束数据]保存: ${_LFileConf_QuestBattleResultDir}/${questBattleId}.json`.cyan + '\r\n');
                exports.WriteFileJson(_LFileConf_QuestBattleResultDir, `${questBattleId}.json`, battleJson, _LogDetail);
            }
            catch (err) {
                console.log(`[mr][战斗结束数据]Error: 保存异常！`.red, err);
            }
        }
    }
    catch (err) {
        console.log(`[mr][战斗结束数据]Error: \r\n    ${urlData.pathname}\r\n`.red, err);
    }
}
function GetJsonKeyValue(json, path) {
    let value = json;
    for (let index = 0; index < path.length; index++) {
        const key = path[index];
        if (value[key] !== undefined) {
            value = value[key];
        }
        else {
            return undefined;
        }
    }
    return value;
}
const _LFileConf_userSQBL_File = 'userBattleList.json';
const _UV_uSectionList = 'userSectionList';
const _UV_uQuestBattleList = 'userQuestBattleList';
function mgoUserSQBL(urlData, body) {
    try {
        let jsonObj;
        if ((urlData.query && urlData.query.indexOf(`${_UV_uSectionList},${_UV_uQuestBattleList}`) > 0)
            || urlData.pathname == '/magica/api/quest/native/result/send'
            || urlData.pathname == '/magica/api/page/EventBranchTop'
            || urlData.pathname == '/magica/api/page/EventSingleRaidTop') {
            if (jsonObj == undefined) {
                try {
                    jsonObj = JSON.parse(body);
                }
                catch (err) {
                    console.log(`[mr][用户关卡列表记录]Error: json转换失败！`.red, err);
                }
            }
            if (!jsonObj)
                return;
            if (_C_IsOnlyDoCollectionSend) {
                if (urlData.pathname != _Url_watchDrop_Send) {
                    _Collection.Send_BattleList(urlData.path, jsonObj);
                }
                return;
            }
            try {
                let isInit = (urlData.query && urlData.query.indexOf(`${_UV_uSectionList},${_UV_uQuestBattleList}`) > 0);
                let save = mgoUserSQBL_DataMerge(jsonObj, isInit);
                if (save) {
                    if (!_Cache_uSectionQuestBattleListData) {
                        console.log(`[mr][用户关卡列表记录]Error: 数据处理异常！数据处理后为null。 \r\n    ${urlData.pathname}\r\n`.magenta);
                    }
                    else {
                        console.log('\r\n' + `[mr][用户关卡列表记录]${_LFileConf_userSQBL_File}.`.cyan + '\r\n');
                        exports.WriteFileJson(_BaseMagicaExtend, _LFileConf_userSQBL_File, _Cache_uSectionQuestBattleListData, true);
                    }
                }
                else { }
            }
            catch (err) {
                console.log(`[mr][用户关卡列表记录]Error: 数据处理异常！ \r\n    ${urlData.pathname}\r\n`.red, err);
            }
        }
    }
    catch (err) {
        console.log(`[mr][用户关卡列表记录]Error: \r\n    ${urlData.pathname}\r\n`.red, err);
    }
}
var _Cache_uSectionQuestBattleListData = undefined;
function mgoUserSQBL_LoadDb() {
    if (!_Cache_uSectionQuestBattleListData) {
        try {
            const oldData = ReadJsonFileSync(_BaseMagicaExtend, _LFileConf_userSQBL_File);
            if (typeof oldData === 'object')
                _Cache_uSectionQuestBattleListData = oldData;
            else
                _Cache_uSectionQuestBattleListData = {};
        }
        catch (err) {
            _Cache_uSectionQuestBattleListData = {};
        }
    }
    if (typeof _Cache_uSectionQuestBattleListData !== 'object')
        _Cache_uSectionQuestBattleListData = {};
}
function mgoUserSQBL_DataMerge(jsonObj, isInit) {
    mgoUserSQBL_LoadDb();
    let canSave = false;
    if (jsonObj.userSectionList) {
        let uSList = _Cache_uSectionQuestBattleListData.uSectionList || {};
        for (var index = 0; index < jsonObj.userSectionList.length; index++) {
            var item = jsonObj.userSectionList[index];
            var sectionId = item.section.sectionId;
            uSList[sectionId] = item.section;
            if (item.section.openConditionSection) {
                var ssId = item.section.openConditionSection.sectionId;
                if (uSList[ssId] == undefined) {
                    uSList[ssId] = item.section.openConditionSection;
                    canSave = true;
                }
            }
        }
        if (isInit)
            canSave = true;
        _Cache_uSectionQuestBattleListData.uSectionList = uSList;
    }
    if (jsonObj.userQuestBattleList) {
        let uQBList = _Cache_uSectionQuestBattleListData.uQuestBattleList || {};
        for (var index = 0; index < jsonObj.userQuestBattleList.length; index++) {
            var qbList = jsonObj.userQuestBattleList[index];
            if (typeof qbList.questBattle === 'object' && qbList.questBattle.sectionId) {
                var sid = qbList.questBattle.sectionId;
                var sQB = qbList.questBattle;
                var sQBid = qbList.questBattle.questBattleId;
                if (!uQBList[sid]) {
                    uQBList[sid] = {};
                }
                if (uQBList[sid][sQBid] == undefined) {
                    canSave = true;
                }
                uQBList[sid][sQBid] = sQB;
            }
        }
        if (isInit)
            canSave = true;
        _Cache_uSectionQuestBattleListData.uQuestBattleList = uQBList;
    }
    if (jsonObj.userEventSingleRaidPointList) {
        let uESRPL = _Cache_uSectionQuestBattleListData.uEventSingleRaidPointList || {};
        let uESRPL_now = jsonObj.userEventSingleRaidPointList;
        for (const index in uESRPL_now) {
            const item = uESRPL_now[index];
            if (item.eventId && item.point) {
                let eventId = item.eventId;
                let sectionId = item.point.sectionId || 0;
                if (uESRPL[eventId] == undefined)
                    uESRPL[eventId] = {};
                if (uESRPL[eventId][sectionId]) {
                }
                else {
                    uESRPL[eventId][sectionId] = item.point;
                    canSave = true;
                }
            }
        }
        _Cache_uSectionQuestBattleListData.uEventSingleRaidPointList = uESRPL;
    }
    if (jsonObj.userEventBranchPointList) {
        let uEBPL = _Cache_uSectionQuestBattleListData.uEventBranchPointList || {};
        let uEBPL_now = jsonObj.userEventBranchPointList;
        for (const index in uEBPL_now) {
            const item = uEBPL_now[index];
            if (item.eventId && item.point) {
                let eventId = item.eventId;
                let questBattleId = item.point.questBattleId || 0;
                if (uEBPL[eventId] == undefined)
                    uEBPL[eventId] = {};
                if (uEBPL[eventId][questBattleId]) {
                }
                else {
                    uEBPL[eventId][questBattleId] = item.point;
                    canSave = true;
                }
            }
        }
        _Cache_uSectionQuestBattleListData.uEventBranchPointList = uEBPL;
    }
    if (canSave) {
        _Cache_uSectionQuestBattleListData.time = Date.now();
        return true;
    }
    else {
        return false;
    }
}
exports.mgoUserSQBL_OnOtherUser = function (jsonObj) {
    mgoUserSQBL_LoadDb();
    let canSave = false;
    if (jsonObj.uSectionList) {
        let uSList = _Cache_uSectionQuestBattleListData.uSectionList || {};
        for (const keyId in jsonObj.uSectionList) {
            if (uSList[keyId] === undefined)
                canSave = true;
            uSList[keyId] = jsonObj.uSectionList[keyId];
        }
        _Cache_uSectionQuestBattleListData.uSectionList = uSList;
    }
    if (jsonObj.uQuestBattleList) {
        let uQBList = _Cache_uSectionQuestBattleListData.uQuestBattleList || {};
        for (const keySid in jsonObj.uQuestBattleList) {
            if (uQBList[keySid] === undefined) {
                uQBList[keySid] = {};
                canSave = true;
            }
            const sL = jsonObj.uQuestBattleList[keySid];
            for (const keyQBid in sL) {
                if (uQBList[keySid][keyQBid] === undefined) {
                    canSave = true;
                }
                uQBList[keySid][keyQBid] = sL[keyQBid];
            }
        }
        _Cache_uSectionQuestBattleListData.uQuestBattleList = uQBList;
    }
    if (jsonObj.uEventSingleRaidPointList) {
        let uESRPList = _Cache_uSectionQuestBattleListData.uEventSingleRaidPointList || {};
        for (const keyEid in jsonObj.uEventSingleRaidPointList) {
            if (uESRPList[keyEid] === undefined) {
                uESRPList[keyEid] = {};
                canSave = true;
            }
            const eL = jsonObj.uEventSingleRaidPointList[keyEid];
            for (const keySid in eL) {
                if (uESRPList[keyEid][keySid] === undefined) {
                    canSave = true;
                }
                uESRPList[keyEid][keySid] = eL[keySid];
            }
        }
        _Cache_uSectionQuestBattleListData.uEventSingleRaidPointList = uESRPList;
    }
    if (jsonObj.uEventBranchPointList) {
        let uESRPList = _Cache_uSectionQuestBattleListData.uEventBranchPointList || {};
        for (const keyEid in jsonObj.uEventBranchPointList) {
            if (uESRPList[keyEid] === undefined) {
                uESRPList[keyEid] = {};
                canSave = true;
            }
            const eL = jsonObj.uEventBranchPointList[keyEid];
            for (const keySid in eL) {
                if (uESRPList[keyEid][keySid] === undefined) {
                    canSave = true;
                }
                uESRPList[keyEid][keySid] = eL[keySid];
            }
        }
        _Cache_uSectionQuestBattleListData.uEventBranchPointList = uESRPList;
    }
    console.log();
    if (!_Cache_uSectionQuestBattleListData) {
        console.log(`[mr][来自其他用户：关卡列表记录]Error: 数据处理异常！数据处理后为null。 `.magenta.bgYellow);
    }
    else {
        console.log(`[mr][来自其他用户：关卡列表记录]Over。 `.magenta.bgYellow);
        exports.WriteFileJson(_BaseMagicaExtend, _LFileConf_userSQBL_File, _Cache_uSectionQuestBattleListData, _LogDetail);
    }
    console.log();
};
const _UPath_TopPage = '/magica/api/page/TopPage';
const _LFileConf_itemGiftPiece_File = 'itemGiftPieceList.json';
const _UV_pieceList = 'pieceList';
const _UV_itemList = 'itemList';
function mgoUserTopPage(urlData, body) {
    try {
        if (urlData.pathname == _UPath_TopPage) {
            let jsonObj;
            try {
                jsonObj = JSON.parse(body);
            }
            catch (err) {
                console.log(`[mr][TopPage]Error: json转换失败！`.red, err);
            }
            if (!jsonObj)
                return;
            if (_C_PlayerCountry) {
                if (jsonObj['user'] && jsonObj['user']['country']) {
                    let country = jsonObj['user']['country'];
                    console.log(`[mr][用户country]: ${country}`.cyan);
                }
            }
            if (jsonObj[_UV_pieceList] && _C_IsOnlyDoCollectionSend == false) {
                try {
                    let data = {
                        pieceList: jsonObj[_UV_pieceList],
                        itemList: jsonObj[_UV_itemList],
                    };
                    console.log(`[mr][记忆记录]${_LFileConf_itemGiftPiece_File}.`.cyan);
                    exports.WriteFileJson(_BaseMagicaExtend, _LFileConf_itemGiftPiece_File, data, _LogDetail);
                }
                catch (err) {
                    console.log(`[mr][记忆记录]Error: 数据处理异常！\r\n`.red, err);
                }
            }
            _Collection.Send_TopPage(urlData.path, jsonObj);
        }
    }
    catch (err) {
        console.log(`[mr][TopPage]Error: \r\n    ${urlData.pathname}\r\n`.red, err);
    }
}
exports.mgoPieceList_OnOtherUser = function (jsonObj) {
    const logTag = '[mr][OtherUser][记忆记录]';
    if (!jsonObj) {
        console.log(`${logTag}Error: 传入null数据`.red);
        return;
    }
    var json = ReadJsonFileSync(_BaseMagicaExtend, _LFileConf_itemGiftPiece_File);
    if (!json || typeof json !== 'object')
        json = {};
    try {
        json[_UV_pieceList] = jsonObj['pieceList'];
        console.log(`${logTag}更新：${_LFileConf_itemGiftPiece_File}.`.bgYellow.magenta);
        exports.WriteFileJson(_BaseMagicaExtend, _LFileConf_itemGiftPiece_File, json, _LogDetail);
    }
    catch (err) {
        console.log(`${logTag}Error: 数据处理异常！\r\n`.red, err);
    }
};
const _Url_watchArenaJJC = '/magica/api/page/ArenaFreeRank';
const _Url_watchArenaJJC_Event = '/magica/api/page/EventArenaRankingTop';
const _Url_watchArenaJJC_reload = '/magica/api/arena/reload';
function mgoWatchArenaJJC(urlData, body) {
    try {
        if (urlData.pathname == _Url_watchArenaJJC
            || urlData.pathname == _Url_watchArenaJJC_Event
            || urlData.pathname == _Url_watchArenaJJC_reload) {
            let battleJson;
            try {
                battleJson = JSON.parse(body);
            }
            catch (err) {
                console.log(`[mr][镜层]Error: json转换失败！`.red, err);
            }
            if (!battleJson || typeof battleJson !== 'object')
                return;
            const userArenaBattleMatch = battleJson.userArenaBattleMatch;
            if (typeof userArenaBattleMatch === 'object') {
                let arenaBattleType = userArenaBattleMatch.arenaBattleType;
                const arenaBattleType_RANKING = 'RANKING';
                const arenaBattleType_FREE_RANK = 'FREE_RANK';
                console.log(`[mr][镜层] ${arenaBattleType}`);
                let showSort = ['HIGHER', 'SAME', 'LOWER',];
                const typeStr = { 'HIGHER': '上层', 'SAME': '同层', 'LOWER': '下层', };
                const cardAttribute = { FIRE: "火", WATER: "水", TIMBER: "木", LIGHT: "光", DARK: "暗" };
                const cardRank = { RANK_1: '星1', RANK_2: '星2', RANK_3: '星3', RANK_4: '星4', RANK_5: '星5', RANK_6: '星6', };
                const arenaBattleOpponentTeamType = { 'A': 1.8, 'B': 1.7, 'C': 1.5, 'D': 1.3, 'E': 1.1, 'F': 1, 'G': 0.9, 'H': 0.8, 'I': 0.7, };
                if (arenaBattleType == arenaBattleType_RANKING) {
                    showSort = Object.keys(arenaBattleOpponentTeamType);
                }
                let logShowIndex = 1;
                for (let indexTypeSort = 0; indexTypeSort < showSort.length; indexTypeSort++) {
                    const keyType = showSort[indexTypeSort];
                    let index = 1;
                    while (userArenaBattleMatch['opponentUserArenaBattleInfo' + index]) {
                        var uArenaBattleInfo = userArenaBattleMatch['opponentUserArenaBattleInfo' + index];
                        index++;
                        if (!uArenaBattleInfo)
                            continue;
                        let type = uArenaBattleInfo.type;
                        if (arenaBattleType == arenaBattleType_RANKING) {
                            if (keyType != uArenaBattleInfo.arenaBattleOpponentTeamType)
                                continue;
                        }
                        else {
                            if (keyType != type)
                                continue;
                        }
                        console.log(`${logShowIndex++}`.cyan);
                        type = typeStr[type];
                        let userName = uArenaBattleInfo.userName;
                        let userRank = uArenaBattleInfo.userRank;
                        let userCardList = uArenaBattleInfo.userCardList;
                        let userRatingPoint = uArenaBattleInfo.userRatingPoint;
                        let countValue = 0;
                        let countCard = 0;
                        let cardLvStr = '';
                        for (let indexCL = 0; indexCL < userCardList.length; indexCL++) {
                            const card = userCardList[indexCL];
                            let level = parseInt(card.level);
                            let attack = parseInt(card.attack);
                            let defense = parseInt(card.defense);
                            let hp = parseInt(card.hp);
                            let cardCount = (attack + defense + hp);
                            countValue += cardCount;
                            countCard++;
                            const cardData = card['card'];
                            let rank = cardData.rank;
                            if (cardRank[rank])
                                rank = cardRank[rank];
                            let attributeId = cardData.attributeId;
                            if (cardAttribute[attributeId])
                                attributeId = cardAttribute[attributeId];
                            cardLvStr += `${attributeId}[${rank}]`.cyan + (level >= 100 ? ('' + level).cyan : level) + ':'.cyan
                                + (cardCount <= 50000 ? ('' + cardCount).cyan : cardCount) + ', '.cyan;
                        }
                        var otherTag = '';
                        if (arenaBattleType == arenaBattleType_RANKING) {
                            otherTag = 'Pt:x'.cyan + arenaBattleOpponentTeamType[uArenaBattleInfo.arenaBattleOpponentTeamType];
                        }
                        let numM = (userRatingPoint - countValue) / 4000;
                        console.log(`   ${type}（Lv${userRank}）人数:`.cyan + (countCard <= 3 ? countCard : ('' + countCard).cyan) + `  `.cyan + cardLvStr);
                        console.log(`   ${userName} ` + otherTag
                            + ` 总战力: ${userRatingPoint} 加成：`.cyan + (userRatingPoint - countValue)
                            + ' / '.cyan + numM.toFixed(1)
                            + ' / '.cyan + (numM / countCard).toFixed(1)
                            + ` 平均：`.cyan + parseInt((userRatingPoint / countCard)));
                    }
                }
            }
        }
    }
    catch (err) {
        console.log(`[mr][镜层]Error: \r\n    ${urlData.pathname} \r\n`.red, err);
    }
}
exports.mgoDoSomethingOnRequest = function (url, body) {
    try {
        const urlData = _Url.parse(url);
        (function (tp_urlData, tp_bodyString) {
            setTimeout(function () {
                if (_AppConfig.SaveAllApiRequset())
                    mgoSaveAllApiRequset(tp_urlData, tp_bodyString);
                if (_C_SaveBattleResultRequest)
                    mgoWatchQuestBattleResult_Request(tp_urlData, tp_bodyString);
            }, 0);
        })(urlData, body);
    }
    catch (err) {
        console.log(`[mr][mgoDoSomethingOnRequest]异常： `.red, err);
    }
};
function mgoSaveAllApiRequset(urlData, body) {
    try {
        if (urlData.pathname.indexOf('/magica/api/') === 0 && body) {
            var pathParse = _Path.parse(urlData.pathname);
            console.log(`[mr][保存Req]Api:${urlData.pathname}.`.magenta);
            WriteFile(_Path.join(_LFileConf_RequsetResponseDir, pathParse.dir), `${pathParse.name}_req${pathParse.ext || '.json'}`, body, _LogDetail);
        }
    }
    catch (err) {
        console.log(`[mr][保存Req]Error:\r\n`.red, err);
    }
}
const _LFileConf_QuestBattleResultRequestDir = _Path.join(_BaseMagicaExtend, 'battleResultRequest');
function mgoWatchQuestBattleResult_Request(urlData, body) {
    try {
        if (urlData.pathname == _Url_watchQuestBattleResult) {
            let jsonObj;
            try {
                if (body instanceof Buffer) {
                    body = body.toString('utf8');
                }
                jsonObj = JSON.parse(body);
            }
            catch (err) {
                console.log(`[mr][战斗返回]Error: json转换失败！`.red, err);
            }
            if (!jsonObj)
                return;
            try {
                let file = 'QuestBattleResultRequest.txt';
                console.log(`[mr][战斗返回]${file}.`.cyan);
                exports.WriteFileJson(_LFileConf_QuestBattleResultRequestDir, file, jsonObj, _LogDetail);
            }
            catch (err) {
                console.log(`[mr][战斗返回]Error: 数据处理异常！\r\n`.red, err);
            }
        }
    }
    catch (err) {
        console.log(`[mr][TopPage]Error: \r\n    ${urlData.pathname} \r\n`.red, err);
    }
}
const _LFileConf_DPPath = '/magica/resource/download/asset/master/';
const _LFileConf_DP_asset_config = 'asset_config.json';
exports.mgoDownloadPackMerge = function () {
    try {
        console.log('[数据包整合].'.yellow + '运行期间不要进行其他操作。'.cyan);
        const basePath = _Path.join(_Base, _LFileConf_DPPath);
        const baseDP = _Path.join(_Base, _SC_downloadPackUrl_L);
        const baseLog = _Path.join(_Base, _SC_downloadPackUrl_L, '..');
        const logMain = 'downloadPack.log.txt';
        let volJson = ReadJsonFileSync(basePath, _LFileConf_DP_asset_config);
        console.log(volJson);
        WriteFileSync(baseLog, logMain, JSON.stringify(volJson) + '\r\n', true);
        var assetFileList = [];
        _Fs.readdirSync(basePath).forEach((val, index) => {
            if (val.indexOf('asset_') === 0 && val != _LFileConf_DP_asset_config && val.indexOf('etag.json') === -1) {
                let stats = _Fs.statSync(_Path.join(basePath, val));
                if (val == 'asset_movie_low.json') {
                    console.log(`[数据包整合]asset_movie_low低清数据包先无视...`.yellow);
                    return;
                }
                if (val.indexOf('asset_scenario_') === 0 || val.indexOf('asset_section_') === 0) {
                    console.log(`[数据包整合]跳过：${val}`);
                }
                else if (stats.isFile()) {
                    assetFileList.push(val);
                }
            }
        });
        let assetPathList = {};
        let assetAllList = [];
        assetFileList.forEach(assetFile => {
            let jsonObj = ReadJsonFileSync(basePath, assetFile);
            if (jsonObj instanceof Array) {
                assetAllList.push(jsonObj);
                console.log('  -> ' + assetFile);
                let i = 0;
                jsonObj.forEach(assetObj => {
                    if (assetObj.path) {
                        assetPathList[assetObj.path] = 1;
                        i++;
                    }
                });
            }
        });
        var countFile = 0;
        var countOK = 0;
        var countDelFile = 0;
        var countMergeErr = 0;
        var countErr1a = 0;
        var countErr1 = 0;
        var countErr2 = 0;
        var countErr3 = 0;
        for (let indexAAL = 0; indexAAL < assetAllList.length; indexAAL++) {
            const assetList = assetAllList[indexAAL];
            console.log(`\r\n  ->[${indexAAL}][${assetFileList[indexAAL]}]: ${assetList.length}\r\n`);
            WriteFileAddSync(baseLog, logMain, `\r\n->[${indexAAL}]: ${assetList.length}\r\n`, false);
            countFile += assetList.length;
            for (let indexAL = 0; indexAL < assetList.length; indexAL++) {
                const asset = assetList[indexAL];
                let tag = `[${indexAAL}-${indexAL}]`;
                let file_list = asset.file_list;
                let md5 = asset.md5;
                const path = asset.path;
                let localFile = _Path.join(baseDP, path);
                let md5OK = false;
                let hasFile = _Fs.existsSync(localFile);
                if (hasFile) {
                    let file = _Fs.readFileSync(localFile);
                    const md5Crypto = _Crypto.createHash('md5');
                    let outMd5 = md5Crypto.update(file).digest('hex');
                    md5OK = (outMd5 == md5);
                    if (md5OK) {
                        if (_LogDetail)
                            console.log(`  ${tag}: OK. `.cyan + path);
                        WriteFileAddSync(baseLog, logMain, `${tag}: OK. ${path}\r\n`, false);
                        countOK++;
                        for (let indexAFL = 0; indexAFL < file_list.length; indexAFL++) {
                            const filePack = file_list[indexAFL];
                            if (path == filePack.url) {
                                continue;
                            }
                            else if (_Fs.existsSync(_Path.join(baseDP, filePack.url))) {
                                if (assetPathList[filePack.url]) {
                                    console.log(`  ${tag}: [你游的.aaa文件的BUG]不删:`.yellow, filePack.url);
                                    WriteFileAddSync(baseLog, logMain, `  ${tag}+> [你游的.aaa文件的BUG]不删: ${filePack.url}\r\n`, false);
                                }
                                else {
                                    _Fs.unlinkSync(_Path.join(baseDP, filePack.url));
                                    if (_LogDetail)
                                        console.log(`  ${tag}+> 删: `.yellow + filePack.url);
                                    WriteFileAddSync(baseLog, logMain, `  ${tag}+> 删: ${filePack.url}\r\n`, false);
                                    countDelFile++;
                                }
                            }
                            else {
                            }
                        }
                    }
                    else {
                    }
                }
                else {
                }
                if (md5OK == false) {
                    if (hasFile && file_list.length == 1) {
                        console.log(`  ${tag}: 存在，MD5错误，文件就1个。`.yellow, path);
                        WriteFileAddSync(baseLog, logMain, `${tag}[E]: 存在，MD5错误，文件就1个。 ${path}\r\n`, false);
                        countErr1a++;
                        continue;
                    }
                    else {
                        let isFileListHas = true;
                        for (let indexAFL = 0; indexAFL < file_list.length; indexAFL++) {
                            const filePack = file_list[indexAFL];
                            if (_Fs.existsSync(_Path.join(baseDP, filePack.url))) { }
                            else {
                                isFileListHas = false;
                                break;
                            }
                        }
                        if (isFileListHas == false) {
                            if (hasFile) {
                                console.log(`  ${tag}: 存在，MD5错误，文件不齐。`.yellow, path);
                                WriteFileAddSync(baseLog, logMain, `${tag}[E]: 存在，MD5错误，文件不齐。 ${path}\r\n`, false);
                                countErr1++;
                            }
                            else {
                                if (_LogDetail)
                                    console.log(`  ${tag}: 不存在，文件不齐。`.yellow, path);
                                WriteFileAddSync(baseLog, logMain, `${tag}[E]: 不存在，文件不齐。 ${path}\r\n`, false);
                                countErr2++;
                            }
                            continue;
                        }
                        let isFileSizeError = false;
                        let errorFilePack = {};
                        let fileBufferList = [];
                        for (let indexAFL = 0; indexAFL < file_list.length; indexAFL++) {
                            const filePack = file_list[indexAFL];
                            let outFile = _Fs.readFileSync(_Path.join(baseDP, filePack.url));
                            if (filePack.size != outFile.length) {
                                isFileSizeError = true;
                                errorFilePack = filePack;
                            }
                            else {
                                fileBufferList.push(outFile);
                            }
                        }
                        if (isFileSizeError) {
                            console.log(`  ${tag}: 不存在，文件齐，但其中文件的大小有错误！`.yellow, path, '->'.cyan, errorFilePack.url);
                            WriteFileAddSync(baseLog, logMain, `${tag}[E]: 不存在，文件齐，但其中文件的大小有错误！ ${path} -> ${errorFilePack.url}\r\n`, false);
                            countErr3++;
                            continue;
                        }
                        let pathData = _Path.parse(path);
                        let r = WriteBufferFileSync(_Path.join(baseDP, pathData.dir), pathData.base, Buffer.concat(fileBufferList), true);
                        if (r) {
                            WriteFileAddSync(baseLog, logMain, `${tag}: 创建文件。 ${path}\r\n`, false);
                            countOK++;
                            for (let indexAFL = 0; indexAFL < file_list.length; indexAFL++) {
                                const filePack = file_list[indexAFL];
                                if (assetPathList[filePack.url]) {
                                    console.log(`  ${tag}: [你游的.aaa文件的BUG]不删:`.yellow, filePack.url);
                                    WriteFileAddSync(baseLog, logMain, `  ${tag}+> [你游的.aaa文件的BUG]不删: ${filePack.url}\r\n`, false);
                                }
                                else {
                                    _Fs.unlinkSync(_Path.join(baseDP, filePack.url));
                                    WriteFileAddSync(baseLog, logMain, `  ${tag}+> 删: ${filePack.url}\r\n`, false);
                                    countDelFile++;
                                }
                            }
                        }
                        else {
                            countMergeErr++;
                            WriteFileAddSync(baseLog, logMain, `${tag}: 创建文件失败。 ${path}\r\n`, false);
                        }
                    }
                }
            }
            console.log();
        }
        let logStr = `文件数量： ${countFile}\r\n` +
            `OK： ${countOK}\r\n` +
            `存在，MD5错误，文件就1个： ${countErr1a}\r\n` +
            `存在，MD5错误，文件不齐： ${countErr1}\r\n` +
            `不存在，文件不齐： ${countErr2}\r\n` +
            `不存在，文件齐，但其中文件的大小有错误： ${countErr3}\r\n` +
            `文件组合失败： ${countMergeErr}\r\n` +
            `OK后删除的文件数量： ${countDelFile}\r\n` +
            `OK： ${countOK}  +  Err： ${countErr1a + countErr1 + countErr2 + countErr3 + countMergeErr}  ` +
            `=  ${countOK + countErr1a + countErr1 + countErr2 + countErr3 + countMergeErr}\r\n`;
        console.log(logStr);
        WriteFileAddSync(baseLog, logMain, '\r\n' + logStr, false);
        console.log('[数据包整合]结束。'.green);
    }
    catch (err) {
        console.log('[数据包整合]出错：'.red, err);
    }
};
const _AssetData = {};
exports.mgoDownloadPack_AssetCache = function (url, data, statusCode) {
    try {
        if (statusCode != 304 && statusCode != 200)
            return;
        if (url.indexOf('/magica/resource/download/asset/master/asset_') == -1) {
            return;
        }
        let urlParse = _Url.parse(url);
        let pathParse = _Path.parse(urlParse.pathname);
        if (pathParse.base.indexOf('asset_scenario_') == 0)
            return;
        if (pathParse.base.indexOf('asset_config.json') == 0)
            return;
        if (pathParse.base.indexOf('asset_movie_low.json') == 0) {
            console.log(`[数据包asset]asset_movie_low低清数据包先无视...`.yellow);
            return;
        }
        if (pathParse.name.indexOf('asset_') == 0) {
            if (statusCode == 304) {
                let path = pathParse.dir;
                let file = pathParse.base.replace('.json.gz', '.json');
                data = ReadFileSync(_Path.join(_Base, path), file);
                if (!data) {
                    console.log(`[数据包asset]本地文件没有数据或没有文件： ${path}/${file}`.magenta);
                    return;
                }
                data = data.toString();
            }
            else if (data instanceof Buffer) {
                data = data.toString();
            }
            let jsonArr = JSON.parse(data);
            if (jsonArr instanceof Array) {
                for (let index = 0; index < jsonArr.length; index++) {
                    const obj = jsonArr[index];
                    _AssetData[obj.path] = obj;
                }
                if (_LogDetail)
                    console.log(`[数据包asset]加载完成(${jsonArr.length})：${pathParse.base}`.cyan);
            }
            else {
                console.log(`[数据包asset]数据异常：`.magenta, pathParse.base, (typeof (jsonArr)).yellow);
            }
        }
        else {
            console.log(`[数据包asset]文件名异常：`.magenta, pathParse.base);
        }
    }
    catch (err) {
        console.log('[数据包asset]出错：', err);
    }
};
const _CacheDownloadPackBuffers = [];
function downloadPackCacheBuffers(assetObjPath) {
    if (assetObjPath) {
        for (let index = 0; index < _CacheDownloadPackBuffers.length; index++) {
            const item = _CacheDownloadPackBuffers[index];
            if (item.path == assetObjPath) {
                return item.buff;
            }
        }
    }
    return undefined;
}
function downloadPackCacheBuffers_Cache(assetObjPath, buff) {
    if (assetObjPath && buff) {
        for (let index = 0; index < _CacheDownloadPackBuffers.length; index++) {
            const item = _CacheDownloadPackBuffers[index];
            if (item.path == assetObjPath) {
                _CacheDownloadPackBuffers[index].buff = buff;
                return;
            }
        }
        if (_CacheDownloadPackBuffers.length >= 6) {
            _CacheDownloadPackBuffers.shift();
        }
        _CacheDownloadPackBuffers.push({
            path: assetObjPath,
            buff: buff
        });
    }
}
function downloadPackCacheBuffers_Clear() {
    while (_CacheDownloadPackBuffers.length)
        _CacheDownloadPackBuffers.shift();
}
exports.mgoDownloadPackGetLFlie = function (url) {
    try {
        if (url.indexOf('/magica/api/page/MyPage') == 0) {
            downloadPackCacheBuffers_Clear();
            console.log('[返回缓存数据包]清空缓存数据包Buffer。');
            return;
        }
        if (url.indexOf(_SC_downloadPackUrl_S) == -1) {
            return;
        }
        let urlParse = _Url.parse(url);
        let pathParse = _Path.parse(urlParse.pathname);
        let queryMD5 = urlParse.query;
        let assetPath;
        let assetObj;
        const assetUrlPath = urlParse.pathname.replace(_SC_downloadPackUrl_S, '');
        if (_AssetData[assetUrlPath]) {
            assetObj = _AssetData[assetUrlPath];
            assetPath = assetUrlPath;
        }
        if (!assetObj) {
            let fileName;
            let fileNameFull = pathParse.base;
            let fileNames = fileNameFull.split('.');
            if (fileNames.length >= 0 && fileNames.length <= 2) {
                fileName = fileNameFull;
            }
            else {
                fileName = fileNames[0];
                for (let index = 1; index < fileNames.length - 1; index++) {
                    fileName += '.' + fileNames[index];
                }
            }
            let temp2Path = _Path.join(assetUrlPath, '..', fileName).replace(/\\/g, '/');
            if (_AssetData[temp2Path]) {
                assetObj = _AssetData[temp2Path];
                assetPath = temp2Path;
            }
            else {
                if (temp2Path.indexOf('scenario/json/adv/scenario_') == 0)
                    return;
                if (temp2Path.indexOf('movie/char/high/movie') == 0) {
                    temp2Path = temp2Path.replace('movie/char/high/movie', 'movie/char/movie');
                    if (_AssetData[temp2Path]) {
                        assetObj = _AssetData[temp2Path];
                        assetPath = temp2Path;
                    }
                }
                if (!assetPath) {
                    console.log('[返回缓存数据包]Asset没有找到对应数据！！'.magenta.bgYellow, temp2Path, fileName.yellow, pathParse.base);
                    return;
                }
            }
        }
        if (!assetObj) {
            console.log('[返回缓存数据包]Asset没有找到对应数据？：'.magenta.bgYellow, urlParse.pathname);
            return;
        }
        else if (!assetObj.md5 || !assetObj.file_list || !assetObj.path) {
            console.log('[返回缓存数据包]Asset数据异常？：'.magenta.bgYellow, urlParse.pathname, assetObj);
            return;
        }
        if (queryMD5 && assetObj.md5 != queryMD5) {
            console.log('[返回缓存数据包]queryMD5不同。'.yellow, assetObj.md5, queryMD5);
            return;
        }
        let fileBuff;
        let assetObjBuffer = downloadPackCacheBuffers(assetObj.path);
        if (assetObjBuffer) {
            fileBuff = assetObjBuffer;
        }
        else {
            let fullFile = _Path.join(_Base, _SC_downloadPackUrl_L, assetObj.path);
            if (_Fs.existsSync(fullFile))
                fileBuff = _Fs.readFileSync(fullFile);
        }
        if (!fileBuff) {
            console.log('[返回缓存数据包]文件不存在：'.yellow, assetObj.path);
            return;
        }
        let md5Crypto = _Crypto.createHash('md5');
        let outMd5 = md5Crypto.update(fileBuff).digest('hex');
        if (outMd5 != assetObj.md5) {
            console.log('[返回缓存数据包]MD5错误：'.yellow, assetObj.path);
            if (_LogDetail) {
                console.log('   当前MD5:', outMd5);
                console.log('   正确MD5:', assetObj.md5);
            }
            return;
        }
        if (assetObj.file_list.length == 1) {
            return fileBuff;
        }
        else {
            if (!assetObjBuffer) {
                downloadPackCacheBuffers_Cache(assetObj.path, fileBuff);
            }
            let fileStart = 0;
            for (let index = 0; index < assetObj.file_list.length; index++) {
                const item = assetObj.file_list[index];
                if (assetUrlPath != item.url) {
                    fileStart += item.size;
                }
                else {
                    var rBuff = fileBuff.slice(fileStart, fileStart + item.size);
                    return rBuff;
                }
            }
            console.log('[返回缓存数据包]找不到file_list中的对应段落？：'.magenta.bgYellow, assetUrlPath, assetObj.file_list);
            return;
        }
    }
    catch (err) {
        console.log('[返回缓存数据包]出错：'.red, err);
    }
    return;
};
