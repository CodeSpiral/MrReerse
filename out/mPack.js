"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const _Http2_Experimental = require("http2");
const { HTTP2_HEADER_METHOD, HTTP2_HEADER_PATH, HTTP2_HEADER_STATUS, HTTP2_HEADER_CONTENT_TYPE, HTTP2_HEADER_AUTHORITY } = _Http2_Experimental.constants;
const _Zlib = require("zlib");
const _Path = require("path");
const _Fs = require("fs");
const _Url = require("url");
const _Crypto = require("crypto");
const _AppConfig = require("./config");
const _Mgo = require("./mgo");
const _LogDetail = false;
const _IsDev = _AppConfig.IsDev();
const _Base = _AppConfig.BasePath_Base();
const _BaseMagica = _AppConfig.BasePath_Magica;
const _Protocol = 'https:';
const _Domain = 'android.magi-reco.com';
const _ProtocolHostname = `${_Protocol}//${_Domain}`;
const _BaseDownloadAssetPath = '/magica/resource/download/asset/master/';
const _AssetJson = [
    'asset_config.json',
    'asset_char_list.json',
    'asset_main.json',
    'asset_voice.json',
    'asset_movie_high.json',
    'asset_fullvoice.json',
];
const _Http2Options = {
    rejectUnauthorized: false,
};
function https2Request_Experimental(protocolHostname, path, headers, cb) {
    const urlPath = `${path}?${parseInt(Date.now() / 1000)}`;
    const url = `${protocolHostname}${urlPath}`;
    const urlParse = _Url.parse(url);
    let client = _Http2_Experimental.connect(url, _Http2Options)
        .on('error', (err) => { console.error(`[DLPack][Http2]err: `.red, err); })
        .on('socketError', (err) => { console.error(`[DLPack][Http2]err: `.red, err); });
    var isCB = false;
    headers[':authority'] = urlParse.hostname;
    headers[':path'] = urlPath;
    headers[':scheme'] = urlParse.protocol;
    if (_LogDetail)
        console.log('[DLPack]'.red, urlPath);
    var zlibType;
    var resConLen;
    var resHeadersObj;
    const req = client.request(headers);
    req.on('response', (resHeaders, flags) => {
        var statusCode = resHeaders[HTTP2_HEADER_STATUS];
        zlibType = resHeaders['content-encoding'] || '';
        resConLen = resHeaders['content-length'];
        resHeadersObj = resHeaders;
        if (statusCode != '200') {
            if (!isCB && typeof cb === 'function')
                isCB = true, cb(url, resHeadersObj, undefined);
        }
    });
    let dataBuf;
    let dataStr;
    req.on('data', function (chunk) {
        if (typeof chunk === 'string')
            dataStr = dataStr ? (dataStr + chunk) : chunk;
        else
            dataBuf = Buffer.concat([(dataBuf ? dataBuf : Buffer.alloc(0)), chunk]);
    }).on('end', () => {
        if (_LogDetail)
            console.log('[DLPack][OVER]'.yellow, `${(zlibType || 'zlibType[NULL]')}`.cyan, path);
        var rBody = dataStr || dataBuf;
        if (dataBuf) {
            try {
                switch (zlibType) {
                    case 'gzip':
                        rBody = _Zlib.gunzipSync(dataBuf);
                        break;
                    case 'deflate':
                        rBody = _Zlib.inflateSync(dataBuf);
                        break;
                }
            }
            catch (err) {
                console.log('[DLPack][ERROR]'.yellow, `${(zlibType || '')}`.cyan, err);
            }
        }
        if (dataBuf && dataBuf.length != resConLen) {
            console.log(`[DLPack]resConLen(Buffer):${resConLen} != ${dataBuf.length}`.red.bgYellow);
            rBody = undefined;
        }
        else if (dataStr && dataStr.length != resConLen) {
            console.log(`[DLPack]resConLen(String):${resConLen} != ${dataStr.length}`.red.bgYellow);
            rBody = undefined;
        }
        if (!isCB && typeof cb === 'function')
            isCB = true, cb(url, resHeadersObj, rBody);
        if (!client.destroyed)
            client.destroy();
    }).on('error', (err) => {
        console.error(`[DLPack][Http2-request]err: `.red, err);
    });
}
function asyncPromise(act) {
    return new Promise(function (resovle, reject) {
        setTimeout(function () {
            act(function () { resovle(); });
        }, 1);
    });
}
;
function mPackGetDownloadPack(checkVer, checkVerAfterDL) {
    console.log('[下载数据包]开始。');
    const basePath = _Path.join(_Base, _BaseDownloadAssetPath);
    const baseDP = _Path.join(_Base, _Mgo.mgo_SC_downloadPackUrl_L());
    let okAssetList = [];
    let checkVerAfterDL_do = false;
    (function () {
        return __awaiter(this, void 0, void 0, function* () {
            for (let index = 0; index < _AssetJson.length; index++) {
                const assetJsonName = _AssetJson[index];
                let urlPath = `${_BaseDownloadAssetPath}${assetJsonName}?${parseInt(Date.now() / 1000)}`;
                if (checkVerAfterDL_do == true) {
                    okAssetList.push(assetJsonName);
                    continue;
                }
                yield asyncPromise(function (resovleFunc) {
                    https2Request_Experimental(_ProtocolHostname, urlPath, {
                        ':method': 'GET',
                        'accept-encoding': 'gzip',
                        'content-type': 'application/json; charset=utf-8',
                        'x-platform-host': _ProtocolHostname,
                    }, function (url, resHeadersObj, rBody) {
                        if (rBody) {
                            if ('asset_config.json' == assetJsonName) {
                                if (checkVer) {
                                    let jsonObj = _Mgo.mgo_ReadJsonFileSync(basePath, assetJsonName);
                                    try {
                                        let jsonObjNew = JSON.parse(rBody.toString());
                                        if (jsonObj.version && jsonObjNew.version && jsonObjNew.version == jsonObj.version) {
                                            console.log(`[DLPack]文件asset_config版本一样。version: ${jsonObjNew.version}`.cyan, jsonObj.version);
                                            if (checkVerAfterDL == true) {
                                                checkVerAfterDL_do = true;
                                                console.log('checkVerAfterDL_do = T');
                                            }
                                            else {
                                                console.log('checkVerAfterDL -> return; ');
                                                return;
                                            }
                                        }
                                    }
                                    catch (error) {
                                        console.log('[DLPack]文件asset_config转JSON错误！停止操作！'.red.yellow);
                                        return;
                                    }
                                }
                            }
                            else {
                                okAssetList.push(assetJsonName);
                                console.log('  <- ' + assetJsonName);
                            }
                            _Mgo.mgoAutoSaveFile(url, resHeadersObj, rBody);
                        }
                        else {
                            console.log('没有body数据...');
                        }
                        resovleFunc();
                    });
                });
            }
            console.log(okAssetList);
            let assetAllList = [];
            okAssetList.forEach(assetFile => {
                let jsonObj = _Mgo.mgo_ReadJsonFileSync(basePath, assetFile);
                if (jsonObj instanceof Array) {
                    assetAllList.push(jsonObj);
                    console.log('  -> ' + assetFile);
                }
            });
            for (let indexAAL = 0; indexAAL < assetAllList.length; indexAAL++) {
                const assetList = assetAllList[indexAAL];
                console.log(`\r\n  ->[${indexAAL}][${okAssetList[indexAAL]}]: ${assetList.length}\r\n`);
                for (let indexAL = 0; indexAL < assetList.length; indexAL++) {
                    const asset = assetList[indexAL];
                    let tag = `[${indexAAL}-${indexAL}]`;
                    let file_list = asset.file_list;
                    let md5 = asset.md5;
                    const path = asset.path;
                    let localFile = _Path.join(baseDP, path);
                    let md5OK = false;
                    let hasFile = _Fs.existsSync(localFile);
                    let fileAndMd5Ok = false;
                    if (hasFile) {
                        let file = _Fs.readFileSync(localFile);
                        const md5Crypto = _Crypto.createHash('md5');
                        let outMd5 = md5Crypto.update(file).digest('hex');
                        fileAndMd5Ok = (outMd5 == md5);
                    }
                    else {
                        fileAndMd5Ok = false;
                    }
                    if (fileAndMd5Ok == false) {
                        for (let indexAFL = 0; indexAFL < file_list.length; indexAFL++) {
                            const filePack = file_list[indexAFL];
                            let filePackUrl = filePack.url;
                            let filePackUrlPath = `${_BaseDownloadAssetPath}resource/${filePackUrl}?${md5}`;
                            yield asyncPromise(function (resovleFunc) {
                                https2Request_Experimental(_ProtocolHostname, filePackUrlPath, {
                                    ':method': 'GET',
                                    'accept-encoding': 'gzip',
                                    'content-type': 'application/json; charset=utf-8',
                                    'x-platform-host': _ProtocolHostname,
                                }, function (url, resHeadersObj, rBody) {
                                    if (rBody) {
                                        _Mgo.mgoAutoSaveFile(url, resHeadersObj, rBody, true);
                                    }
                                    else {
                                        console.log('没有body数据...');
                                    }
                                    resovleFunc();
                                });
                            });
                            if (file_list.length > 1 && indexAFL === file_list.length - 1) {
                                let fileBufferList = [];
                                for (let indexAFL2 = 0; indexAFL2 < file_list.length; indexAFL2++) {
                                    const filePack = file_list[indexAFL2];
                                    if (_Fs.existsSync(_Path.join(baseDP, filePack.url)) == false) {
                                        console.log('[DLPack]缺文件...:'.red.bgYellow, filePack.url, asset);
                                        break;
                                    }
                                    let outFile = _Fs.readFileSync(_Path.join(baseDP, filePack.url));
                                    if (filePack.size != outFile.length) {
                                        console.log('[DLPack]文件长度异常！:'.red.bgYellow, filePack.url, asset);
                                        break;
                                    }
                                    else {
                                        fileBufferList.push(outFile);
                                    }
                                }
                                let mFileBuffer = Buffer.concat(fileBufferList);
                                const mFileBufferMd5 = _Crypto.createHash('md5').update(mFileBuffer).digest('hex');
                                if (mFileBufferMd5 != md5) {
                                    console.log('[DLPack]合并文件MD5错误！:'.red.bgYellow, mFileBufferMd5, md5);
                                }
                                else {
                                    let pathData = _Path.parse(path);
                                    let r = _Mgo.mgoWriteBufferFileSync(_Path.join(baseDP, pathData.dir), pathData.base, mFileBuffer, true);
                                    if (r) {
                                        console.log('[DLPack]写文件结束:'.green, pathData.base);
                                    }
                                    else {
                                        console.log('[DLPack]文件写入异常...'.red.bgYellow, filePack.url, asset);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            yield asyncPromise(function (resovleFunc) {
                console.log('[DLPack]结束');
                resovleFunc();
            });
        });
    })();
}
exports.mPackGetDownloadPack = mPackGetDownloadPack;
