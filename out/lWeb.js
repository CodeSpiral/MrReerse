"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Http = require("http");
const _Path = require("path");
const _Fs = require("fs");
const _Url = require("url");
const _Zlib = require("zlib");
const _Mgo = require("./mgo");
const _AppConfig = require("./config");
const _WebTranslate = require("./lWebTranslate");
const _C_WebMemoriaImgResponse = _AppConfig.WebMemoriaImgResponse();
const _Port = _AppConfig.WebPort();
const _LogDetail = false;
const _IsDev = _AppConfig.IsDev();
const _BaseHtml = _AppConfig.BasePath_Html();
const _BaseMagicaExtendData = _AppConfig.BasePath_MagicaExtend();
const _BaseMagica = _AppConfig.BasePath_Magica();
console.log(`[服务器]路径： ${_BaseHtml}`);
console.log(`[服务器]数据： ${_BaseMagicaExtendData}  （该文件夹记录了用户的数据，请勿共享！）`);
const _R = {
    '/drop': 'drop.html',
    '/piece': 'piece.html',
};
const _R_3 = {
    '/d': { c: 302, url: 'drop' },
    '/p': { c: 302, url: 'piece' },
};
const _PostUrl_drop = '/drop/post';
const _PostUrl_dropUBL = '/drop/postUBL';
const _PostUrl_piece = '/piece/postPiece';
const _PostUrl_l2d_base = '/l2d/post/';
const _PostUrl_l2d = {
    'bgList': 'reqPost_l2dBgList',
    'l2dList': 'reqPost_l2dList',
};
const _ApiUrl_CloseServer = '/api/CloseServer?true';
console.log(`[服务器]页面: 记忆结晶  -> http://localhost:${_Port}/piece`);
console.log(`[服务器]页面: 关卡&掉落 -> http://localhost:${_Port}/drop`);
const _Server = _Http.createServer(function (req, res) {
    if (req.method == 'POST') {
        console.log(`[本地服务器][${req.method}]: ${req.url}`);
        let dataBuf;
        let dataStr;
        req.on('data', function (chunk) {
            if (typeof chunk === 'string') {
                dataStr = dataStr ? (dataStr + chunk) : chunk;
            }
            else {
                dataBuf = Buffer.concat([(dataBuf ? dataBuf : Buffer.alloc(0)), chunk]);
            }
        }).on('end', function () {
            let data;
            if (dataBuf) {
                data = dataBuf.toString();
            }
            else {
                data = dataStr;
            }
            if (data) {
                reqPost(req, res, data);
            }
            else {
                res.end('404: no data');
            }
        });
    }
    else if (req.method == 'OPTIONS') {
        console.log(`[本地服务器][${req.method}]: ${req.url}`);
        reqOPTIONS(req, res);
        return;
    }
    else {
        reqGet(req, res);
    }
}).on('error', function (err) {
    console.warn('[本地服务器]错误'.red, err);
}).listen(_Port, function (err) {
    if (err) {
        console.warn('[本地服务器]错误'.red, err);
    }
    else {
        console.log(`[本地服务器]地址： http://127.0.0.1:${_Port}`.bgBlue.yellow);
    }
});
function ServerClose() {
    console.log('关闭本地服务器。');
    _Server.close();
}
var _NoImg_xpng;
var _NoImg_xpng_header;
function reqGet(req, res) {
    let path = req.url;
    if (path && path !== '/') {
        if (reqGet_API(req, res)) {
            return;
        }
        let urlParse = _Url.parse(path);
        let pathParse = _Path.parse(urlParse.pathname);
        let nowPath = urlParse.pathname;
        if (_R_3[nowPath]) {
            let conf = _R_3[nowPath];
            res.writeHead(conf.c, { 'Location': conf.url });
            res.end(`${conf.c}: ${conf.url}`);
            return;
        }
        if (_R[nowPath]) {
            nowPath = _R[nowPath];
            pathParse = _Path.parse(nowPath);
        }
        var filePath = '';
        if (_Fs.existsSync(_Path.join(_BaseHtml, nowPath))) {
            filePath = _Path.join(_BaseHtml, nowPath);
        }
        else if (_Fs.existsSync(_Path.join(_BaseMagicaExtendData, nowPath))) {
            filePath = _Path.join(_BaseMagicaExtendData, nowPath);
        }
        else if (_Fs.existsSync(_Path.join(_BaseMagica, nowPath))) {
            filePath = _Path.join(_BaseMagica, nowPath);
        }
        else if (_IsDev && path.indexOf('/magica/') === 0 && _Fs.existsSync(_Path.join(_BaseMagica, '..', nowPath))) {
            filePath = _Path.join(_BaseMagica, '..', nowPath);
        }
        else {
            switch (pathParse.ext) {
                case '.png':
                    if (_NoImg_xpng === undefined) {
                        if (_Fs.existsSync(_Path.join(_BaseHtml, 'x.png'))) {
                            _NoImg_xpng = _Fs.readFileSync(_Path.join(_BaseHtml, 'x.png'));
                            if (_NoImg_xpng) {
                                _NoImg_xpng = _Zlib.gzipSync(_NoImg_xpng);
                                _NoImg_xpng_header = createHeader('image/png', { 'content-encoding': 'gzip' });
                            }
                            else {
                                _NoImg_xpng_header = createHeader('image/png', {});
                            }
                        }
                        else {
                            _NoImg_xpng = false;
                        }
                    }
                    if (_NoImg_xpng) {
                        res.writeHead(200, _NoImg_xpng_header);
                        res.end(_NoImg_xpng);
                    }
                    else {
                        res.writeHead(404);
                        res.end('404: ' + path);
                    }
                    return;
                default: break;
            }
        }
        if (filePath) {
            let stat = _Fs.statSync(filePath);
            if (stat.isDirectory) {
                if (_Fs.existsSync(_Path.join(filePath, 'index.html'))) {
                    if (nowPath[nowPath.length - 1] !== '/') {
                        res.writeHead(302, { 'Location': nowPath + '/' });
                        res.end(`302: ${nowPath}/`);
                        return;
                    }
                    filePath = _Path.join(filePath, 'index.html');
                    pathParse = _Path.parse(filePath);
                }
            }
            let contentType = '';
            let header = {
                'Cache-Control': ''
            };
            let useGzip = true;
            switch (pathParse.ext) {
                case '.txt':
                    contentType = 'text/plain';
                    break;
                case '.json':
                    contentType = 'application/json';
                    break;
                case '.js':
                    contentType = 'application/javascript';
                    break;
                case '.css':
                    contentType = 'text/css';
                    break;
                case '.png':
                    contentType = 'image/png';
                    if (reqGet_memoriaImg_CanSend(filePath) == false) {
                        res.writeHead(403, createHeader(contentType, header));
                        res.end('[file]403: ' + path);
                        return;
                    }
                    header['Cache-Control'] = 'max-age=604800';
                    break;
                case '.html':
                    contentType = 'text/html; charset=utf-8';
                    break;
                case '.jpg':
                    contentType = 'image/jpeg';
                    break;
                case '.ico':
                    contentType = 'image/x-icon';
                    header['Cache-Control'] = 'max-age=6048000';
                    break;
                default:
                    useGzip = false;
                    break;
            }
            _Fs.readFile(filePath, function (err, data) {
                if (useGzip) {
                    header['content-encoding'] = 'gzip';
                }
                if (contentType)
                    res.writeHead(200, createHeader(contentType, header));
                if (useGzip) {
                    var rD = _Zlib.gzipSync(data);
                    res.end(rD);
                }
                else {
                    res.end(data);
                }
            });
        }
        else {
            res.writeHead(404);
            res.end('404: ' + path);
        }
    }
    else {
        res.writeHead(200);
        res.end('._.');
    }
}
function reqGet_API(req, res) {
    let path = req.url;
    if (_WebTranslate.WebTranslate_UrlRouter(path, req, res)) {
        return true;
    }
    else {
        return false;
    }
}
function createHeader(contentType, header) {
    var obj = { 'Content-Type': contentType };
    for (const key in header) {
        if (header[key]) {
            obj[key] = header[key];
        }
    }
    return obj;
}
function createHeaderOrigin(contentType, header, req) {
    header['Access-Control-Allow-Origin'] = req.headers['origin'];
    header['Access-Control-Allow-Headers'] = 'content-type';
    header['Access-Control-Allow-Methods'] = 'POST';
    return createHeader(contentType, header);
}
function reqUrlCheck(path) {
    if (_IsDev && path.indexOf(_PostUrl_l2d_base) === 0) {
        var u = path.substring(_PostUrl_l2d_base.length);
        if (!u)
            return false;
        for (const key in _PostUrl_l2d) {
            if (u === key) {
                var txtFuncName = _PostUrl_l2d[key];
                try {
                    if (/^[0-9a-zA-Z_]+$/.exec(txtFuncName) && eval(`typeof ${txtFuncName} === 'function'`)) {
                        return eval(txtFuncName);
                    }
                    else {
                        return false;
                    }
                }
                catch (error) {
                    return false;
                }
            }
        }
    }
    return false;
}
function reqOPTIONS(req, res) {
    let path = req.url;
    if (!(path && path !== '/')) {
        res.end('404: ' + path);
        return;
    }
    if (path == _PostUrl_drop || path == _PostUrl_dropUBL || _PostUrl_piece) {
        res.writeHead(200, createHeaderOrigin('application/json', {}, req));
        res.end('over');
        return;
    }
    res.end('404: ' + path);
}
function reqPost(req, res, data) {
    let path = req.url;
    if (path == _PostUrl_drop) {
        var r = reqPost_drop(data);
        res.writeHead(200, createHeaderOrigin('application/json', {}, req));
        res.end(r || 'over');
    }
    else if (path == _PostUrl_dropUBL) {
        var r = reqPost_dropUBL(data);
        res.writeHead(200, createHeaderOrigin('application/json', {}, req));
        res.end(r || 'over');
    }
    else if (path == _PostUrl_piece) {
        let r = reqPost_pieceData(data);
        res.writeHead(200, createHeaderOrigin('application/json', {}, req));
        res.end(r || 'over');
    }
    else {
        let rFunc = reqUrlCheck(path);
        if (typeof rFunc === 'function') {
            let r = rFunc(data);
            res.writeHead(200, createHeaderOrigin('application/json', {}, req));
            res.end(r || 'over');
            return;
        }
        res.end('404[post]: ' + path);
    }
}
function reqPost_toJson(data) {
    try {
        return JSON.parse(data);
    }
    catch (err) {
        return false;
    }
}
var _FLAG_drop_ing = false;
function reqPost_drop(data) {
    if (_FLAG_drop_ing)
        return 'doing...';
    _FLAG_drop_ing = true;
    try {
        var json = JSON.parse(data);
        for (const userName in json) {
            if (/^[a-zA-Z_0-9]+$/.exec(userName)) {
                const dropData = json[userName];
                _Mgo.WriteFileJson(_Path.join(_BaseMagicaExtendData, _Mgo.LFileConf_watchDropOtherDir()), `drop_${userName}.json`, dropData, true);
                return 'ok';
            }
            else {
                return 'name error.';
            }
        }
    }
    catch (err) {
        console.log('[本地服务器][Post]drop 处理异常'.red);
        console.log(err);
    }
    finally {
        _FLAG_drop_ing = false;
    }
    return 'error';
}
var _FLAG_dropUBL_ing = false;
function reqPost_dropUBL(data) {
    if (_FLAG_dropUBL_ing)
        return 'doing...';
    _FLAG_dropUBL_ing = true;
    try {
        var json = JSON.parse(data);
        if (json && typeof json === 'object') {
            _Mgo.mgoUserSQBL_OnOtherUser(json);
            return 'ok';
        }
        else {
            return 'data unll.';
        }
    }
    catch (err) {
        console.log('[本地服务器][Post]drop 处理异常'.red);
        console.log(err);
    }
    finally {
        _FLAG_dropUBL_ing = false;
    }
    return 'data error';
}
var _FLAG_pieceData_doing = false;
function reqPost_pieceData(data) {
    if (_FLAG_pieceData_doing)
        return 'doing...';
    _FLAG_pieceData_doing = true;
    try {
        var json = JSON.parse(data);
        if (json && typeof json === 'object') {
            _Mgo.mgoPieceList_OnOtherUser(json);
            return 'ok';
        }
        else {
            return 'data unll.';
        }
    }
    catch (err) {
        console.log('[本地服务器][Post]piece 处理异常'.red);
        console.log(err);
    }
    finally {
        _FLAG_pieceData_doing = false;
    }
    return 'data error';
}
function reqGet_memoriaImg_CanSend(path) {
    if (path.indexOf('image_native\\memoria\\memoria') > 0) {
        return _C_WebMemoriaImgResponse;
    }
    return true;
}
var _Cache_l2dBgList = undefined;
function reqPost_l2dBgList(data) {
    var json = reqPost_toJson(data);
    if (typeof json !== 'object') {
        return 'data null or error.';
    }
    let refresh = (json.refresh === true);
    if (refresh === true || !_Cache_l2dBgList) {
        console.log('[reqPost_l2dBgList]获取数据');
        const bgPathBase = '/resource/resource/image_native/bg/story/';
        let result = [];
        try {
            let dirPath = _Path.join(_BaseMagica, bgPathBase);
            let files = _Fs.readdirSync(dirPath);
            files.forEach((val, index) => {
                if (val.indexOf('.jpg') === val.length - 4) {
                    let stats = _Fs.statSync(_Path.join(dirPath, val));
                    if (stats.isFile())
                        result.push(val);
                }
            });
        }
        catch (err) {
            console.log('[ERROR][reqPost_bgList]: '.bgYellow.red);
            console.log(err);
        }
        _Cache_l2dBgList = {
            base: bgPathBase,
            list: result
        };
    }
    return JSON.stringify(_Cache_l2dBgList);
}
var _Cache_l2dList = undefined;
function reqPost_l2dList(data) {
    var json = reqPost_toJson(data);
    if (typeof json !== 'object') {
        return 'data null or error.';
    }
    let refresh = (json.refresh === true);
    if (refresh === true || !_Cache_l2dList) {
        console.log('[reqPost_l2dBgList]获取数据');
        const bgPathBase = '/resource/resource/image_native/live2d/';
        let result = [];
        try {
            let dirPath = _Path.join(_BaseMagica, bgPathBase);
            let files = _Fs.readdirSync(dirPath);
            files.forEach((val, index) => {
                if (/^[0-9]+$/.exec(val)) {
                    let stats = _Fs.statSync(_Path.join(dirPath, val));
                    if (stats.isDirectory())
                        result.push(val);
                }
            });
        }
        catch (err) {
            console.log('[ERROR][reqPost_bgList]: '.bgYellow.red);
            console.log(err);
        }
        _Cache_l2dList = {
            base: bgPathBase,
            list: result
        };
    }
    return JSON.stringify(_Cache_l2dList);
}
