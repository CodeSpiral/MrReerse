"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("colors");
const _AppConfig = require("./config");
if (_AppConfig.ReerseServer()) {
    require('./rs');
}
if (_AppConfig.WebServer()) {
    require('./lWeb');
}
function networkAddress() {
    let network = require('os').networkInterfaces();
    console.log('本机地址：'.magenta);
    for (const key in network) {
        if (network.hasOwnProperty(key)) {
            const itemNW = network[key];
            for (let index = 0; index < itemNW.length; index++) {
                const item = itemNW[index];
                console.log(`   [${key}]`.green, item.family, '->'.green, item.address);
            }
        }
    }
}
try {
    if (_AppConfig.NetworkAddressLog())
        networkAddress();
}
catch (err) { }
