"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Path = require("path");
const _Fs = require("fs");
const _Zlib = require("zlib");
const _AppConfig = require("./config");
const _BaseHtml = _AppConfig.BasePath_Html();
const _BaseMagicaExtendData = _AppConfig.BasePath_MagicaExtend();
const _BaseMagica = _AppConfig.BasePath_Magica();
const _AdvFileInOneDir = false;
const _AdvFileDir = '/original/';
const _RouterUrl_base = '/translate/';
const _RouterUrl = {
    'advList': 'req_advList',
};
function urlRouter(path, req, res) {
    if (path.indexOf(_RouterUrl_base) === 0) {
        var key = path.substring(_RouterUrl_base.length);
        if (key && _RouterUrl[key]) {
            var txtFuncName = _RouterUrl[key];
            try {
                if (/^[0-9a-zA-Z_]+$/.exec(txtFuncName) && eval(`typeof ${txtFuncName} === 'function'`)) {
                    try {
                        let func = eval(txtFuncName);
                        return func(req, res);
                    }
                    catch (error) {
                        return true;
                    }
                }
                else {
                    return false;
                }
            }
            catch (error) {
                return false;
            }
        }
    }
    return false;
}
exports.WebTranslate_UrlRouter = urlRouter;
function req_advList(req, res) {
    console.log('[req_advList]获取adv列表');
    const advPathBase = '/resource/resource/scenario/json/adv/scenario_';
    let result = [];
    try {
        var dirList = [];
        if (_AdvFileInOneDir) {
            dirList.push(_Path.join(_BaseMagica, _AdvFileDir));
        }
        else {
            [0, 1, 2, 3, 4, 5].forEach((val, index) => dirList.push(_Path.join(_BaseMagica, advPathBase + val)));
        }
        dirList.forEach(dirPath => {
            let files = _Fs.readdirSync(dirPath);
            files && files.forEach((val, index) => {
                if (val.indexOf('.json') === val.length - 5) {
                    let stats = _Fs.statSync(_Path.join(dirPath, val));
                    if (stats.isFile())
                        result.push(val);
                }
            });
        });
    }
    catch (err) {
        console.log('[ERROR][req_advList]: '.bgYellow.red);
        console.log(err);
    }
    var resText = JSON.stringify(result);
    try {
        var objHeader = {
            'Content-Type': 'application/json',
            'content-encoding': 'gzip',
        };
        res.writeHead(200, objHeader);
        res.end(_Zlib.gzipSync(resText));
    }
    catch (error) {
        console.log('[ERROR][req_advList]: '.bgYellow.red);
        console.log(error);
        if (res.writable)
            res.end();
    }
}
