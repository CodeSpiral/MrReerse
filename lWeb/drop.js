/**
 * 获取
 * @param {*} url 
 * @param {*} cb 
 */
function xhrGet(url, cb) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (typeof cb === 'function') {
                cb(xhr.responseText);
            }
        }
    };
    xhr.open('GET', url);
    xhr.send();
}

function xhrPost(url, postStr, cb) {
    // var data = new FormData();
    // data.append('data', postStr);
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (typeof cb === 'function') {
                cb(xhr.responseText);
            }
        }
        // else {
        //     cb('[Error]:' + xhr.readyState);
        // }
    };
    xhr.open('POST', url);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(postStr);
}

xhrGet('./userBattleList.json', function (txt) {
    var div = document.querySelector('#MainContent');
    if (txt) {
        var jsonObj;
        try {
            jsonObj = JSON.parse(txt);
        } catch (err) {
            div.innerHTML = '异常<br>' + txt;
        }
        if (jsonObj) initRun(jsonObj);
        else {
            div.innerHTML = '没有内容？<br>' + txt;
        }
    }
    else {
        div.innerText = '获取失败！';
    }
});

/** 本地配置 */
var _config = {};
var _localStorageConfig_Prefix = 'DROP';

var _divUpdateTime = document.querySelector('#UpdateTime');
var _divMainContent = document.querySelector('#MainContent');

var _templates_section = document.querySelector('#Templates_section').innerHTML;
document.querySelector('#Templates_section').remove();
var _templates_questBattleLayer = document.querySelector('#Templates_questBattleLayer').innerHTML;
document.querySelector('#Templates_questBattleLayer').remove();
var _templates_questBattle = document.querySelector('#Templates_questBattle').innerHTML;
document.querySelector('#Templates_questBattle').remove();
var _templates_questBattle_dropItem = document.querySelector('#Templates_questBattle_dropItem').innerHTML;//.replace('{{templates-src}}','src');
document.querySelector('#Templates_questBattle_dropItem').remove();

var _idContentDiv = '.id_content';

var _templates_dropTable_Data = document.querySelector('#Templates_dropTable_Data').innerHTML;
document.querySelector('#Templates_dropTable_Data').remove();
var _templates_dropTableTr = document.querySelector('#Templates_dropTableTr').innerHTML;
document.querySelector('#Templates_dropTableTr').remove();
var _templates_dropTable = document.querySelector('#Templates_dropTable').innerHTML;
document.querySelector('#Templates_dropTable').remove();
/** 初回掉落 */
var _templates_dropTable_DataFC = lib_getTemplate('#Templates_dropTable_DataFC');

//#region 无图模式（要刷新页面）
function onchange_codeImgNone(mini) { _config.gConfig_codeImgNone = mini; }
lib_localStorageConfigInit2(_localStorageConfig_Prefix, 'gConfig_codeImgNone', true)
lib_setElement('#checkbox_codeImgNone', _config.gConfig_codeImgNone)
/** 无图模式 */
var _gConfig_codeImgNone = _config.gConfig_codeImgNone;
//#endregion



var _userSList;
var _userQBList;
var _uEventSingleRaidPointList;
var _uEventBranchPointList;
/** 按活动分组 */
var _userSList_event;
// var _userQBList_event;

/** 开始 */
function initRun(data) {
    console.log(data);

    if (data.time) {
        var updateTime = dateToString(new Date(data.time), 'yyyy年MM月dd日 hh:mm:ss');
        _divUpdateTime.innerText = updateTime ? updateTime : '未知';
    }

    _userSList = data.uSectionList;
    _userQBList = data.uQuestBattleList;
    _uEventSingleRaidPointList = data.uEventSingleRaidPointList || {};
    _uEventBranchPointList = data.uEventBranchPointList || {};
    _userSList_event = {};
    for (var keySectionId in _userSList) {
        var section = _userSList[keySectionId];
        if (section.eventId) {
            if (!_userSList_event[section.eventId]) _userSList_event[section.eventId] = [];
            _userSList_event[section.eventId].push(section);
        }
    }
    // _userSList_event[section.eventId] 的内容按 genericIndex 排序

    for (var sId in _userQBList) {
        try {
            for (var key in _userQBList[sId]) {
                createRewardCodeDataSet(_userQBList[sId][key], sId, key);
            }
        } catch (error) {
            console.error(error);
        }
    }

    // 插入自定义的
    var customSection_COMPOSE = {
        questType: 'COMPOSE_EXT',
        sectionId: 900090,
        title: '強化結界（整合）',
    }
    var customQuestBattle_COMPOSE_1 = {
        sectionId: 900090, questBattleId: 9000901, sectionIndex: 1,
        ap: 5, exp: 30, cardExp: 20, baseBondsPt: 10, riche: 1000,
        customExtend: [4000111, 4000211, 4000311, 4000411, 4000511, 4000611],
    }
    var customQuestBattle_COMPOSE_2 = {
        sectionId: 900090, questBattleId: 9000902, sectionIndex: 2,
        ap: 10, exp: 45, cardExp: 30, baseBondsPt: 15, riche: 3000,
        customExtend: [4000112, 4000212, 4000312, 4000412, 4000512, 4000612],
    }
    var customQuestBattle_COMPOSE_3 = {
        sectionId: 900090, questBattleId: 9000903, sectionIndex: 3,
        ap: 15, exp: 60, cardExp: 40, baseBondsPt: 20, riche: 5000,
        customExtend: [4000113, 4000213, 4000313, 4000413, 4000513, 4000613],
    }
    var customQuestBattle_COMPOSE_4 = {
        sectionId: 900090, questBattleId: 9000904, sectionIndex: 4,
        ap: 25, exp: 90, cardExp: 60, baseBondsPt: 30, riche: 10000,
        customExtend: [4000114, 4000214, 4000314, 4000414, 4000514, 4000614],
    }
    _userSList[customSection_COMPOSE.sectionId] = customSection_COMPOSE;
    _userQBList[customSection_COMPOSE.sectionId] = {};
    _userQBList[customSection_COMPOSE.sectionId][customQuestBattle_COMPOSE_1.questBattleId] = customQuestBattle_COMPOSE_1;
    _userQBList[customSection_COMPOSE.sectionId][customQuestBattle_COMPOSE_2.questBattleId] = customQuestBattle_COMPOSE_2;
    _userQBList[customSection_COMPOSE.sectionId][customQuestBattle_COMPOSE_3.questBattleId] = customQuestBattle_COMPOSE_3;
    _userQBList[customSection_COMPOSE.sectionId][customQuestBattle_COMPOSE_4.questBattleId] = customQuestBattle_COMPOSE_4;

    var customSection_MATERIAL = {
        questType: 'MATERIAL_EXT',
        sectionId: 900091,
        title: '覚醒結界（整合）',
    }
    var customQuestBattle_MATERIAL_1 = {
        sectionId: 900091, questBattleId: 9000911, sectionIndex: 1,
        ap: 10, exp: 30, cardExp: 20, baseBondsPt: 10, riche: 1000,
        customExtend: [4000121, 4000221, 4000321, 4000421, 4000521, 4000621],
    }
    var customQuestBattle_MATERIAL_2 = {
        sectionId: 900091, questBattleId: 9000912, sectionIndex: 2,
        ap: 15, exp: 45, cardExp: 30, baseBondsPt: 15, riche: 3000,
        customExtend: [4000122, 4000222, 4000322, 4000422, 4000522, 4000622],
    }
    var customQuestBattle_MATERIAL_3 = {
        sectionId: 900091, questBattleId: 9000913, sectionIndex: 3,
        ap: 20, exp: 60, cardExp: 40, baseBondsPt: 20, riche: 5000,
        customExtend: [4000123, 4000223, 4000323, 4000423, 4000523, 4000623],
    }
    var customQuestBattle_MATERIAL_4 = {
        sectionId: 900091, questBattleId: 9000914, sectionIndex: 4,
        ap: 30, exp: 90, cardExp: 60, baseBondsPt: 30, riche: 10000,
        customExtend: [4000124, 4000224, 4000324, 4000424, 4000524, 4000624],
    }
    _userSList[customSection_MATERIAL.sectionId] = customSection_MATERIAL;
    _userQBList[customSection_MATERIAL.sectionId] = {};
    _userQBList[customSection_MATERIAL.sectionId][customQuestBattle_MATERIAL_1.questBattleId] = customQuestBattle_MATERIAL_1;
    _userQBList[customSection_MATERIAL.sectionId][customQuestBattle_MATERIAL_2.questBattleId] = customQuestBattle_MATERIAL_2;
    _userQBList[customSection_MATERIAL.sectionId][customQuestBattle_MATERIAL_3.questBattleId] = customQuestBattle_MATERIAL_3;
    _userQBList[customSection_MATERIAL.sectionId][customQuestBattle_MATERIAL_4.questBattleId] = customQuestBattle_MATERIAL_4;


    for (var bId in _userSList) {
        var item = _userSList[bId];
        try {
            doSection(item);
        } catch (error) {
            console.error(error);
        }
    }


    var itemList = [];
    var htmlItemList = '';
    for (var key in _rewardCode_DataDisc.Quest) {
        // console.log(key);
        var obj = _rewardCode_DataDisc.Quest[key];
        if (key.indexOf('RICHE_') === 0
            || key.indexOf('ITEM_EVENT_') === 0
            || key.indexOf('ITEM_COMPOSE_ITEM_') === 0
            || key.indexOf('ITEM_CURE_AP') === 0
        ) {
            continue;
        }
        itemList.push(key);
    }
    itemList.sort();
    // console.log(itemList);
    for (var index = 0; index < itemList.length; index++) {
        var key = itemList[index];
        // if (key.indexOf('ITEM_EVENT_') === 0) continue;

        if (key == 'GIFT_501_1' || key == 'GIFT_401_1') {
            // htmlItemList += '<br>'
        }

        htmlItemList += createHtmlStr_dropItemImg(key, _TYPE_rewardCodeIn.Quest, false, true, true);

        if (/* key == 'GIFT_153_1' ||*/ key == 'GIFT_253_1') {
            htmlItemList += '<br>'
        }
    }
    if (_gConfig_codeImgNone) {
        document.querySelector('#ItemList').innerHTML = '';
    }
    else if (htmlItemList) {
        document.querySelector('#ItemList').innerHTML = htmlItemList;
    }


    // 页面打开定位
    setTimeout(function () {
        var sid = 0;
        var bid = 0;
        var m = location.hash.match(/\#s=([0-9]+)&b=([0-9]+)/);
        if (m && m[1]) {
            var id = parseInt(m[1]);
            if (id != NaN) sid = id;

            // if (m[2]) {
            //     id = parseInt(m[2]);
            //     if (id != NaN) bid = id;
            // }
        }
        m = location.hash.match(/\#s=([0-9]+)/);
        if (m && m[1]) {
            var id = parseInt(m[1]);
            if (id != NaN) sid = id;
        }
        // console.log(sid, bid);

        var elem;
        if (sid) {
            var elem = document.querySelector('#MainContent #s' + id + ' > div[data-id]');
            show_Section(sid);
        }
        if (bid && _userQBList[sid]) {
            var questBattleList = _userQBList[sid];
            var questBattle = questBattleList[bid];
            // console.log(questBattleList, questBattle);
            for (var keyId in questBattleList) {
                if (questBattleList.hasOwnProperty(keyId)) {
                    if (keyId != bid) {
                        showHide_QuestBattle(keyId, true);
                    }
                    else {
                        showHide_QuestBattle(keyId);
                        // show_QuestBattle(sid, bid);
                    }
                }
            }
        }

        if (elem) setTimeout(function () { elem.scrollIntoView(); }, 1);
    }, 1);
}


// 图片的基础路径
var _imgSrc = './resource/';
lib_localStorageConfigInit2(_localStorageConfig_Prefix, '_gConfig_codeImgSrc', _imgSrc);
if (_config._gConfig_codeImgSrc)
    _imgSrc = _config._gConfig_codeImgSrc;
else
    _config._gConfig_codeImgSrc = _imgSrc;


// 
/**
 * 掉落物品Code的取得途径
 */
var _TYPE_rewardCodeIn = {
    /** 掉落表 */
    Quest: 'Quest',
    /** 战斗后的掉落 */
    Battle: 'Battle',
}
/** 缓存 */
var _rewardCode_DataDisc = {
    Quest: {},
    Battle: {},
}
/**
 * rewardCode 转 图片路径
 */
function rewardCode_To_Data(rewardCode, inType) {
    if (!inType) {
        console.warn('[rewardCode_To_Data]', rewardCode, inType);
        return undefined;
    }
    var db_rewardCode = _rewardCode_DataDisc[inType];
    if (!db_rewardCode) {
        console.warn('没有对应的数据库？', rewardCode, inType)
        return undefined;
    }

    if (db_rewardCode[rewardCode]) return db_rewardCode[rewardCode];

    var data = {
        rewardCode: rewardCode,
        name: '',
        /** 掉落数量 */
        dropNum: 0,
        imgSrc: '',
        /** 活动掉落 */
        isEvent: false,
        /** 活动掉落 加成 */
        isEventEffect: false,
        /** 是否显示 模拟加成 */
        isEventExt: false,

        idList:{},
    };

    /**
     * 
     * @param {boolean} hasNum     源码是否包含掉落数量
     * @param {string}  imgSrcBase 
     * @param {boolean} isOne      只算一个
     * @param {string}  delPrefix   [img路径创建用]删除的Code的前缀
     */
    function _rewardCode_hasNum(hasNum, imgSrcBase, isOne, delPrefix) {
        if (hasNum) {
            var i = rewardCode.lastIndexOf('_');
            data.name = rewardCode.substring(0, i);
            data.dropNum = parseInt(rewardCode.substring(i + 1));
            if (isOne)
                data.dropNum = 1;
        }
        else {
            data.name = rewardCode;
            data.dropNum = 1;
        }

        var name = data.name;
        if (delPrefix) name = name.replace(delPrefix, '').toLocaleLowerCase();
        data.imgSrc = _imgSrc + imgSrcBase + name + '.png';
        return data.name;
    }

    if (rewardCode.indexOf('ITEM_EVENT_') === 0) { // 活动掉落
        // ITEM_EVENT_TRAINING_1012_1    【来源：掉落表】
        _rewardCode_hasNum(true, 'image_web/item/event/', false, 'ITEM_');
        data.isEvent = true;

        if (_TYPE_rewardCodeIn.Battle) {
            // ITEM_EVENT_DAILYTOWER_1025_EXCHANGE_1_7  【来源：战斗后掉落】
        }
    }
    else if (rewardCode.indexOf('EVENT_') === 0) { // 活动掉落 加成
        // EVENT_DAILYTOWER_1025_EXCHANGE_1  【来源：掉落表】
        _rewardCode_hasNum(false, 'image_web/item/event/');
        data.isEvent = true;

        if (_TYPE_rewardCodeIn.Battle) {
            data.isEventExt = true;
        }
    }
    else if (rewardCode.indexOf('EVENTEFFECT_DROPADD_EVENT_') === 0) { // 活动掉落 加成
        // EVENTEFFECT_DROPADD_EVENT_DAILYTOWER_1014_KEY_7    【来源：战斗后掉落】
        _rewardCode_hasNum(true, 'image_web/item/event/event_', false, 'EVENTEFFECT_DROPADD_EVENT_');
        data.isEvent = true;
        data.isEventEffect = true;
    }
    else if (rewardCode.indexOf('RICHE_') === 0) { // 
        _rewardCode_hasNum(true, 'image_web/item/main/', true);
    }
    else if (rewardCode.indexOf('ITEM_COMPOSE_ITEM_') === 0 || rewardCode.indexOf('ITEM_CURE_') === 0) { // 
        _rewardCode_hasNum(true, 'image_web/item/main/', true, 'ITEM_');
    }
    else if (rewardCode.indexOf('ITEM_') === 0 || rewardCode.indexOf('GIFT_') === 0) { // 
        _rewardCode_hasNum(true, 'resource/image_native/gift/item_');
    }

    if (!data.imgSrc) {
        console.warn(rewardCode, inType);
    }

    // 兼容旧版...
    data.dropCode = data.name;

    db_rewardCode[rewardCode] = data;
    return data;
}

/**
 * @param {boolean} dropNum  写掉落数量
 * @param {boolean} alt      alt写值
 * @param {boolean} title    title写值
 */
function createHtmlStr_dropItemImg(rewardCode, inType, dropNum, alt, title) {
    var data = rewardCode_To_Data(rewardCode, inType);
    if (!data) {
        console.error('[createHtmlStr_QuestBattle_dropItem_img]', rewardCode, data);
        return '';
    }
    var src = data.imgSrc;
    if (!src) return '';
    var html = '<img alt="' + (alt ? rewardCode : '') + '" src="' + src + '" title="' + (title ? rewardCode : '') + '">';
    if (_gConfig_codeImgNone) {
        html = '';
    }
    if (dropNum && data.dropNum > 1) {
        html += '<span class="dropNum">' + data.dropNum + '</span>';
    }
    return html;

}
function createRewardCodeDataSet(item, sId, bId) {
    var list = [];

    // 初回掉落
    if (item.firstClearRewardCode) {
        list.push(item.firstClearRewardCode);
    }
    // 默认        
    if (item.defaultDropItem) {
        var dropItem = item.defaultDropItem;
        var index = 1;
        while (dropItem['rewardCode' + index]) {
            var code = dropItem['rewardCode' + index];
            if (code) list.push(code);
            index++;
        }
    }
    var indexItem = 1;
    while (item['dropItem' + indexItem]) {
        var dropItem = item['dropItem' + indexItem];
        var index = 1;
        while (dropItem['rewardCode' + index]) {
            var code = dropItem['rewardCode' + index];
            if (code) list.push(code);
            index++;
        }
        indexItem++;
    }

    // console.log(list);

    for (var index = 0; index < list.length; index++) {
        var rewardCode = list[index];
        var obj = rewardCode_To_Data(rewardCode, _TYPE_rewardCodeIn.Quest);

        if (obj.idList[sId] == undefined) {
            obj.idList[sId] = {};
        }
        obj.idList[sId][bId] = true;
    }
}

/** 判断是不是 活动掉落的道具（支持加成那种）。 输入的code是不带数量的 */
function codeRewardCode_isEventKey(code, doEventExt) {
    if (doEventExt) {
        if (/^ITEM_EVENT_(?!DAILYTOWER_[0-9]+_EXCHANGE)/.exec(code)) {
            return true;
        } else {
            return false;
        }
    }
    else {
        return code.indexOf('ITEM_EVENT_') === 0 //&& (code.indexOf('_KEY') + 4 == code.length))
    }
}

function doSection(/** section */section) {
    var questType = section.questType;//:"MAIN"
    var sectionId = section.sectionId;
    var today = '';
    var title = section_Title(section);

    var charaName = '';
    var miniTitle = '';
    switch (questType) {
        case 'CHARA':
            charaName = section.charaName + ' (' + section.genericIndex + ')';
            break;
        case 'MAIN':
            miniTitle = '' + (section.genericId - 10) + '-' + section.genericIndex;
            break;
        case 'SUB':
            miniTitle = '' + (section.genericId - 50) + '-' + section.genericIndex;
            break;

        default:
            break;
    }



    // var ap = item.ap;
    // console.log(questTypeToString(questType), sectionId, title, '(' + charaName + ')', ap);
    var html = _templates_section.trim();

    html = html.replace(/\{\{questTypeStr\}\}/g, questTypeToString(questType));
    html = html.replace(/\{\{questType\}\}/g, questType);
    html = html.replace(/\{\{sectionId\}\}/g, sectionId);
    html = html.replace('{{charaName}}', charaName ? charaName : '');
    html = html.replace('{{miniTitle}}', miniTitle ? miniTitle : '');
    html = html.replace('{{miniTitle-hide}}', miniTitle ? '' : 'hide');
    html = html.replace('{{title}}', title);
    // html = html.replace('{{ap}}', ap ? ap : '');


    if (questType == 'MATERIAL'/*觉醒*/ || questType == 'COMPOSE'/*强化*/) {
        switch (section.dayOfTheWeekQuestType) {
            case /*****/ 'MONDAY': today = 1; break;
            case /****/ 'TUESDAY': today = 2; break;
            case /**/ 'WEDNESDAY': today = 3; break;
            case /***/ 'THURSDAY': today = 4; break;
            case /*****/ 'FRIDAY': today = 5; break;
            case /****/ 'WEEKEND': today = 6; break;
        }
    }
    html = html.replace('{{today}}', today ? 'today' + today : '');
    html = html.replace('{{eventId}}', section.eventId ? 'event' + section.eventId : 'notEvent');



    //#region 输出
    var div = document.createElement('div');
    div.innerHTML = html;
    div = div.children[0];
    div.dataSection = section;
    _divMainContent.appendChild(div);
    //#endregion
}

function doQuestBattleList(itemList) {
    // console.log(itemList);
    if (itemList) {
        var layer = {};
        for (var id in itemList) {
            var item = itemList[id];
            if (layer[item.questBattleType || '-'] == undefined)
                layer[item.questBattleType || '-'] = [];
            layer[item.questBattleType || '-'].push(createDiv_QuestBattle(item));
        }
        // console.log(layer);
        var divContent = document.createElement('div');
        for (var questBattleType in layer) {
            var item = layer[questBattleType];
            var divLayer = createDiv_QuestBattleLayer(questBattleType);
            var divLayerContent = divLayer.querySelector(_idContentDiv);
            for (var index = 0; index < item.length; index++) {
                divLayerContent.appendChild(item[index]);
            }
            divContent.appendChild(divLayer);
        }
        return divContent;
    }
    else {
        var div = document.createElement('div');
        div.innerText = 'NULL';
        return div;
    }
}
function createDiv_QuestBattleLayer(questBattleType) {
    var html = _templates_questBattleLayer.trim();

    html = html.replace(/\{\{questBattleTypeCss\}\}/g, questBattleType);
    html = html.replace(/\{\{questBattleType\}\}/g, questBattleTypeToString(questBattleType));

    var div = document.createElement('div');
    div.innerHTML = html;
    div = div.children[0];
    return div;
}
/** 关卡 */
function createDiv_QuestBattle(item) {
    var html = _templates_questBattle.trim();
    var sectionId = item.sectionId;
    var section = _userSList[sectionId];


    var title = questBattle_Title(item, section);
    html = html.replace('{{sectionIndex}}', title.indexOf('BATTLE') ? item.sectionIndex : '');
    html = html.replace('{{title}}', title);
    html = html.replace(/\{\{sectionId\}\}/g, item.sectionId);
    html = html.replace(/\{\{questBattleId\}\}/g, item.questBattleId);

    var ap = item.ap || section.ap || item.needItemNum || 0;
    var expap = (item.exp / ap);
    html = html.replace('{{ap}}', ap);
    html = html.replace('{{expap}}', (expap % 1 == 0 ? expap : expap.toFixed(2)));

    html = html.replace('{{exp}}', item.exp);
    html = html.replace('{{cardExp}}', item.cardExp);
    html = html.replace('{{baseBondsPt}}', item.baseBondsPt);
    html = html.replace('{{riche}}', item.riche);

    var ccap = (item.riche / ap);
    html = html.replace('{{richeAp}}', (ccap % 1 == 0 ? ccap : ccap.toFixed(2)));

    var parameterTxt = '';
    if (item.parameterMap) {
        if (item.parameterMap.EXPC) {
            var cExp = item.cardExp * parseInt(item.parameterMap.EXPC / 1000);
            parameterTxt += '角色: ' + cExp + '/' + (cExp / ap) + ', ';
        }
        if (item.parameterMap.EP) {
            var cExp = item.baseBondsPt * parseInt(item.parameterMap.EP / 1000);//* 1.5 * 1.2;
            parameterTxt += '短篇: ' + cExp + '/' + (cExp / ap) + ', ';
        }
    }
    html = html.replace('{{parameter}}', item.parameter ? '[' + item.parameter + (parameterTxt ? ' (' + parameterTxt + ')' : '') + ']' : '');

    html = html.replace('{{difficulty}}', item.difficulty || '-');


    var html_dropItems = '';

    html_dropItems += item.firstClearRewardCode ? createHtmlStr_QuestBattle_dropItem({
        dropItemId: '初回掉落',
        rewardCode1: item.firstClearRewardCode,
    }) : '';

    html_dropItems += item.defaultDropItem ? createHtmlStr_QuestBattle_dropItem(item.defaultDropItem) : '';
    var index = 1;
    while (item['dropItem' + index]) {
        html_dropItems += createHtmlStr_QuestBattle_dropItem(item['dropItem' + index]);
        index++;
    }

    if (item.addDropItemId) {
        html_dropItems += createHtmlStr_QuestBattle_dropItem({
            dropItemId: '活动（' + item.addDropNum + '）',
            rewardCode1: item.addDropItemId,
        })
    }

    html = html.replace('{{dropItems}}', html_dropItems);

    var div = document.createElement('div');
    div.innerHTML = html;
    div = div.children[0];
    div.dataQuestBattle = item;
    if (div.children[0]) div.children[0].dataQuestBattle = item;
    return div;
}

//#region 标题处理
/** 大关卡标题 */
function section_Title(section) {
    var questType = section.questType;//:"MAIN"
    var title = section.title;
    if (questType == 'MATERIAL'/*觉醒*/) {
        switch (section.dayOfTheWeekQuestType) {
            case /*****/ 'MONDAY': title += '（周一：暗）'; break;
            case /****/ 'TUESDAY': title += '（周二：火）'; break;
            case /**/ 'WEDNESDAY': title += '（周三：水）'; break;
            case /***/ 'THURSDAY': title += '（周四：木）'; break;
            case /*****/ 'FRIDAY': title += '（周五：光）'; break;
            case /****/ 'WEEKEND': title += '（周末：火,水,木,暗,光）'; break;
        }
    }
    else if (questType == 'COMPOSE'/*强化*/) {
        switch (section.dayOfTheWeekQuestType) {
            case /*****/ 'MONDAY': title += '（周一：火,木）'; break;
            case /****/ 'TUESDAY': title += '（周二：水,光）'; break;
            case /**/ 'WEDNESDAY': title += '（周三：木,暗）'; break;
            case /***/ 'THURSDAY': title += '（周四：光,火）'; break;
            case /*****/ 'FRIDAY': title += '（周五：暗,水）'; break;
            case /****/ 'WEEKEND': title += '（周末：火,水,木,暗,光,CC）'; break;
        }
    }
    else {
        if (questType == 'DAILYTOWER' && /** section.eventId == 1014 && */ section.parameter) {
            switch (section.parameter) {
                case 'DAILYTOWERTYPE=NORMAL':
                    title += " ◆ ストーリークエスト（故事）";
                    break;
                case 'DAILYTOWERTYPE=CHALLENGE':
                    title += " ◆ チャレンジクエスト（挑战）";
                    break;
            }
        }
        else if (questType == 'SINGLERAID') {
            var eventId = section.eventId;
            var sectionId = section.sectionId;
            switch (section.eventId) {
                case 1015: title = '活动「時を超えて鳴らす鐘」';
                    if (_uEventSingleRaidPointList[eventId] && _uEventSingleRaidPointList[eventId][sectionId]) {
                        var srpl = _uEventSingleRaidPointList[eventId][sectionId];
                        // console.log(srpl)
                        var pointType = srpl.pointType;
                        title += srpl.areaNo + '-' + srpl.areaSubNo + ' ' + srpl.title;
                        if (pointType == 'BOSS') {
                            title += ' BOSS';
                        }
                    }
                    break;
                default:
                    if (!title) {
                        if (section.eventId) {
                            title = '活动[' + section.eventId + ']';
                        }
                    }
                    break;
            }
        }
        else if (questType == 'BRANCH') {
            var eventId = section.eventId;
            var sectionId = section.sectionId;
            var qbId = parseInt('' + sectionId + '1');
            switch (section.eventId) {
                case 1017: title = '活动「バイバイ、また明日」';
                    if (_uEventBranchPointList[eventId] && _uEventBranchPointList[eventId][qbId]) {
                        var srpl = _uEventBranchPointList[eventId][qbId];
                        switch (srpl.title) {
                            case 'れいら編':
                                title += section.title + ' <span style="color: #E91E63;">れいら編</span>';
                                break;
                            case 'せいか編':
                                title += section.title + ' <span style="color: #2196F3;">せいか編</span>';
                                break;
                            case 'みと編':
                                title += section.title + ' <span style="color: #0cc206;">みと編</span>';
                                break;
                            default:
                                title += section.title + ' ' + srpl.title;
                                break;
                        }
                    }
                    break;
                default:
                    if (!title) {
                        if (section.eventId) {
                            title = '活动[' + section.eventId + ']';
                        }
                    }
                    break;
            }
        }
    }

    return title;
}
/** 关卡标题 */
function questBattle_Title(questBattle, sectionData) {
    var title = questBattle.title ? (questBattle.title + ' (' + questBattle.sectionIndex + ')') : ('BATTLE ' + questBattle.sectionIndex);

    var questBattleId = questBattle.questBattleId;
    var questType = sectionData.questType;

    if (questType == 'COMPOSE' || questType == 'COMPOSE_EXT' || questType == 'MATERIAL' || questType == 'MATERIAL_EXT') {
        switch (questBattle.sectionIndex) {
            case 1: title = "BATTLE ◆ 初級"; break;
            case 2: title = "BATTLE ◆ 中級"; break;
            case 3: title = "BATTLE ◆ 上級"; break;
            case 4: title = "BATTLE ◆ 超級"; break;
        }
    }
    else if (questType == 'DAILYTOWER') {
        if (sectionData.parameter == 'DAILYTOWERTYPE=NORMAL' /* （故事） */ && questBattle.eventId == 1014) {
            var c = '';
            switch (questBattle.sectionIndex - 1) {
                case 0: c = '①'; break; case 1: c = '②'; break; case 2: c = '③'; break; case 3: c = '④'; break;
                case 4: c = '⑤'; break; case 5: c = '⑥'; break; case 6: c = '⑦'; break; case 7: c = '⑧'; break;
                case 8: c = '⑨'; break; case 9: c = '⑩'; break; case 10: c = '⑪'
            }
            title = sectionData.charaName + '編' + c + ' (' + questBattle.sectionIndex + ')';
        }
        else { // if (sectionData.parameter == 'DAILYTOWERTYPE=CHALLENGE') {
            // （挑战）
            var eventSectionListAll = _userSList_event[questBattle.eventId]; // 可能要genericIndex排序？
            var questBattle_genericIndex = 1;
            // var eventSectionList = [];
            for (var indexS = 0; indexS < eventSectionListAll.length; indexS++) {
                var itemSection = eventSectionListAll[indexS];
                if (itemSection.eventId && itemSection.eventId == sectionData.eventId && itemSection.parameter == sectionData.parameter) {
                    var questBattleList = _userQBList[itemSection.sectionId];
                    // console.log(questBattleId, sectionData.sectionId, itemSection, questBattleList);
                    if (questBattleList) {
                        for (var keyQB in questBattleList) {
                            var itemQuestBattle = questBattleList[keyQB];
                            if (itemQuestBattle.questBattleId == questBattleId) {
                                return 'BATTLE ' + questBattle_genericIndex;
                            }
                            else {
                                questBattle_genericIndex++;
                            }
                        }
                    }
                }
            }
        }
    }
    else if (questType == 'SINGLERAID') {
        var tArr = {
            1: 'BATTLE◆初級',
            2: 'BATTLE◆中級',
            3: 'BATTLE◆上級',
        };
        title = tArr[questBattle.sectionIndex];
    }
    else if (questType == 'ACCOMPLISH') {
        var eventSectionListAll = _userSList_event[questBattle.eventId]; // 可能要genericIndex排序？
        var questBattle_genericIndex = 1;
        // var eventSectionList = [];
        for (var indexS = 0; indexS < eventSectionListAll.length; indexS++) {
            var itemSection = eventSectionListAll[indexS];
            if (itemSection.eventId && itemSection.eventId == sectionData.eventId && itemSection.parameter == sectionData.parameter) {
                var questBattleList = _userQBList[itemSection.sectionId];
                // console.log(questBattleId, sectionData.sectionId, itemSection, questBattleList);
                if (questBattleList) {
                    for (var keyQB in questBattleList) {
                        var itemQuestBattle = questBattleList[keyQB];
                        if (itemQuestBattle.questBattleId == questBattleId) {
                            return 'BATTLE ' + questBattle_genericIndex;
                        }
                        else {
                            questBattle_genericIndex++;
                        }
                    }
                }
            }
        }
    }
    return title;
}
/** 关卡标题 - 短写 */
function questBattle_TitleMini(questBattle, sectionData) {
    var miniTitle = '';
    var questType = sectionData.questType;//:"MAIN"
    var COMPOSE_MATERIAL = { 1: '初级', 2: '中级', 3: '上级', 4: '超级', }
    switch (questType) {
        case 'MAIN':
            miniTitle = '' + (questBattle.questBattleType == 'HARD' ? 'H-' : '')
                + (sectionData.genericId - 10) + '' + sectionData.genericIndex + '' + questBattle.sectionIndex;
            break;
        case 'SUB':
            miniTitle = '支线' + (sectionData.genericId - 50) + '' + sectionData.genericIndex + '' + questBattle.sectionIndex;
            break;
        case 'CHARA':
            miniTitle = sectionData.charaName + ' (' + sectionData.genericIndex + '-' + questBattle.sectionIndex + ')';
            break;

        case 'COMPOSE':
        case 'COMPOSE_EXT':
            miniTitle = '强化 ' + COMPOSE_MATERIAL[questBattle.sectionIndex];
            break;
        case 'MATERIAL':
        case 'MATERIAL_EXT':
            miniTitle = '觉醒 ' + COMPOSE_MATERIAL[questBattle.sectionIndex];
            break;

        case 'EVENT_S':
            var title = sectionData.title || '';
            if (title.indexOf('<ストーリー>') > 0) miniTitle = '故事 ' + questBattle.sectionIndex;
            else if (title.indexOf('<強化>') > 0) miniTitle = '强化 ' + questBattle.sectionIndex;
            else if (title.indexOf('<エピソード>') > 0) miniTitle = '短篇 ' + questBattle.sectionIndex;
            else if (title.indexOf('<エクストラ>') > 0) miniTitle = 'Extra ' + questBattle.sectionIndex;
            else {
                miniTitle = '特训活动 ' + questBattle.sectionIndex;
            }
            break;

        case 'DAILYTOWER':
            if (sectionData.parameter == 'DAILYTOWERTYPE=NORMAL') {
                // （故事）
                if (questBattle.eventId == 1014) {
                    var c = '';
                    switch (questBattle.sectionIndex - 1) {
                        case 0: c = '①'; break; case 1: c = '②'; break; case 2: c = '③'; break; case 3: c = '④'; break;
                        case 4: c = '⑤'; break; case 5: c = '⑥'; break; case 6: c = '⑦'; break; case 7: c = '⑧'; break;
                        case 8: c = '⑨'; break; case 9: c = '⑩'; break; case 10: c = '⑪'
                    }
                    miniTitle = sectionData.charaName.replace(' ', '') + ' ' + questBattle.sectionIndex;
                }
                else {
                    // （故事）
                    var questBattleId = questBattle.questBattleId;
                    var eventSectionListAll = _userSList_event[questBattle.eventId]; // 可能要genericIndex排序？
                    var questBattle_genericIndex = 1;
                    // var eventSectionList = [];
                    for (var indexS = 0; indexS < eventSectionListAll.length; indexS++) {
                        var itemSection = eventSectionListAll[indexS];
                        if (itemSection.eventId && itemSection.eventId == sectionData.eventId && itemSection.parameter == sectionData.parameter) {
                            var questBattleList = _userQBList[itemSection.sectionId];
                            // console.log(questBattleId, sectionData.sectionId, itemSection, questBattleList);
                            if (questBattleList) {
                                for (var keyQB in questBattleList) {
                                    var itemQuestBattle = questBattleList[keyQB];
                                    if (itemQuestBattle.questBattleId == questBattleId) {
                                        return '活动故事 ' + questBattle_genericIndex;
                                    }
                                    else {
                                        questBattle_genericIndex++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (sectionData.parameter == 'DAILYTOWERTYPE=CHALLENGE') {
                // （挑战）
                var questBattleId = questBattle.questBattleId;
                var eventSectionListAll = _userSList_event[questBattle.eventId]; // 可能要genericIndex排序？
                var questBattle_genericIndex = 1;
                // var eventSectionList = [];
                for (var indexS = 0; indexS < eventSectionListAll.length; indexS++) {
                    var itemSection = eventSectionListAll[indexS];
                    if (itemSection.eventId && itemSection.eventId == sectionData.eventId && itemSection.parameter == sectionData.parameter) {
                        var questBattleList = _userQBList[itemSection.sectionId];
                        // console.log(questBattleId, sectionData.sectionId, itemSection, questBattleList);
                        if (questBattleList) {
                            for (var keyQB in questBattleList) {
                                var itemQuestBattle = questBattleList[keyQB];
                                if (itemQuestBattle.questBattleId == questBattleId) {
                                    return '活动高难 ' + questBattle_genericIndex;
                                }
                                else {
                                    questBattle_genericIndex++;
                                }
                            }
                        }
                    }
                }
            }


            break;

        case 'SINGLERAID':
            var tArr = {
                1: '初級',
                2: '中級',
                3: '上級',
            };
            var eventId = sectionData.eventId;
            var sectionId = sectionData.sectionId;
            if (_uEventSingleRaidPointList[eventId] && _uEventSingleRaidPointList[eventId][sectionId]) {
                var srpl = _uEventSingleRaidPointList[eventId][sectionId];
                // console.log(srpl)
                var pointType = srpl.pointType;
                if (pointType == 'BOSS') {
                    miniTitle = '活动' + srpl.areaNo + '-' + srpl.areaSubNo + '-BOSS';
                }
                else {
                    miniTitle = '活动' + srpl.areaNo + '-' + srpl.areaSubNo + '-' + tArr[questBattle.sectionIndex];
                }
            }
            break;

        case 'BRANCH':
            var eventId = sectionData.eventId;
            var questBattleId = questBattle.questBattleId;
            switch (eventId) {
                case 1017: miniTitle = sectionData.title;
                    if (_uEventBranchPointList[eventId] && _uEventBranchPointList[eventId][questBattleId]) {
                        var item = _uEventBranchPointList[eventId][questBattleId];
                        switch (item.title) {
                            case 'れいら編':
                                miniTitle += ' <span style="color: #E91E63;">れいら編</span>';
                                break;
                            case 'せいか編':
                                miniTitle += ' <span style="color: #2196F3;">せいか編</span>';
                                break;
                            case 'みと編':
                                miniTitle += ' <span style="color: #0cc206;">みと編</span>';
                                break;
                            default:
                                miniTitle += ' ' + item.title;
                                break;
                        }
                    }
                    break;
                default:
                    miniTitle = sectionData.title;
                    break;
            }


        default:
            break;
    }

    return miniTitle;
}
//#endregion

/** 掉落项 */
function createHtmlStr_QuestBattle_dropItem(dropItem) {
    var html = _templates_questBattle_dropItem.trim();

    html = html.replace('{{dropItemId}}', dropItem.dropItemId);
    html = html.replace(/\{\{isFC\}\}/g, (dropItem.dropItemId == '初回掉落') ? 'isFC' : '');
    // html = html.replace('{{img}}', '');

    var rewardCode = '';
    var index = 1;
    while (dropItem['rewardCode' + index]) {
        if (rewardCode)
            rewardCode += ' <span class="or">or</span> ';
        var code = dropItem['rewardCode' + index];
        var img = createHtmlStr_dropItemImg(code, _TYPE_rewardCodeIn.Quest, true, true, true);
        rewardCode += img ? img : code;
        index++;
    }
    html = html.replace('{{rewardCode}}', rewardCode);
    return html;
}


/** 全局掉落数据 */
var _g_dropDatabase = {};
/** 全局掉落数据 （其他数据）*/
var _g_dropDatabase_other = {
    // userName: {
    //     v: 1,
    //     d: {}
    // }
};

lib_localStorageConfigInit2(_localStorageConfig_Prefix, '_g_dropFCConfig', {});
var _g_dropFCConfig = _config._g_dropFCConfig;

// var _txt_statistics_QuestBattle;
/**
 * 掉落统计
 */
function statistics_QuestBattle(txt, questBattleId, sectionId) {
    // console.log(txt);    
    // _txt_statistics_QuestBattle = txt;
    function html_dropItemsNull(txtStr, questBattleId, sectionId) {
        var t = '<div class="drop-items-null" onclick="onclick_selectQuestBattle(this)" data-id="'
            + questBattleId + '" data-sid="' + sectionId + '">没有掉落数据 ' + (txtStr ? txtStr : '') + '</div>';
        document.querySelector('#dropS_' + questBattleId).innerHTML = t;
    }
    // if (!txt) {
    //     html_dropItemsNull('', questBattleId, sectionId);
    //     return;
    // }
    txt = txt.trim();
    // if (!txt) {
    //     html_dropItemsNull('', questBattleId, sectionId);
    //     return;
    // }

    var questBattleData = _userQBList[sectionId][questBattleId];
    var sectionData = _userSList[sectionId];

    var miniTitle = questBattle_TitleMini(questBattleData, sectionData)

    if (txt.indexOf('404:') === 0) {
        console.log('无数据：', sectionId, '.', questBattleId);
        txt = '';
        // html_dropItemsNull('[ ' + miniTitle + ' ]', questBattleId, sectionId);
        // return;
    }

    /** 当前掉落列表 */
    var dropRewardCodeList = [];

    /** 初回掉落 */
    var firstClearRewardCode = questBattleData.firstClearRewardCode;
    /** 是否去掉初回掉落的数据 */
    var isFCCodeIgnore = true;
    function checkIsFCCodeIgnore(dropItem) {
        var index = 1;
        while (dropItem['rewardCode' + index]) {
            var code = dropItem['rewardCode' + index];
            if (dropRewardCodeList.indexOf(code) == -1) dropRewardCodeList.push(code);
            if (dropItem['rewardCode' + index] == firstClearRewardCode) {
                isFCCodeIgnore = false;
            }
            index++;
        }
    }
    if (questBattleData.defaultDropItem) {
        checkIsFCCodeIgnore(questBattleData.defaultDropItem);
    }
    var indexItem = 1;
    while (questBattleData['dropItem' + indexItem]) {
        checkIsFCCodeIgnore(questBattleData['dropItem' + indexItem]);
        indexItem++;
    }
    // console.log('是否处理初回掉落：', isFCCodeIgnore);

    /** 忽略掉输数量 */
    var fCCodeIgnoreNum = 0;
    /** 掉落数据 */
    var itemList = [];
    /** 按时间忽略掉的数量 */
    var filterDateNum = 0;

    /** 过滤：时间限制 和 初回掉落处理 */
    function doFilter(date, codeList) {
        /** 过滤这个时间之前的 */
        if (_g_timeStart && _g_timeStart > date) {
            filterDateNum++;
            return true;
        }
        if (_g_timeClose && _g_timeClose < date) {
            filterDateNum++;
            return true;
        }

        // 初回掉落处理
        if (isFCCodeIgnore && codeList.indexOf(firstClearRewardCode) >= 0) {
            fCCodeIgnoreNum++;
            // console.log(codeList);
            codeList[codeList.indexOf(firstClearRewardCode)] = '[FCRC]';
            // console.log(codeList);
            return !true;
        }
    }

    var rows = txt.split('\n');
    // console.log(rows);
    for (var index = 0; index < rows.length; index++) {
        var line = rows[index];
        if (!line) continue;
        line = line.trim();
        if (!line) continue;
        // console.log(line);

        // 格式：  tiem|code|活动？
        var data = line.split('|');
        if (data.length <= 1) {
            console.warn('忽略： ', line);
            continue;
        }

        /** 掉落时间 */
        var date = isNaN(data[0]) ? new Date(data[0]) : new Date(parseInt(data[0]));
        var time = dateToString(date, 'yyyy-MM-dd hh:mm:ss');
        var codeList = data[1].split(',');
        // console.log(time, codeList);

        if (doFilter(date, codeList)) {
            // console.log(codeList);
            continue;
        }

        itemList.push({
            time: time,
            codes: codeList, // 掉落列表
        });
    }
    // console.log('忽略掉输数量:', fCCodeIgnoreNum);
    // console.log('按时间忽略掉的数量:', filterDateNum);
    // console.log(itemList);

    // 掉落处理
    /** 头部 */
    var dropHeader = {};
    // 填充默认掉落信息
    for (var index = 0; index < dropRewardCodeList.length; index++) {
        var rewardCode = dropRewardCodeList[index];
        var t_obj = rewardCode_To_Data(rewardCode, _TYPE_rewardCodeIn.Quest);
        /** 掉落物品 */
        var dropCode = t_obj.name;
        // 头部
        if (dropHeader[dropCode] == undefined) {
            dropHeader[dropCode] = {
                rewardCode: rewardCode
            };
        }
    }
    if (questBattleData.addDropItemId) {
        var rewardCode = questBattleData.addDropItemId;
        var t_obj = rewardCode_To_Data(rewardCode, _TYPE_rewardCodeIn.Quest);
        var dropCode = 'ITEM_' + t_obj.name;
        // 头部
        if (dropHeader[dropCode] == undefined) {
            dropHeader[dropCode] = {
                rewardCode: rewardCode
            };
        }
    }

    for (var index = 0; index < itemList.length; index++) {
        var item = itemList[index];
        // item.rowDrop = [];
        item.drop = {};
        for (var indexCode = 0; indexCode < item.codes.length; indexCode++) {
            var rewardCode = item.codes[indexCode];
            if (rewardCode === '-') continue; // 这次没有掉落
            if (rewardCode === '[FCRC]') {
                var dropCode = '[FCRC]';
                var dropNum = 1;
            }
            else {
                // code 分析数量
                var t_obj = rewardCode_To_Data(rewardCode, _TYPE_rewardCodeIn.Battle);
                if (t_obj == undefined) {
                    console.warn(rewardCode);
                    continue;
                }
                /** 掉落物品 */
                var dropCode = t_obj.name;
                /** 掉落数量 */
                var dropNum = t_obj.dropNum;
                // item.rowDrop.push({ item: dropCode, num: dropNum });
            }

            if (item.drop[dropCode] === undefined) {
                item.drop[dropCode] = dropNum;
            }
            else {
                item.drop[dropCode] += dropNum;
            }

            // 头部
            if (dropCode !== '-' && dropCode !== '[FCRC]' && dropHeader[dropCode] == undefined) {
                dropHeader[dropCode] = {
                    rewardCode: rewardCode,
                };
            }
        }
        // console.log(item.drop);
    }

    // 活动掉落处理
    for (var index = 0; index < itemList.length; index++) {
        var item = itemList[index];
        var dropList = item.drop;
        for (var code in dropList) {
            if (codeRewardCode_isEventKey(code)) {
                var codeDropAdd = code.replace('ITEM_EVENT_', 'EVENTEFFECT_DROPADD_EVENT_');
                // console.log(code, dropList[code], codeDropAdd, dropList[codeDropAdd]);
                if (dropList[codeDropAdd]) {
                    item.drop[code] = (item.drop[code] - item.drop[codeDropAdd]) // 减掉加成
                }
                // console.log(code, dropList[code], codeDropAdd, dropList[codeDropAdd]);
            }
        }
    }
    // console.log(itemList);

    // 导出用的数据库
    if (questBattleId < 9000900) {
        _g_dropDatabase[questBattleId] = {
            itemList: itemList,
            dropHeader: dropHeader,
            // dropDataSet: dropDataSet,
        }
    }

    /** 其他用户掉落 */
    for (var user in _g_dropDatabase_other) {
        if (_g_userNameOtherConfigUserList[user] === false) continue;
        var iLdata = _g_dropDatabase_other[user];
        var userName = iLdata.n;
        if (iLdata.v && iLdata.v < 1) {
            console.log('过期：', user, userName, iLdata);
            continue;
        }
        if (!iLdata.d) continue;
        var iL = iLdata.d[questBattleId] ? iLdata.d[questBattleId].itemList : {};
        for (var i in iL) {
            var oData = iL[i];
            // console.log(oData);
            var date = new Date(oData.time)
            if (doFilter(date, oData.codes)) {
                // console.log(codeList);
                continue;
            }
            oData.user = userName;
            // itemList_other.push(iL[i]);
            itemList.push(oData);
        }

        // 头部
        var dhL = iLdata.d[questBattleId] ? iLdata.d[questBattleId].dropHeader : {};
        if (dhL) {
            for (var keyDropCode in dhL) {
                var item = dhL[keyDropCode];
                if (dropHeader[keyDropCode] === undefined)
                    dropHeader[keyDropCode] = item;
            }
        }
    }

    /** AP */
    var questBattleAp = questBattleData.ap || sectionData.ap || questBattleData.needItemNum || 0;
    // console.log(questBattleAp, questBattleData, sectionData);

    var dropFCCode = _g_dropFCConfig[questBattleId] || false;
    if (dropFCCode) dropFCCode = (rewardCode_To_Data(dropFCCode, _TYPE_rewardCodeIn.Quest) || {}).dropCode;

    /** 整合的掉落表 */
    var dropDataSet = dropHeader;
    for (var code in dropDataSet) {
        if (dropDataSet.hasOwnProperty(code)) {
            var rewardCode = dropDataSet[code].rewardCode;
            var rewardCodeData = rewardCode_To_Data(rewardCode, _TYPE_rewardCodeIn.Battle);

            /** 掉落0的次数 */
            var drop0 = 0;
            /** 最大值 */
            var max = 0;
            /** 掉落总数量 */
            var countNum = 0;
            /** 次数(场数) */
            var countLen = 0;

            /** 手动的初回掉落配置 */
            var doFCRF = false;
            if (isFCCodeIgnore === false && dropFCCode == code) {
                doFCRF = true;
            }
            /** 处理次数 */
            var doFCRFNum = 0;
            /** 降的数据 */
            var doFCRFDropNum = 0;

            for (var index = 0; index < itemList.length; index++) {
                var item = itemList[index];
                // console.log(item);                
                var num = item.drop[code] || 0;
                // console.log('num', num);

                if (index == 0 && doFCRF && item.codes.indexOf(firstClearRewardCode) >= 0) {
                    var t_obj = rewardCode_To_Data(firstClearRewardCode, _TYPE_rewardCodeIn.Quest);
                    num -= t_obj.dropNum;
                    doFCRFNum++;
                    item.doFCRC = code;
                    item.doFCRCNum = (-t_obj.dropNum);
                    item.drop[code] = num;
                    doFCRFDropNum -= t_obj.dropNum;
                }
                else {
                    // doFCRF = false;
                }

                countNum += num;
                countLen++;
                if (num > max) max = num;
                if (num == 0) drop0++;
            }
            dropDataSet[code] = {
                rewardCode: rewardCode,
                code: code,
                drop0: drop0 / countLen,
                dropR: (countLen - drop0) / countLen,
                countNum: countNum,
                countLen: countLen,
                ave: countNum / countLen,
                ap: questBattleAp,
                aveAp: countNum / countLen / questBattleAp,
                max: max,
                doFCRFNum: doFCRFNum,
                doFCRFDropNum: doFCRFDropNum,
            };

            if (codeRewardCode_isEventKey(code, true) || rewardCodeData.isEventExt) {
                /** 模拟 次数(场数) */
                var eventExt_countLen = parseInt(_g_simulatorAP / questBattleAp);
                /** 模拟 活动掉落加成 */
                var eventExt_dropAdd = _g_simulatorDropAdd;
                /** 模拟 有掉落的场数 */
                var eventExt_countDropLen = eventExt_countLen * (1 - (drop0 / countLen));

                /** 有掉落的场数 的 平均掉落 */
                var hasDropAve = 0;
                if ((countLen - drop0) !== 0) {// 可能是0
                    hasDropAve = (countNum / (countLen - drop0));
                }
                /** 模拟 掉落总数量 */
                var eventExt_countNum = eventExt_countDropLen * (eventExt_dropAdd + hasDropAve);
                /** 模拟 平均掉落 */
                var eventExt_ave = eventExt_countNum / eventExt_countLen;
                /** 模拟  */
                var eventExt_aveAp = eventExt_ave / questBattleAp;
                dropDataSet[code].eventExt_countLen = eventExt_countLen;
                dropDataSet[code].eventExt_dropAdd = eventExt_dropAdd;
                dropDataSet[code].eventExt_countDropLen = eventExt_countDropLen;
                dropDataSet[code].hasDropAve = hasDropAve;
                dropDataSet[code].eventExt_countNum = eventExt_countNum;
                dropDataSet[code].eventExt_ave = eventExt_ave;
                dropDataSet[code].eventExt_aveAp = eventExt_aveAp;
                // console.log(dropDataSet[code]);
            }
        }
    }

    // console.log(dropDataSet);

    var divCon = document.querySelector('#dropS_' + questBattleId);
    if (itemList.length == 0) {
        if (filterDateNum) {
            html_dropItemsNull('[ ' + miniTitle + ' , AP: ' + questBattleAp + ' ]（按时间忽略数量：' + filterDateNum + '）', questBattleId, sectionId);
        }
        else {
            if (isFCCodeIgnore && fCCodeIgnoreNum) {
                html_dropItemsNull('[ ' + miniTitle + ' , AP: ' + questBattleAp + ' ]（初回掉落忽略数量：' + filterDateNum + '）', questBattleId, sectionId);
            }
            else {
                html_dropItemsNull('[ ' + miniTitle + ' , AP: ' + questBattleAp + ' ]', questBattleId, sectionId);
            }
        }
        return;
    }
    else
        divCon.innerText = '';

    var title = section_Title(sectionData) + ' - ' + questBattle_Title(questBattleData, sectionData);;
    var questBattle = {
        isFCCodeIgnore: isFCCodeIgnore,
        fCCodeIgnoreNum: fCCodeIgnoreNum,
        filterDateNum: filterDateNum,
        firstClearRewardCode: firstClearRewardCode,
        title: title,
        miniTitle: miniTitle,
        ap: questBattleAp,
        dropHeader: dropHeader,
        dropRewardCodeList: dropRewardCodeList,

        questBattleData: questBattleData,
        sectionData: sectionData,
    };

    // console.log(divCon);
    var div = createHtmlStr_dropTable(itemList, dropDataSet, questBattle);
    if (!div) return;
    var dataSet = {
        itemList: itemList,
        dropDataSet: dropDataSet,
        questBattle: questBattle
    }
    // console.log(dataSet);
    div.dataset.id = questBattleId;
    div.dataDropList = dataSet;
    div.dataset.d = 'dataSet';
    divCon.dataDropList = dataSet;
    divCon.dataset.d = 'dataSet';
    divCon.appendChild(div);
}
function createHtmlStr_dropTable(dropList, dropDataSet, questBattle) {

    if (dropList === undefined) {
        var html = _templates_dropTable.trim();
        html = html.replace('{{filterDateNum}}', '');
        html = html.replace('{{fCCodeIgnoreNum}}', '');
        var div = document.createElement('div');
        div.innerHTML = html;
        div = div.querySelector('table');
        var con = document.querySelector('#MainContentTableHeader');
        con.innerHTML = div.outerHTML;
        return div;
    }

    var html = _templates_dropTable.trim();

    html = html.replace(/\{\{sectionId\}\}/g, questBattle.questBattleData.sectionId);
    html = html.replace(/\{\{questBattleId\}\}/g, questBattle.questBattleData.questBattleId);

    html = html.replace('{{miniTitle}}', questBattle.miniTitle);
    html = html.replace('{{title}}', questBattle.title);
    html = html.replace('{{ap}}', questBattle.ap);

    var filterDateNum = questBattle.filterDateNum;
    html = html.replace('{{filterDateNum}}', filterDateNum ? ('（按时间忽略数量: ' + filterDateNum + '）') : '');

    if (questBattle.isFCCodeIgnore) {
        var fCCodeIgnoreNum = questBattle.fCCodeIgnoreNum;
        html = html.replace('{{fCCodeIgnoreNum}}', fCCodeIgnoreNum ? ('（初回掉落忽略: ' + fCCodeIgnoreNum + '）') : '');
    }
    else {
        if (questBattle.firstClearRewardCode) {
            // 存在
            // html = html.replace('{{fCCodeIgnoreNum}}', '（不忽略初回掉落）');
            html = html.replace('{{fCCodeIgnoreNum}}', '');
        }
        else {
            html = html.replace('{{fCCodeIgnoreNum}}', '');
        }
    }

    var div = document.createElement('div');
    div.innerHTML = html;
    div = div.children[0];

    var divCon = div.querySelector(_idContentDiv);
    if (divCon) {
        for (var key in dropDataSet) {
            if (dropDataSet.hasOwnProperty(key)) {
                var item = dropDataSet[key];
                // console.log(key);
                try {
                    var divRow = createHtmlStr_dropTable_Tr(item, dropList, questBattle);
                    // console.log(divRow);
                    divCon.innerHTML += divRow;
                } catch (error) {
                    console.warn(error);
                }
            }
        }
        // console.log(divCon);
    }

    return div;
}
var _g_eventExt_aveAp_Max = 1;
function createHtmlStr_dropTable_Tr(dropItem, dropRow, questBattle) {
    // console.log(dropItem, dropRow, questBattle);
    var html = _templates_dropTableTr.trim();

    var code = dropItem.code;

    var img = createHtmlStr_dropItemImg(dropItem.rewardCode, _TYPE_rewardCodeIn.Battle, false, true, true)
    var codeTxt = code;
    if (code.indexOf('ITEM_EVENT_') === 0) codeTxt = code.replace('ITEM_EVENT_', '(活动)');
    /** 掉落加成 */
    var isEventDropAdd_DROPADD = false;
    if (code.indexOf('EVENTEFFECT_DROPADD_EVENT_') === 0) {
        codeTxt = '<i>' + code.replace('EVENTEFFECT_DROPADD_EVENT_', '(加成)') + '</i>';
        isEventDropAdd_DROPADD = true;
    }

    var questBattleId = questBattle.questBattleData.questBattleId;

    html = html.replace(/\{\{sectionId\}\}/g, questBattle.questBattleData.sectionId);
    html = html.replace(/\{\{questBattleId\}\}/g, questBattleId);

    html = html.replace('{{qBattle}}', questBattle.miniTitle);
    html = html.replace('{{ap}}', questBattle.ap);

    html = html.replace('{{code}}', codeTxt);
    html = html.replace('{{codeImg}}', (img ? img : ''));

    html = html.replace('{{isRiche}}', (code.indexOf('RICHE') >= 0 ? 'isRiche' : ''));


    /** 活动掉落的道具（支持加成那种） */
    var isEventDropItem = codeRewardCode_isEventKey(code);
    if (questBattle.sectionData && questBattle.sectionData.eventId) {
        if (code.indexOf('ITEM_EVENT_BRANCH_' + questBattle.sectionData.eventId + '_CHARA') === 0) {
            isEventDropItem = false;
        }
    }

    /** 是不是活动道具 */
    var isEventDrop = (code.indexOf('_EVENT_') > 0);
    html = html.replace('{{isEvent}}', isEventDrop ? 'isEvent' : '');
    html = html.replace('{{isEventAdd}}', (isEventDropItem || isEventDropAdd_DROPADD ? 'isEventAdd' : '') + (isEventDropAdd_DROPADD ? ' isEventAddDROP' : ''));

    var drop0 = dropItem.drop0;
    if (drop0 != 0) {
        drop0 = parseInt(drop0 * 100); // 转百分百
    }

    var dropR = dropItem.dropR;
    if (dropR != 0) dropR = parseInt(dropR * 100);

    var ave = dropItem.ave;
    if (ave % 1 !== 0) {
        ave = parseInt(ave * 100) / 100
    }

    var aveAp = dropItem.aveAp;
    if (aveAp % 1 !== 0) {
        aveAp = parseInt(aveAp * 1000) / 1000
    }

    var html_eventExt_countLen = '';
    var html_eventExt_ave = '';
    /** 总AP */
    var html_eventExt_countAp = '';
    var html_eventExt_aveAp = '';

    var rewardCodeData = rewardCode_To_Data(dropItem.rewardCode, _TYPE_rewardCodeIn.Battle)

    if (codeRewardCode_isEventKey(code, true) || rewardCodeData.isEventExt) {
        html_eventExt_countLen = '<span class="eventExt" title="模拟场数">' + dropItem.eventExt_countLen + '</span>'
        if (drop0 > 0) {
            html_eventExt_countLen += '/<span class="eventExt has-drop" title="模拟：有掉落的场数">'
                + parseInt(dropItem.eventExt_countDropLen) + '</span>';
        }

        html_eventExt_countAp = (dropItem.eventExt_countLen * dropItem.ap);// 总AP

        var eventExt_ave = dropItem.eventExt_ave;
        if (eventExt_ave % 1 !== 0) eventExt_ave = parseInt(eventExt_ave * 100) / 100
        html_eventExt_ave = '<span class="eventExt" title="模拟">' + eventExt_ave + '</span>';

        var eventExt_aveAp = dropItem.eventExt_aveAp;
        if (eventExt_aveAp % 1 !== 0) eventExt_aveAp = parseInt(eventExt_aveAp * 1000) / 1000
        html_eventExt_aveAp = '<span class="eventExt" title="模拟： ' + dropItem.eventExt_aveAp
            + '\r\n总掉落数： ' + dropItem.eventExt_countNum
            + '\r\n有掉落场数： ' + (dropItem.eventExt_countDropLen)
            + '\r\n单场AP： ' + (dropItem.ap)
            + '\r\n实际总AP： ' + (dropItem.eventExt_countLen * dropItem.ap)
            + '\r\n模拟加成： ' + dropItem.eventExt_dropAdd
            + '">' + eventExt_aveAp + '</span>';
    }
    html = html.replace('{{ee-len}}', html_eventExt_countLen);
    html = html.replace('{{ee-ap}}', html_eventExt_countAp);
    html = html.replace('{{ee-ave}}', html_eventExt_ave);
    html = html.replace('{{ee-aveap}}', html_eventExt_aveAp);
    var isMax = false;
    if (parseInt(dropItem.eventExt_aveAp * 1000) >= parseInt(_g_eventExt_aveAp_Max * 1000)) {
        isMax = true;
        _g_eventExt_aveAp_Max = dropItem.eventExt_aveAp;
    }
    html = html.replace('{{ee-aveap-isMax}}', isMax ? 'max' : '');

    html = html.replace('{{drop0}}', isEventDropAdd_DROPADD ? '-' : (drop0 == 0 ? (drop0) : (drop0 + '%')));
    html = html.replace('{{drop0-zero}}', isEventDropAdd_DROPADD ? '' : (drop0 == 0 ? 'zero' : ''));
    html = html.replace('{{drop0-one}}', isEventDropAdd_DROPADD ? '' : (drop0 == 100 ? 'one' : ''));

    html = html.replace('{{dropR}}', isEventDropAdd_DROPADD ? '-' : (dropR == 0 ? (dropR) : (dropR + '%')));
    html = html.replace('{{dropR-zero}}', isEventDropAdd_DROPADD ? '' : (dropR == 0 ? 'zero' : ''));
    html = html.replace('{{dropR-one}}', isEventDropAdd_DROPADD ? '' : (dropR == 100 ? 'one' : ''));

    html = html.replace('{{t-ave}}', dropItem.ave);
    html = html.replace('{{ave}}', isEventDropAdd_DROPADD ? '-' : (ave));
    html = html.replace('{{ave-good}}', isEventDrop || isEventDropAdd_DROPADD ? '' : (ave >= 1 ? 'good' : ''));
    html = html.replace('{{t-aveAp}}', dropItem.aveAp);
    html = html.replace('{{aveAp}}', isEventDropAdd_DROPADD ? '-' : (aveAp));
    html = html.replace('{{countLen}}', isEventDropAdd_DROPADD ? '-' : (dropItem.countLen));




    /** 最大值 */
    var maxValue = dropItem.max;
    /** 当前的掉落物品是否和 初回掉落 的相同 */
    var isCodeFCRC = false;
    if (questBattle.firstClearRewardCode) {
        var t_obj = rewardCode_To_Data(questBattle.firstClearRewardCode, _TYPE_rewardCodeIn.Quest);
        if (t_obj && t_obj.name == code) isCodeFCRC = true;
    }

    var htmlDateList = '';

    // 掉落详细
    var maxLen = 50;
    var len = (dropRow.length - maxLen) > 0 ? (dropRow.length - maxLen) : 0;
    for (var index = dropRow.length - 1; index >= len; index--) {
        var item = dropRow[index];
        // console.log(item);
        var htmlDate = _templates_dropTable_Data.trim();
        var num = (item.drop[code]) ? (item.drop[code]) : 0;
        htmlDate = htmlDate.replace('{{num}}', num);
        htmlDate = htmlDate.replace('{{is-max}}', ((num > 3 && num >= maxValue) ? 'max' : ''));
        htmlDate = htmlDate.replace('{{is-zero}}', ((num == 0) ? 'zero' : ''));
        htmlDate = htmlDate.replace('{{time}}', item.time);
        htmlDate = htmlDate.replace('{{is-user}}', (item.user ? 'user' : ''));
        htmlDate = htmlDate.replace('{{user}}', (item.user ? ' - ' + item.user : ''));

        var fcrc = item.drop['[FCRC]'] ? (isCodeFCRC ? true : false) : false;
        if (item.doFCRC == code) fcrc = true;
        htmlDate = htmlDate.replace('{{fcrc}}', fcrc ? 'fcrc' : '');
        // doFCRCNum


        var tips = '\r\n';
        if (isEventDrop) {
            for (var indexICodes = 0; indexICodes < item.codes.length; indexICodes++) {
                var rewardCode = item.codes[indexICodes];
                if (rewardCode.indexOf(code + '_') === 0) {
                    tips += '\r\n' + rewardCode;
                }
            }
        }

        if (fcrc) {
            if (item.doFCRC)
                tips += ('\r\n[初回掉落]手动处理：' + (item.doFCRCNum));
            else
                tips += ('\r\n[初回掉落]' + questBattle.firstClearRewardCode);
        }

        htmlDate = htmlDate.replace('{{tips}}', isEventDrop ? tips : '');


        htmlDateList += htmlDate;
    }

    // 初回处理
    if (questBattle.isFCCodeIgnore) { } else {
        if (isCodeFCRC) {
            var htmlDate = _templates_dropTable_DataFC.trim();
            var tips = '不（即物品的统计可能包含初回掉落）';
            var txt = '[ 初 ]';
            var doFCRF = false;
            if (_g_dropFCConfig[questBattleId] == questBattle.firstClearRewardCode) {
                tips = '初回掉落\r\n处理次数：' + dropItem.doFCRFNum + ''
                    + '\r\n扣减数量：' + dropItem.doFCRFDropNum
                    + '\r\n初回掉落物品：' + questBattle.firstClearRewardCode;
                txt = '' + dropItem.doFCRFDropNum;
                doFCRF = true;
            }
            htmlDate = htmlDate.replace('{{doFCRF}}', doFCRF ? 'do' : '');
            htmlDate = htmlDate.replace('{{txt}}', txt);
            htmlDate = htmlDate.replace('{{tips}}', tips);
            htmlDate = htmlDate.replace('{{questBattleId}}', questBattleId);
            htmlDate = htmlDate.replace('{{code}}', questBattle.firstClearRewardCode);
            htmlDateList += htmlDate;
        }
    }

    html = html.replace('{{dropData}}', htmlDateList);

    return html;
}
function onclick_selectDropDataFC(questBattleId, code) {
    _g_dropFCConfig[questBattleId] = _g_dropFCConfig[questBattleId] ? '' : code;
    _config._g_dropFCConfig = _g_dropFCConfig; // 保存...
}



function questTypeToString(type) {
    switch (type) {
        case 'MAIN': return '主线';
        case 'SUB': return '支线';
        case 'CHARA': return '角色';
        case 'COMPOSE': return '强化';
        case 'MATERIAL': return '觉醒';
        case 'EVENT_S': return 'EVENT_S';
        case 'DAILYTOWER': return 'DAILYTOWER';
        case 'SINGLERAID': return 'SINGLERAID';
        case 'BRANCH': return 'BRANCH';
        case 'ACCOMPLISH': return 'ACCOMPLISH';

        case 'COMPOSE_EXT': return '強化（整合）';
        case 'MATERIAL_EXT': return '覚醒（整合）';

        default:
            console.warn('未知类型： ', type)
            return type;
    }
}
//#region ToString

function questBattleTypeToString(type) {
    switch (type) {
        case 'NORMAL': return '普通';
        case 'HARD': return '困难';
        case '-': return '-';
        default:
            console.warn('未知类型： ', type)
            return type;
    }
}
function dateToString(date, fmt) {
    try {
        // var date = new Date(dateStr);
        var o = {
            "M+": date.getMonth() + 1, //月份 
            "d+": date.getDate(), //日 
            "h+": date.getHours(), //小时 
            "m+": date.getMinutes(), //分 
            "s+": date.getSeconds(), //秒 
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    } catch (err) {
        return;
    }
}
//#endregion

function show_Section(sid) {
    var div = document.querySelector('#s' + sid);
    var divCon = div.querySelector(_idContentDiv);
    if (divCon.innerText) return;
    var list = _userQBList[sid];
    var div = doQuestBattleList(list);
    if (!div) return;
    divCon.innerText = '';
    divCon.appendChild(div);

    if (_gConfig_showType.EventDropShowOnlyTable) {
        // console.log(sid);
        for (var bid in _userQBList[sid]) {
            show_QuestBattle(sid, bid)
        }
    }
}
/** 折叠/显示  关卡信息数据 */
function showHide_QuestBattle(bid, hide) {
    var div = document.querySelector('#qb' + bid);
    var divCon = div.querySelector('.data');
    // console.log(divCon)
    if (hide === undefined) {
        if (divCon.classList.contains('hide')) {
            divCon.classList.remove('hide');
        } else {
            divCon.classList.add('hide');
        }
    }
    else {
        if (hide == true) {
            divCon.classList.add('hide');
        } else {
            divCon.classList.remove('hide');
        }
    }
}
/** 折叠/显示  关卡信息 的 掉落列表 */
function showHide_QuestBattle_DropList(bid, hide) {
    var div = document.querySelector('#qb' + bid);
    var divCon = div.querySelector('.dropItems');
    // console.log(divCon)
    if (hide === undefined) {
        if (divCon.classList.contains('hide')) {
            divCon.classList.remove('hide');
        } else {
            divCon.classList.add('hide');
        }
    }
    else {
        if (hide == true) {
            divCon.classList.add('hide');
        } else {
            divCon.classList.remove('hide');
        }
    }
}
/** 掉落统计 */
function show_QuestBattle(sid, bid) {
    if (sid == 900090 || sid == 900091) {
        var qb = _userQBList[sid][bid];
        // console.log(qb);
        if (qb.customExtend) {
            (function (list) {
                var listData = {};
                for (var index = 0; index < list.length; index++) {
                    var bid = list[index];
                    listData[bid] = undefined;
                    (function (bid) {
                        xhrGet('./drop/' + bid + '.txt', function (txt) {
                            if (txt.indexOf('404:') === 0) listData[bid] = '';
                            else listData[bid] = txt;

                            // console.log(bid);
                            for (var keyBid in listData) {
                                if (listData[keyBid] === undefined) return;
                            }
                            // console.log(listData);

                            var txt = '';
                            for (var keyBid in listData) {
                                txt += '\r\n' + listData[keyBid];
                            }
                            // console.log(txt);
                            statistics_QuestBattle(txt, qb.questBattleId, qb.sectionId);
                        })
                    })(bid);
                }
            })(qb.customExtend);
        }
    }
    else {
        xhrGet('./drop/' + bid + '.txt', function (txt) {
            // if (txt.indexOf('404:') === 0) {
            //     console.log('无数据：', sid, '->', bid);
            //     return;
            // }
            statistics_QuestBattle(txt, bid, sid);
        });
    }
}


//#region 事件： onclick/onchange
function onclick_selectSection(elem, noHash) {
    // console.log(elem);
    // console.log(arguments)
    if (!elem.dataset.id) return;

    var divCon = elem.parentNode.querySelector(_idContentDiv);
    if (divCon.innerText) {
        if (divCon.classList.contains('hide')) {
            divCon.classList.remove('hide');
            // console.log(divCon);
            var qbL = divCon.querySelectorAll('div.questBattle');
            for (var index = 0; index < qbL.length; index++) {
                var div = qbL[index];
                div.hidden = false;
            }
        } else {
            divCon.classList.add('hide');
            return;
        }
    }

    var id = elem.dataset.id;

    if (!noHash)
        location.hash = 's=' + id;

    show_Section(id);
}

/** 掉落统计 */
function onclick_selectQuestBattle(elem, noHash) {
    // console.log(elem);
    if (!elem.dataset.id) return;

    var sid = elem.dataset.sid;
    var bid = elem.dataset.id;

    if (!noHash)
        location.hash = 's=' + sid + '&b=' + bid;

    showHide_QuestBattle(bid, false);
    show_QuestBattle(sid, bid);
}

/** 显示战斗数据 */
function onclick_selectBattleData(elem) {
    // console.log(elem);
    if (!elem.dataset.id) return;

    var id = elem.dataset.id;
    var sid = elem.dataset.sid;
    xhrGet('./battle/' + id + '.json', function (txt) {
        if (txt) {
            var jsonObj;
            try {
                jsonObj = JSON.parse(txt);
            } catch (err) {
                if (txt.indexOf('404:') === 0) {
                    console.warn('JSON：', txt);
                } else {
                    console.warn('JSON转换失败：', txt);
                    console.warn(err);
                }
                return;
            }
            // console.info('战斗数据 ' + id + ' :', jsonObj);

            log_selectBattleData(jsonObj, _userSList[sid], _userQBList[sid][id])
        }
        else {
            console.warn('获取失败：', txt);
        }
    });
}

function onchange_sectionCollapsible(elem) {
    // var hide = elem.checked;
    console.log(arguments)
    // event.stopPropagation();
    // event.preventDefault()
}

/** 折叠/显示  关卡信息 */
function onclick_hideQuestBattle(elem) {
    var bid = elem.dataset.id;
    showHide_QuestBattle(bid)
}
function onclick_hideDropList(elem) {
    var bid = elem.dataset.id;
    showHide_QuestBattle_DropList(bid);
}

function onchange_hideDropModel(elem) {
    var hide = elem.checked;
    var div = elem.parentElement.parentElement;
    var divCon = div.parentElement.parentElement;
    if (hide) {
        divCon.querySelector('.dropItems').style.display = 'none';
        // divCon.querySelector('.div_footer').appendChild(div);
    }
    else {
        divCon.querySelector('.dropItems').style.display = '';
        // divCon.querySelector('.div_haeder').appendChild(div);
    }
}


/** 显示全部 */
function onclick_showAll() {
    for (var sid in _userSList) {
        show_Section(sid);
    }
}
function onclick_showAllQuestBattle() {
    for (var sid in _userSList) {
        show_Section(sid);
    }

    for (var sid in _userQBList) {
        for (var bid in _userQBList[sid]) {
            show_QuestBattle(sid, bid)
        }
    }
}


var _g_LSKey_DROP_HideAllButton = 'DROP_hideAllButton';
document.querySelector('#hideButton').checked = localStorage.getItem(_g_LSKey_DROP_HideAllButton) == 'false' ? false : true;
if (document.querySelector('#hideButton').checked == false) document.querySelector('#MainContent').classList.remove('haid-but');
function onchange_hideAllButton(hide) {
    var div = document.querySelector('#MainContent');
    var className = 'haid-but';
    if (!hide)
        div.classList.remove(className);
    else
        div.classList.add(className);
    localStorage.setItem(_g_LSKey_DROP_HideAllButton, hide);
}

function onchange_hideAllDrop(hide) {
    var div = document.querySelector('#MainContent');
    var className = 'haid-data';
    if (!hide)
        div.classList.remove(className);
    else
        div.classList.add(className);
}

var _g_LSKey_DROP_showType = 'DROP_showType';
var _gConfig_showType = {
    'MAIN': true,
    'SUB': true,
    'CHARA': true,
    'COMPOSE': true,
    'MATERIAL': true,
    'COMPOSE_MATERIAL_Today': true, // 强化/觉醒 只显示今天
    'EVENT_S': true,
    'DAILYTOWER': true,
    'BRANCH': true,
    'ACCOMPLISH': true,

    'COMPOSE_EXT': true,
    'MATERIAL_EXT': true,

    'EventDropHide': false,
    'EventDropShowOnly': false,
    'EventDropShowOnlyTable': false,
    'EventDropShowOnlyTableSHH': false,
    'EventDropShowOnlyTableSH': false,
    'EventDropShowOnlyTableSD': false,
    'EventDropShowOnlyTableSN': false,
    'EventDropShowOnlyTableHideSimulator': false,

    'HideDropRiche': false,
    'HideHardBattle': false,

    'HideQuestBattleHeader': false,

    'DropList': true,
}
/** 主线  支线  角色  强化  觉醒  活动 */
function onchange_type(type, show, config) {
    var div = document.querySelector('#MainContent');
    // console.log(type, 'show', show)
    if (type == 'COMPOSE_MATERIAL_Today') {
        var day = new Date(Date.now()).getDay();
        if (day == 0) day = 6;
        if (show) {
            div.classList.add('hideDay');
            div.classList.add('showOnlyToday' + day);
        }
        else {
            div.classList.remove('hideDay');
            div.classList.remove('showOnlyToday' + day);
        }
        if (new Date(Date.now()).getHours() >= 23) {
            day++;
            if (day == 0) day = 6;
            if (show) {
                div.classList.add('showOnlyToday' + day);
            }
            else {
                div.classList.remove('showOnlyToday' + day);
            }
        }
    }
    else if (type.indexOf('EventDrop') == 0 || type.indexOf('Hide') == 0) {
        if (show)
            div.classList.add(type);
        else
            div.classList.remove(type);
        if (type == 'EventDropShowOnlyTable' || type == 'EventDropShowOnlyTableHideSimulator') {
            if (show) {
                document.querySelector('#MainContentTableHeader').classList.add(type);
                // setTimeout(onclick_showAllQuestBattle, 100);
            }
            else
                document.querySelector('#MainContentTableHeader').classList.remove(type);
        }
    }
    else {
        if (show)
            div.classList.remove('hide' + type);
        else
            div.classList.add('hide' + type);
    }
    if (config === true) {
        document.querySelector('#type' + type).checked = show;
    }
    else {
        _gConfig_showType[type] = show;
        localStorage.setItem(_g_LSKey_DROP_showType, JSON.stringify(_gConfig_showType));
    }
}
try {
    var confStr = localStorage.getItem(_g_LSKey_DROP_showType);
    if (confStr) {
        var json = JSON.parse(confStr);
        for (var key in json) {
            _gConfig_showType[key] = json[key];
        }
        var div = document.querySelector('#MainContent');
        for (var key in _gConfig_showType) {
            var show = _gConfig_showType[key];
            if (key == 'COMPOSE_MATERIAL_Today' || key.indexOf('EventDrop') == 0 || key.indexOf('Hide') == 0) {
                onchange_type(key, show, true);
            }
            else if (show == false) {
                onchange_type(key, false, true);
            }
        }
    }
    createHtmlStr_dropTable()
} catch (err) { console.error(err) }

/**  道具 刷选 */
function onclick_selectItemList() {
    // console.log(event)
    if (event.target.nodeName == "IMG") {
        var key = event.target.title;
        // console.log(key);

        var div = document.querySelector('#MainContent');
        div.classList.add('filterMode');

        selectShow_filter_CodeItem(key);

        div.scrollIntoView();
    }
}
function onclick_selectItemListremove() {
    var div = document.querySelector('#MainContent');
    div.classList.remove('filterMode');

}
//#endregion

/** 道具 刷选 */
function selectShow_filter_CodeItem(key) {
    var obj = rewardCode_To_Data(key, _TYPE_rewardCodeIn.Quest);
    // console.log(obj);
    if (obj.idList) {
        for (var sid in _userSList) {
            if (obj.idList[sid]) {
                document.querySelector('#s' + sid).classList.remove('filterMode');
                show_Section(sid);
                var bIdList = obj.idList[sid];
                var uSBL = _userQBList[sid];
                for (var bid in uSBL) {
                    if (bIdList[bid]) {
                        document.querySelector('#qb' + bid).classList.remove('filterMode');
                    }
                    else {
                        document.querySelector('#qb' + bid).classList.add('filterMode');
                        showHide_QuestBattle(bid, true);
                    }
                }
            }
            else {
                document.querySelector('#s' + sid).classList.add('filterMode');
            }
        }
    }
}


//#region 其他用户数据
lib_localStorageConfigInit2(_localStorageConfig_Prefix, '_userName', '');
document.querySelector('#userName').value = _config._userName;
lib_localStorageConfigInit2(_localStorageConfig_Prefix, '_pushServer', '');
document.querySelector('#pushServer').value = _config._pushServer;
/** 发送数据 */
function onclick_sendData(useDomain, elem) {
    if (!_g_dropDatabase) {
        console.warn('没有数据，不发送');
        return;
    }

    /** @type {string} */
    var userName = document.querySelector('#userName').value;
    var userUserName = '';
    var userData = userName.split('@');
    if (userData.length >= 2) {
        userName = userData[0];
        userUserName = userData[1];
    }

    if (/[,'"\\]/.exec(userUserName)) {
        alert('@名字错误！');
        return;
    }
    if (/^[a-zA-Z_0-9]+$/.exec(userName)) {
        // 'ok';
        var userValue = userName;
        if (userUserName) userValue += '@' + userUserName;
        // localStorage.setItem(_g_LSKey_DROP_userName, userValue);
        _config._userName = userValue;
    }
    else {
        alert('名字错误！');
        return;
    }
    var data = {};
    data[userName] = {
        v: 1,
        d: _g_dropDatabase,
        n: userUserName || userName,
    };
    console.log('发送数据：', data);

    var url = '/drop/post';
    var domain = document.querySelector('#pushServer').value.trim();
    elem.disabled = true;
    if (useDomain === true) {
        url = 'http://' + domain + url;
        console.log('发送URL：', url);
    }

    // return;
    xhrPost(url, JSON.stringify(data), function (txt) {
        elem.disabled = false;
        console.log(txt);
        if (txt) {
            if (useDomain === true && txt) {
                _config._pushServer = domain;
            }
            alert(txt);
        }
        else {
            alert('发送失败！');
        }
    });
}

/** 其他用户配置 */
var _g_userNameOtherConfig = '';
var _g_userNameOtherConfigUserList = {};
var _g_LSKey_DROP_userNameOtherConfig = 'DROP_userNameOtherConfig';
/** 保存配置 */
function onclick_saveUserConfig() {
    var userNameList = document.querySelector('#userNameConfig').value;
    if (_g_userNameOtherConfig != userNameList) {
        _g_userNameOtherConfig = userNameList;
        localStorage.setItem(_g_LSKey_DROP_userNameOtherConfig, _g_userNameOtherConfig);
        loadOtherUserData();
    }
}
_g_userNameOtherConfig = localStorage.getItem(_g_LSKey_DROP_userNameOtherConfig);
document.querySelector('#userNameConfig').value = _g_userNameOtherConfig;

setTimeout(function () {
    loadOtherUserData();
}, 1);

/** 加载数据 */
function loadOtherUserData() {
    if (!_g_userNameOtherConfig) return;

    var userList = _g_userNameOtherConfig.split(',');
    _g_userNameOtherConfigUserList = {};
    for (var index = 0; index < userList.length; index++) {
        var userName = userList[index];
        userName = userName.trim();
        if (!userName) continue;
        if (userName[0] == '!' || userName[0] == '！') {
            console.log('停用：', userName);
            _g_userNameOtherConfigUserList[userName.substring(1)] = false;
            continue;
        }
        if (!/^[a-zA-Z_0-9]+$/.exec(userName)) {
            console.log('名字错误：', userName);
            continue;
        }

        _g_userNameOtherConfigUserList[userName] = true;
        (function (ap_userName) {
            xhrGet('dropOther/drop_' + ap_userName + '.json', function (txt) {
                // console.log(txt);
                try {
                    var json = JSON.parse(txt);
                    _g_dropDatabase_other[ap_userName] = json;
                } catch (error) {
                    console.warn('数据处理失败！名字：', ap_userName);
                }
            })
        })(userName)
    }

}

//#region 发送关卡数据
/** 发送关卡数据 */
function onclick_sendDataUBL(elem) {
    var url = '/drop/postUBL';
    var domain = document.querySelector('#pushServer').value.trim();
    elem.disabled = true;
    url = 'http://' + domain + url;
    console.log('发送URL：', url);

    var data = {
        uSectionList: _userSList,
        uQuestBattleList: _userQBList,
        uEventSingleRaidPointList: _uEventSingleRaidPointList,
        uEventBranchPointList: _uEventBranchPointList,
    };
    console.log(data);

    // return;
    xhrPost(url, JSON.stringify(data), function (txt) {
        elem.disabled = false;
        console.log(txt);
        if (txt) {
            _config._pushServer = domain;
            alert(txt);
        }
        else {
            alert('发送失败！');
        }
    });
}
//#endregion
//#endregion




//#region 时间过滤

/** 时间过滤 */
var _g_timeStart = 0;
/** 时间过滤 */
var _g_timeClose = 0;
var _eventTimeDate = {
    '1014': ['2018年2月5日17:00', '2018年2月15日14:59'],
    '1021': ['2018年4月2日16:00', '2018年4月13日14:59'],
}
function onclick_eventTime(eventId) {
    var date = _eventTimeDate[eventId];
    if (date) {
        _textBox_timeStart.value = date[0];
        _textBox_timeClose.value = date[1];
        onclick_timeSelect();
    }
}

var _textBox_timeStart = document.querySelector('#textBox_timeStart');
var _textBox_timeClose = document.querySelector('#textBox_timeClose');
var _textBox_timeAdd = document.querySelector('#textBox_timeAdd'); // 小时 加减 控制（改时区）
var _text_timeStartClose = document.querySelector('#text_timeStartClose');

/** 时间段 过滤 */
function onclick_timeSelect() {
    var s = _textBox_timeStart.value;
    var c = _textBox_timeClose.value;
    /** 小时 加减 控制（改时区） */
    var hAdd = _textBox_timeAdd.value;
    // console.log(s, c)
    if (s) s = s.replace('年', '-').replace('月', '-').replace('日', ' ').replace(/\\/g, '-');
    if (c) c = c.replace('年', '-').replace('月', '-').replace('日', ' ').replace(/\\/g, '-');

    var ds = s ? new Date(s) : 0;
    var dc = c ? new Date(c) : 0;
    // console.log(ds, dc)

    hAdd = isNaN(hAdd) ? 0 : parseInt(hAdd);
    // console.log(hAdd);

    _g_timeStart = (ds == 0 || ds.toString() == 'Invalid Date') ? 0 : new Date(ds.valueOf() + (hAdd * 3600000));
    _g_timeClose = (dc == 0 || dc.toString() == 'Invalid Date') ? 0 : new Date(dc.valueOf() + (hAdd * 3600000));

    if (_g_timeStart === 0 && _g_timeClose === 0)
        _text_timeStartClose.innerText = '限制：无';
    else if (_g_timeStart === 0)
        _text_timeStartClose.innerText = '限制：本地时间：' + dateToString(_g_timeClose, 'yyyy-MM-dd hh:mm:ss') + ' 之前';
    else if (_g_timeClose === 0)
        _text_timeStartClose.innerText = '限制：本地时间：' + dateToString(_g_timeStart, 'yyyy-MM-dd hh:mm:ss') + ' 之后';
    else
        _text_timeStartClose.innerText = '限制：本地时间：' + dateToString(_g_timeStart, 'yyyy-MM-dd hh:mm:ss') + ' 至 ' + dateToString(_g_timeClose, 'yyyy-MM-dd hh:mm:ss') + ' 之间';

    // console.log(_g_timeStart, _g_timeClose)
    saveLocalStorage_timeStartClose();
}
/** 时间段 清除 */
function onclick_timeRemove() {
    _textBox_timeStart.value = '';
    _textBox_timeClose.value = '';
    onclick_timeSelect();
}


var _g_LSKey_DROP_textBox_timeStart = 'DROP_textBox_timeStart';
var _g_LSKey_DROP_textBox_timeClose = 'DROP_textBox_timeClose';
var _g_LSKey_DROP_textBox_timeAdd = 'DROP_textBox_timeAdd';
function saveLocalStorage_timeStartClose() {
    localStorage.setItem(_g_LSKey_DROP_textBox_timeStart, _textBox_timeStart.value);
    localStorage.setItem(_g_LSKey_DROP_textBox_timeClose, _textBox_timeClose.value);
    localStorage.setItem(_g_LSKey_DROP_textBox_timeAdd, _textBox_timeAdd.value);
}
function loadLocalStorage_timeStartClose() {
    var s = localStorage.getItem(_g_LSKey_DROP_textBox_timeStart)
    var c = localStorage.getItem(_g_LSKey_DROP_textBox_timeClose)
    var hAdd = localStorage.getItem(_g_LSKey_DROP_textBox_timeAdd)
    _textBox_timeAdd.value = hAdd;
    if (s !== null && c !== null) {
        _textBox_timeStart.value = s;
        _textBox_timeClose.value = c;
        onclick_timeSelect();
    }
}
loadLocalStorage_timeStartClose();


// ---- 活动刷选(隐藏不匹配的活动) ----
lib_localStorageConfigInit2(_localStorageConfig_Prefix, '_gConfig_filterEventId');
document.querySelector('#id_filterEventId_inputNumber').value = _config._gConfig_filterEventId || '';
filterEventIdRemove(_config._gConfig_filterEventId)
function onclick_filterEventId() {
    _config._gConfig_filterEventId = document.querySelector('#id_filterEventId_inputNumber').value.trim();
    filterEventIdRemove(_config._gConfig_filterEventId)
}
function onclick_filterEventIdRemove() {
    document.querySelector('#id_filterEventId_inputNumber').value = '';
    _config._gConfig_filterEventId = '';
    filterEventIdRemove(_config._gConfig_filterEventId)
}
function filterEventIdRemove(v) {
    if (v && !isNaN(v)) {
        document.querySelector('#id_style_eventFilter').innerText = '#MainContent .section:not(.notEvent):not(.event' + parseInt(v) + '){ display: none; }';
    }
    else {
        document.querySelector('#id_style_eventFilter').innerText = '';
    }

}


//#endregion

//#region 模拟掉落
var _g_simulatorAP = 500;
var _g_simulatorDropAdd = 0;
var _textBox_simulatorAP = document.querySelector('#textBox_simulatorAP');
var _textBox_simulatorDropAdd = document.querySelector('#textBox_simulatorDropAdd');
function onclick_eventSimulator() {
    var ap = _textBox_simulatorAP.value;
    var drop = _textBox_simulatorDropAdd.value;

    if (ap && !isNaN(ap)) _g_simulatorAP = parseInt(ap);
    if (drop && !isNaN(drop)) _g_simulatorDropAdd = parseInt(drop);

    _textBox_simulatorAP.value = _g_simulatorAP;
    _textBox_simulatorDropAdd.value = _g_simulatorDropAdd;

    saveLocalStorage_simulator();
}

var _g_LSKey_DROP_textBox_simulatorAP = 'DROP_textBox_simulatorAP';
var _g_LSKey_DROP_textBox_simulatorDropAdd = 'DROP_textBox_simulatorDropAdd';
function saveLocalStorage_simulator() {
    localStorage.setItem(_g_LSKey_DROP_textBox_simulatorAP, _textBox_simulatorAP.value);
    localStorage.setItem(_g_LSKey_DROP_textBox_simulatorDropAdd, _textBox_simulatorDropAdd.value);
}
function loadLocalStorage_simulator() {
    var s = localStorage.getItem(_g_LSKey_DROP_textBox_simulatorAP)
    var c = localStorage.getItem(_g_LSKey_DROP_textBox_simulatorDropAdd)
    if (s !== null && c !== null) {
        _textBox_simulatorAP.value = s;
        _textBox_simulatorDropAdd.value = c;
    }
    onclick_eventSimulator();
}
loadLocalStorage_simulator();
//#endregion

//#region 小图片模式
/** 小图片模式 */
function onchange_codeImgMini(mini, init) {
    // console.log(mini, init);
    var body = document.querySelector('body');
    if (mini) {
        body.classList.add('rewardCodeImgMini');
    } else {
        body.classList.remove('rewardCodeImgMini');
    }
    if (init) {
        document.querySelector('#checkbox_codeImgMini').checked = mini;
    } else {
        _config._gConfig_codeImgMini = mini;
    }
}
onchange_codeImgMini(lib_localStorageConfigInit2(_localStorageConfig_Prefix, '_gConfig_codeImgMini', false), true);


function onchange_codeImgSrc() {
    _config._gConfig_codeImgSrc = document.querySelector('#textBox_codeImgSrc').value.trim();
}
document.querySelector('#textBox_codeImgSrc').value = _config._gConfig_codeImgSrc;


//#endregion




/*
    战斗数据						
*/
function log_selectBattleData(data, sectionData, questBattle) {
    console.clear();
    console.log(['战斗数据 ', data, sectionData, questBattle]);
    var miniTitle = '';
    var questType = sectionData.questType;//:"MAIN"
    switch (questType) {
        case 'MAIN':
            miniTitle = 'M ' + (sectionData.genericId - 10) + '' + sectionData.genericIndex + '' + questBattle.sectionIndex;
            break;
        case 'SUB':
            miniTitle = 'S ' + (sectionData.genericId - 50) + '' + sectionData.genericIndex + '' + questBattle.sectionIndex;
            break;
        case 'CHARA':
            miniTitle = sectionData.charaName + ' (' + sectionData.genericIndex + '-' + questBattle.sectionIndex; ')';
            break;
        default: break;
    }
    console.log('\r\n', questBattle.questBattleId, data.battleType,
        (sectionData.title ? sectionData.title : ''), questBattle_TitleMini(questBattle, sectionData),
        '开启时间', sectionData.openDate);

    var ap = questBattle.ap || sectionData.ap || questBattle.needItemNum || 0;
    var exp = questBattle.exp;
    var difficulty = questBattle.difficulty || sectionData.difficulty;
    console.log('难度', difficulty, 'AP', ap, 'EXP', exp,
        '\r\n任务: ',
        '\r\n    ', questBattle['missionMaster1']['description'],
        '\r\n    ', questBattle['missionMaster2']['description'],
        '\r\n    ', questBattle['missionMaster3']['description'],
        '\r\n3星奖励: ', '\r\n    ', questBattle.missionRewardCode);



    var cardAttribute = { FIRE: "火", WATER: "水", TIMBER: "木", LIGHT: "光", DARK: "暗" };
    var memoriaList = {};// = data.memoriaList;
    for (var index = 0; index < data.memoriaList.length; index++) {
        var d = data.memoriaList[index];
        memoriaList[d.memoriaId] = d;
    }
    // console.log(memoriaList);
    var artList = {};
    for (var index = 0; index < data.artList.length; index++) {
        var d = data.artList[index];
        artList[d.artId] = d;
    }
    // console.log(artList);
    var magiaList = {};
    for (var index = 0; index < data.magiaList.length; index++) {
        var d = data.magiaList[index];
        magiaList[d.magiaId] = d;
    }
    // console.log(magiaList);


    var css_D = 'color:#666';
    var css_N = 'color:#1C00CF';
    var css_NB = 'color:#1C00CF; font-weight: bold;';
    var css_S = 'color:#ff5722;';
    var css_S2 = 'color:#e53935;';

    var waveList = data.waveList;
    for (var index = 0; index < waveList.length; index++) {
        console.log('\r\nwave' + (index + 1));
        var wave = waveList[index];
        var enemyList = wave.enemyList;
        // console.log(enemyList);
        var showMemoriaId = [];
        for (var indexEL = 0; indexEL < enemyList.length; indexEL++) {
            var enemy = enemyList[indexEL];
            // console.log(enemy);
            console.log('%c[%d(%d)] %c%s%c  属性: %c%s%c  HP: %c%s%c  攻: %c%s%c  防: %c%s'
                , css_D
                , indexEL, enemy.pos
                , css_S2, enemy.name, css_D
                , css_S, cardAttribute[enemy.align], css_D
                , css_N, enemy.hp, css_D
                , css_N, enemy.attack, css_D
                , css_N, enemy.defence
            );
            if (enemy.magiaId) {
                var magia = magiaList[enemy.magiaId];
                // console.log('   magia:  %d  '
                //     ,magia.magiaId 
                //     ,magia.name 
                //     ,magia.description
                //     );
                console.log('   magia:', magia.magiaId, magia.name, ':', magia.description);
            }
            if (enemy.memoriaList.length) {
                var enemyMemoriaList = enemy.memoriaList;
                // console.log('    技能:', enemyMemoriaList);
                for (var indexML = 0; indexML < enemyMemoriaList.length; indexML++) {
                    var memoriaId = enemyMemoriaList[indexML];
                    var memoria = memoriaList[memoriaId];
                    var memoria_Str = memoria.name + '（' + memoria.description + '）';
                    var isShowD = (showMemoriaId.indexOf(memoriaId) >= 0);
                    if (isShowD) {
                        console.log('    %c%d%c(已显示,详细略)  %c%s%c   '
                            , css_N, memoriaId, css_D
                            , css_D, memoria.name, css_D
                        );
                        // console.log('  ', memoriaId, memoria.name, '(详细同' + memoriaId + ')');
                        continue;
                    }
                    else {
                        console.log('    %c%d%c  %c%s%c %c(%s)%c  :  CD回合: %c%d%c  Lv: %c%d%c '
                            , css_N, memoriaId, css_D
                            , '', memoria.name, css_D
                            , css_D, memoria.description, css_D
                            , css_NB, memoria.cost, css_D
                            , css_N, memoria.level, css_D
                        );
                    }
                    showMemoriaId.push(memoriaId);
                    for (var indexAL = 0; indexAL < memoria.artList.length; indexAL++) {
                        var artId = memoria.artList[indexAL];
                        var art = artList[artId];
                        var artInfoKey = {
                            'artId': 'artId',

                            'target': '目标',
                            'code': '类型',
                            'sub': '效果',
                            'effect': '效果值',
                            'growPoint': '成长值',
                            'turn': '持续回合',
                            'rate': '概率',
                        };
                        // 100904 目标: SELF 类型: CONDITION_GOOD 效果: DEFENSE_IGNORED   概率: 100% 持续回合: 1
                        console.log('      %c%s%c  %c%s%c: %c%s%c  %c%s%c: %c%s%c  %c%s%c: %c%s%c  %c%s%c: %c%s%c  %c%s%c: %c%s%c  %c%s%c: %c%s%c'
                            , css_N, stringPadRight(art['artId'], 6), css_D
                            , css_D, artInfoKey['target'], css_D, css_S, stringPadRight(art['target'], 4), css_D
                            , css_D, artInfoKey['code'], css_D, css_S, stringPadRight((art['code'] || '-'), 15), css_D
                            , css_D, artInfoKey['sub'], css_D, css_S, stringPadRight((art['sub'] || '-'), 15), css_D
                            , css_D, artInfoKey['effect'], css_D, css_N, stringPadRight((art['effect'] || '-'), 4), css_D
                            , css_D, artInfoKey['rate'], css_D, css_N, stringPadRight((art['rate'] ? (art['rate'] / 10) + '%' : '-'), 4), css_D
                            , css_D, artInfoKey['turn'], css_D, css_N, (art['turn'] || '-'), css_D
                        );
                        // console.log('      ', art['artId'],
                        //     artInfoKey['target'] + ':', art['target'],
                        //     artInfoKey['code'] + ':', art['code'],
                        //     artInfoKey['sub'] + ':', art['sub'],
                        //     (art['effect'] ? (artInfoKey['effect'] + ':') : ''), (art['effect'] ? art['effect'] : ''),
                        //     (art['rate'] ? (artInfoKey['rate'] + ':') : ''), (art['rate'] ? (art['rate'] / 10) + '%' : ''),
                        //     // artInfoKey['growPoint'] + ':', art['growPoint'],
                        //     (art['turn'] ? (artInfoKey['turn'] + ':') : ''), (art['turn'] ? art['turn'] : '')
                        //     // art
                        // );
                        for (var key in art) {
                            var item = art[key];
                            if (artInfoKey[key]) {
                                continue;
                            }
                            console.log('          [未解析参数]', key, item)
                        }
                    }
                }
            }
        }
    }
}

function stringPadRight(txt, len) {
    txt = '' + txt;
    if (txt.length >= len) {
        return txt;
    }
    else {
        for (var index = txt.length; index < len; index++) {
            txt += ' ';
        }
        return txt;
    }
}

/*
var l = _userSList_event[1019];
for(var i in l){
    var item = l[i];
    console.log(item.genericIndex==1?'剧情： ':'高难： ', item.charaName,' : ',item.message);
}
*/