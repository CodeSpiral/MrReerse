var _TBL = {
    'artId': ' ',
    'targetId': '目标',
    'verbCode': '类型',
    'effectCode': '效果',
    'effectValue': '效果值',
    'probability': '几率',
    'enableTurn': '回合',
    'growPoint': '成长',
    'parameter': '参数',
    'limitedValue': '限制',
    'genericValue': 'generic值',
};
var _TBL_NotShow = {
    'parameter': false, // false 表示不显示
    'limitedValue': false,
    'genericValue': false,
};
var _TBL_EVENT = {
    'artId': ' ',
    'verbCode': '类型',
    'effectCode': '效果',
    'effectValue': '效果值',
    'genericValue': 'generic值',
};
var _artInfo_effectCode = {};
var _artInfo_verbCode = {};
function CreateHtmlArtInfo(artListObj, log_type, log_name) {
    if (log_type !== '活动') {
        var isEvevt = false;
        var cell = _TBL;
        /** 判断是否需要显示 */
        var cell_NotShow = JSON.parse(JSON.stringify(_TBL_NotShow));
        var keyName = 'art';
    }
    else {
        var isEvevt = true;
        var cell = _TBL_EVENT;
        var cell_NotShow = JSON.parse(JSON.stringify(_TBL_NotShow));
        var keyName = 'eventArt';
    }

    var _l = (typeof SkillArt_targetId == 'function');
    var isHas = false;
    var index = 1;
    var htmlDisc = {};
    while (artListObj[keyName + index]) {
        isHas = true;
        var art = artListObj[keyName + (index++)];
        // console.log(art);
        for (var key in cell) {
            if (cell_NotShow[key] === false && art[key]) {
                cell_NotShow[key] = true;
            }

            // LOG查看用
            if (key == 'effectCode') _artInfo_effectCode[art[key]] ? (_artInfo_effectCode[art[key]]++) : (_artInfo_effectCode[art[key]] = 1);
            if (key == 'verbCode') _artInfo_verbCode[art[key]] ? (_artInfo_verbCode[art[key]]++) : (_artInfo_verbCode[art[key]] = 1);

            var html = '';
            switch (key) {
                case 'artId':
                    html = '<td class="' + key + '" title="' + art[key] + '"> · </td>';
                    break;

                case 'targetId':
                    html = '<td class="' + key + '" title="' + (art[key] || '') + '">'
                        + (_l ? SkillArt_targetId(art[key]) : art[key]) + '</td>';
                    break;

                case 'effectCode':
                    html = '<td class="' + key + '" title="' + (art[key] || '') + '">'
                        + (_l ? SkillArt_effectCode(art[key], art['verbCode']) : art['verbCode']) + '</td>';
                    break;


                case 'growPoint':
                case 'probability':
                    html = '<td class="' + key + '">' + (art[key] === 0 ? 0 : (!art[key] ? '' : (art[key] / 10) + '%')) + '</td>';
                    break;

                case 'effectValue':
                    if (isEvevt)
                        html = '<td class="' + key + '">' + (art[key] == undefined ? '' : (art[key])) + '</td>';
                    else
                        html = '<td class="' + key + '">' + (art[key] == undefined ? '' : (art[key] / 10) + '%') + '</td>';
                    break;

                case 'limitedValue':
                    var limitedValue = art[key];
                    if (limitedValue && _charaList[limitedValue]) {
                        let chara = _charaList[limitedValue];
                        let name = GetZhName(chara.name + (chara.title ? ' ' + chara.title : ''))
                        html = '<td class="' + key + '" title="' + art[key] + '">' + (name) + '</td>';
                        break;
                    }

                default:
                    html = '<td class="' + key + '">' + (art[key] == undefined ? '' : art[key]) + '</td>';
                    break;
            }
            if (htmlDisc[index] === undefined) htmlDisc[index] = {};
            htmlDisc[index][key] = html;
        }

        // 检查用的
        for (var key in art) {
            if (art.hasOwnProperty(key)) {
                if (!cell[key]) console.log('新值：', log_type, key, log_name, art);
            }
        }
    }

    if (!isHas) return '';

    var html = '<tr>';
    for (var key in cell) {
        if (cell_NotShow[key] === false) continue;
        html += '<th class="' + key + '" >' + (cell[key] || key) + '</th>';
    }
    html += '</tr>';

    for (var index in htmlDisc) {
        html += '<tr>';
        var item = htmlDisc[index];
        for (var key in cell) {
            if (cell_NotShow[key] === false) continue;
            html += (item[key] || '');
        }
        html += '</tr>';
    }

    html = '<table class="' + (isEvevt ? 'event' : '') + '">' + html + '</table>';
    return html;
}


function Log_artInfo_effectCodeVerbCode() {
    console.log(_artInfo_verbCode)
    console.log(_artInfo_effectCode)
    var vc = Object.keys(_artInfo_verbCode);
    var txt = 'verbCode:\r\n';
    for (var index = 0; index < vc.length; index++) {
        var item = vc[index];
        txt += `${item}\r\n`;
    }

    txt += '\r\n';

    var ec = Object.keys(_artInfo_effectCode);
    txt += 'effectCode:\r\n';
    for (var index = 0; index < ec.length; index++) {
        var item = ec[index];
        txt += `${item}\r\n`;
    }
    console.log(txt);
}