//#region xhr
/**
 * 获取
 * @param {*} url 
 * @param {*} cb 
 */
function lib_xhrGet(url, cb) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (typeof cb === 'function') {
                cb(xhr.responseText);
            }
        }
    };
    xhr.open('GET', url);
    xhr.send();
}
/**
 * 获取
 * @param {*} url 
 * @param {*} cb  cb(json) 或 cb(txt, err)
 */
function lib_xhrGetJson(url, cb) {
    lib_xhrGet(url, function (txt) {
        if (txt) {
            var jsonObj;
            try { jsonObj = JSON.parse(txt); }
            catch (err) { cb(txt, err); }
            cb(jsonObj, undefined);
        } else {
            cb(undefined, null);
        }
    });
}

/**
 * 
 * @param {*} url 
 * @param {object} postObj ``` object ```
 * @param {*} cb  cb(xhr.responseText, xhr)
 */
function lib_xhrPostJson(url, postObj, cb) {
    var postStr = JSON.stringify(postObj)
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (typeof cb === 'function') {
                cb(xhr.responseText, xhr);
            }
        }
    };
    xhr.open('POST', url);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(postStr);
}

/**
 * @param {*} txt 
 * @return 返回 object | null
 */
function lib_toJson(txt) {
    if (typeof txt === 'string') {
        try { return JSON.parse(txt); }
        catch (err) { return null; }
    } else {
        return null;
    }
}
//#endregion


//#region 模板

function lib_getTemplate(id, not_remove) {
    var t = document.querySelector(id);
    if (!t) return '';
    t = t.innerHTML;
    t = t.replace(/(\[\[(\w+)\]\])/g, '$2');
    if (not_remove != true) {
        document.querySelector(id).remove();
    }
    return t;
}

function lib_template_If2(html, key, value, ifValue) {
    if (ifValue === undefined) ifValue = value;
    var reg = new RegExp('\\[\\[IF\\=' + key + ' {0,}\\|(.+?)\\]\\]', 'g')
    // console.log(reg);
    if (!ifValue) {
        return html.replace(reg, '');
    }
    else {
        html = html.replace(reg, '$1');
        return html.replace(new RegExp('{{' + key + '}}', 'g'), value);
    }
}
// lib_template_If('[[IF=rank|<span class="itemTypeAttr">等级：{{rank}}</span>{{rank}}]]', 'rank', 'xxx', 10);

/** 需要css配合... */
function lib_template_If(html, key, value, ifValue) {
    if (ifValue === undefined) ifValue = value;
    var reg = new RegExp('\\[\\[IF\\|' + key + '\\]\\](\\="")?', 'ig');
    // console.log(reg);
    if (!ifValue) {
        return html.replace(reg, 'hide').replace(new RegExp('{{' + key + '}}', 'g'), '');
    }
    else {
        return html.replace(reg, '').replace(new RegExp('{{' + key + '}}', 'g'), value);
    }
}
// lib_template_If('<span class="itemTypeAttr" [[IF|rank]] >等级：</span>{{rank}}', 'rank', 'xxx', 10);

/**
 * 必须 ``` <img [[IFSHOW|img-src]]  ``` 格式的开头
 * @param {*} key 
 * @param {*} value 如果有值就显示
 */
function lib_template_IfShow(html, key, value) {
    key = key.replace(/\-/g, '\\-')
    var reg = new RegExp('<[a-z]+ \\[\\[IFSHOW\\|' + key + '\\]\\](\\="")?(.*?)\/?>', 'ig');
    // console.log(reg);
    if (value) {
        return html.replace(new RegExp('\\[\\[IFSHOW\\|' + key + '\\]\\]'), '').replace(new RegExp('{{' + key + '}}', 'g'), value);
    }
    else {
        return html.replace(reg, '');
    }
}
// lib_template_IfShow('<img [[IFSHOW|img-src]] [[src]]="gift/{{img-src}}" >asd', 'img-src', 'xxx');



function lib_appendTemplate(elem, html) {
    var div = document.createElement('div');
    div.innerHTML = html;
    div = div.children[0];
    elem.appendChild(div);
}
//#endregion

/**
 * 支持
 * ```input type="text"```
 * @param {*} selector 
 * @param {*} value 
 */
function lib_setElement(selector, value) {
    var elem = document.querySelector(selector);
    if (!elem) return;
    if (elem.type) {
        if (elem.type === "text") {
            elem.value = value;
        }
        else if (elem.type === 'checkbox') {
            elem.checked = (!!value)
        }
    }
}
/**
 * 
 * @param {*} selector  string | Element
 * @param {*} optionsDisc  [ {k:v} , {k:v} ]
 */
function lib_setElementSelectOptions(selector, optionsDisc) {
    var elem;
    if (typeof selector === 'string') {
        elem = document.querySelector(selector);
    }
    else {
        elem = selector;
    }
    elem.options.length = 0; // 清空
    for (var index = 0; index < optionsDisc.length; index++) {
        var item = optionsDisc[index];
        if (item && item.k) {
            elem.options.add(new Option(item.k, item.v || item.k))
        }
        else {
            console.warn('item? : ', item)
        }
    }
}

/**
 * 
 * @param {*} selector string | Element
 * @param {*} o  selectedIndex移动： -1 , 0 , 1
 * @return 返回设置后的```selectedIndex```
 */
function lib_setElementSelectIndex(selector, o) {
    var elem = (typeof selector === 'string') ? document.querySelector(selector) : selector;
    if (!elem.options) return -1;
    if (o === 0) { }
    else if (o === -1) {
        elem.selectedIndex = (elem.selectedIndex == 0) ? (elem.options.length - 1) : elem.selectedIndex--;
    } else {
        elem.selectedIndex = (elem.selectedIndex >= elem.options.length - 1) ? 0 : elem.selectedIndex++;
    }
    return elem.selectedIndex;
}

function dateFormat(date, fmt) { //author: meizz 
    // 对Date的扩展，将 Date 转化为指定格式的String
    // 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
    // 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
    // 例子： 
    // (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
    // (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
    var o = {
        "M+": date.getMonth() + 1, //月份 
        "d+": date.getDate(), //日 
        "h+": date.getHours(), //小时 
        "HH": date.getHours() - 12,//) > 12 ? ('下午 ' + (date.getHours()-12) ) : ('上午 ' + date.getHours()), //小时 
        "m+": date.getMinutes(), //分 
        "s+": date.getSeconds(), //秒 
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度 
        "S": date.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (k == 'HH')
            fmt = fmt.replace('HH', (date.getHours() > 12 ? '下午 ' : '上午 ') + (("00" + o[k]).substr(("" + o[k]).length)));
        else if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}


//#region localStorage Config 方法
/**
 * 注意在全局写 ``` var name = {} ``` 会报错...
 * 
 * 支持： 
 * - boolean
 * - object （注意object修改里面的键值时不会触发保存操作）
 * @param {*} keyNamePrefix 
 * @param {*} keyName 
 * @param {*} defValue 
 */
function lib_localStorageConfigInit(keyNamePrefix, keyName, /** 默认值 */defValue) {
    if (!keyName || typeof keyName !== 'string') {
        console.error('localStorageConfig: ', keyName);
        return;
    }
    try {
        var localStorageKeyName = keyNamePrefix + keyName;
        var s = localStorage.getItem(localStorageKeyName);
        var value = defValue;
        if (s !== null && s !== undefined) {
            if (typeof defValue === 'boolean') {
                value = !!JSON.parse(s);
            } else if (typeof defValue === 'object') {
                value = JSON.parse(s);
            } else {
                value = s;
            }
        }
        Object.defineProperty(window, keyName, {
            enumerable: true,
            get: function () {
                // console.log('get', value);
                return value;
            },
            set: function (v) {
                if (typeof v === 'object') {
                    var vStr = JSON.stringify(v);
                    // console.log('set[object]', vStr);
                    value = v;
                    localStorage.setItem(localStorageKeyName, vStr);
                }
                else {
                    if (v === value) return;
                    // console.log('set', v, '->', value);
                    value = v;
                    localStorage.setItem(localStorageKeyName, v);
                }
            }
        });
        // console.log(window[keyName]);
    } catch (error) {
        console.error('localStorageConfig: ', error);
    }
    return window[keyName];
}

var _config = {};
/**
 * 全局的 ``` var _config = {} ``` 中
 * 
 * 支持： 
 * - boolean
 * - object （注意object修改里面的键值时不会触发保存操作）
 * @param {*} keyNamePrefix 
 * @param {*} keyName 
 * @param {*} defValue 
 */
function lib_localStorageConfigInit2(keyNamePrefix, keyName, /** 默认值 */defValue) {
    if (!keyName || typeof keyName !== 'string') {
        console.error('localStorageConfig: ', keyName);
        return;
    }
    try {
        var localStorageKeyName = keyNamePrefix + keyName;
        var s = localStorage.getItem(localStorageKeyName);
        var value = defValue;
        if (s !== null && s !== undefined) {
            if (typeof defValue === 'boolean') {
                value = !!JSON.parse(s);
            } else if (typeof defValue === 'object') {
                value = JSON.parse(s);
            } else {
                value = s;
            }
        }
        Object.defineProperty(_config, keyName, {
            enumerable: true,
            get: function () {
                // console.log('get', value);
                return value;
            },
            set: function (v) {
                if (typeof v === 'object') {
                    var vStr = JSON.stringify(v);
                    // console.log('set[object]', vStr);
                    value = v;
                    localStorage.setItem(localStorageKeyName, vStr);
                }
                else {
                    if (v === value) return;
                    // console.log('set', v, '->', value);
                    value = v;
                    localStorage.setItem(localStorageKeyName, v);
                }
            }
        });
        // console.log(_config[keyName]);
    } catch (error) {
        console.error('localStorageConfig: ', error);
    }
    return _config[keyName];
}
//#endregion