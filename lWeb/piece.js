var _templates_pickUpDetail = document.querySelector('#templates_pickUpDetail').innerHTML;
var _templates_pieceSkill = document.querySelector('#templates_pieceSkill').innerHTML;
document.querySelector('#templates_pickUpDetail').remove();
var _elem_pieceList = document.querySelector('#pieceList');
var _test_index = undefined;

var _baseSrc = '/resource/';
var _baseSrc_localFile = '../../magica/resource/';

var _localStorage_Prefix = 'PIECE_';

//#region online时配置
var _id_text_baseSrc = '#text_baseSrc';
var _id_checkbox_imgMini = '#checkbox_imgMini';
_config.baseSrc = lib_localStorageConfigInit2(_localStorage_Prefix, 'baseSrc', '');
_config.imgMini = lib_localStorageConfigInit2(_localStorage_Prefix, 'imgMini', true);
if (_config.baseSrc) {
    lib_setElement(_id_text_baseSrc, _config.baseSrc);
    _baseSrc = _config.baseSrc;
}
lib_setElement(_id_checkbox_imgMini, _config.imgMini);
function on_baseSrc() {
    _config.baseSrc = document.querySelector(_id_text_baseSrc).value.trim();
    _config.imgMini = document.querySelector(_id_checkbox_imgMini).checked;
}
//#endregion


var _pieceListData;
function initRun(json) {
    if (json) {
        // console.log(json);
        _pieceListData = json.pieceList;
    }
    else {
        _pieceListData = _pieceList;
    }

    for (var i = _pieceListData.length - 1; i >= 0; i--) {
        var item = _pieceListData[i];

        if (_test_index == undefined)
            _test_index = (item.pieceId - i);
        else {
            if (item.pieceId != (i + _test_index)) {
                console.log('缺：', (i + _test_index));
                _test_index = (item.pieceId - i);
            }
        }

        // console.log(item);
        var html = _templates_pickUpDetail.replace('{{memoriaName}}', item.pieceName);
        html = html.replace(/\{\{pieceId\}\}/g, item.pieceId);

        // if (_miniImg) {
        //     html = html.replace('{{memoriaImageSrc}}', 'memoria_' + item.pieceId + '_s.png');
        // } else {
        //     html = html.replace('{{memoriaImageSrc}}', 'memoria_' + item.pieceId + '_c.png');
        // }

        html = html.replace('{{description}}', item.description.replace(/＠/g, '<br>'));

        if (item.illustrator && item.illustrator.indexOf('：') >= 0)
            html = html.replace('{{illustrator}}', ('' + item.illustrator.replace('　　', '<br>')) || '-');
        else
            html = html.replace('{{illustrator}}', '画师：' + (item.illustrator || '-'));


        html = html.replace('{{ATK}}', item.attack);
        html = html.replace('{{DEF}}', item.defense);
        html = html.replace('{{HP}}', item.hp);

        if (typeof MemoriaUtil === 'object') {
            var maxLv = MemoriaUtil.getMaxLevel(item.rank, 4);
            var maxLvParams = MemoriaUtil.getParam(item, maxLv);

            html = html.replace('{{MAX_ATK}}', maxLvParams.attack);
            html = html.replace('{{MAX_DEF}}', maxLvParams.defense);
            html = html.replace('{{MAX_HP}}', maxLvParams.hp);

            html = html.replace('{{Total}}', (item.attack + item.defense + item.hp));
            html = html.replace('{{MAX_Total}}', (maxLvParams.attack + maxLvParams.defense + maxLvParams.hp));
        }

        var charaListHtml = '';
        if (item.charaList && item.charaList.length > 0) {
            var tlen = item.charaList.length;
            for (var ti = 0; ti < tlen; ti++) {
                if (ti > 0)
                    charaListHtml += ", ";
                charaListHtml += item.charaList[ti].name;
            }

            if (_localMode) {
                html = html.replace('{{charaList}}', GetZhName(charaListHtml));
                html = html.replace('{{charaList_jp}}', charaListHtml.replace(' ', ''));
            }
            else {
                html = html.replace('{{charaList}}', charaListHtml.replace(' ', ''));
                html = html.replace('{{charaList_jp}}', '');
            }

        }
        else {
            html = html.replace('{{charaListNull}}', 'charaListNull');
        }

        var pieceType = item.pieceType.toLowerCase();
        html = html.replace(/\{\{pieceType\}\}/g, pieceType);

        var pieceTypeText = pieceType == 'skill' ? '技能型' : (pieceType == 'ability' ? '能力型' : '不明');
        html = html.replace('{{pieceTypeText}}', pieceTypeText);


        html = html.replace('{{rank}}', item.rank);
        html = html.replace('{{rankInt}}', item.rank[5]);

        // 技能
        /** 技能信息HTML */
        var pieceSkillHtml = '';
        var pieceSkill = item.pieceSkill;
        var pieceSkillIndex = 2;
        while (pieceSkill) {
            var skillHtml = _templates_pieceSkill;
            skillHtml = skillHtml.replace('{{pieceType}}', pieceType);
            skillHtml = skillHtml.replace('{{name}}', pieceSkill.name);
            skillHtml = skillHtml.replace('{{id}}', pieceSkill.id);
            if (pieceSkill.groupId === 0 && pieceSkillIndex - 1 == 2) {
                console.log('[Miss]:groupId', item.pieceName)
                // console.warn(item.pieceName, '[Miss]:groupId', item)
                skillHtml = skillHtml.replace('{{groupId}}', item['pieceSkill'].groupId);
            }
            else {
                skillHtml = skillHtml.replace('{{groupId}}', pieceSkill.groupId);
            }

            skillHtml = skillHtml.replace('{{eventDescription}}', pieceSkill.eventDescription || '');
            skillHtml = skillHtml.replace('{{intervalTurn}}', pieceSkill.intervalTurn === undefined ? '' : ('CD回合：' + pieceSkill.intervalTurn));

            if (_localMode) {
                skillHtml = skillHtml.replace('{{shortDescription}}', pieceSkill.shortDescription);
                skillHtml = skillHtml.replace('{{shortDescription_tran}}', GetZhSkillShortDescription(pieceSkill.shortDescription));

                skillHtml = skillHtml.replace('{{pieceSkill_artHtml}}', CreateHtmlArtInfo(pieceSkill, '', pieceSkill.name));
                skillHtml = skillHtml.replace('{{pieceSkill_artHtml_event}}', CreateHtmlArtInfo(pieceSkill, '活动', pieceSkill.name));
            }
            else {
                skillHtml = skillHtml.replace('{{shortDescription_tran}}', (pieceSkill.shortDescription));

                skillHtml = skillHtml.replace('{{pieceSkill_artHtml}}', CreateHtmlArtInfo(pieceSkill, '', pieceSkill.name));
                skillHtml = skillHtml.replace('{{pieceSkill_artHtml_event}}', CreateHtmlArtInfo(pieceSkill, '活动', pieceSkill.name));
            }


            pieceSkillHtml += skillHtml;

            // 下一个
            pieceSkill = item['pieceSkill' + pieceSkillIndex];
            pieceSkillIndex++;
        }
        html = html.replace('{{pieceSkill}}', pieceSkillHtml);

        var baseSrc = _localMode ? _baseSrc_localFile : _baseSrc;
        html = html.replace(/\{\{baseSrc\}\}/g, baseSrc);

        //#region 输出
        var div = document.createElement('div');
        div.innerHTML = html;
        div = div.children[0];
        div.dataPiece = item;
        _elem_pieceList.appendChild(div);
        //#endregion
    }


    // 加载图片
    var imgElem = document.querySelectorAll('img[data-src]');
    if (imgElem) {
        for (var index = 0; index < imgElem.length; index++) {
            var img = imgElem[index];
            if (img.dataset['src'] && img.dataset['src'].indexOf('{') === -1)
                img.src = img.dataset['src'];
        }
    }
    if (_localMode || _miniImg) {
        imgElem = document.querySelectorAll('img[data-id]');
        if (imgElem) {
            for (var index = 0; index < imgElem.length; index++) {
                var img = imgElem[index];
                if (img.dataset['id']) {
                    if (_localMode)
                        img.src = baseSrc + 'resource/image_native/memoria/memoria_' + img.dataset['id'] + '_c.png';
                    else if (_miniImg)
                        img.src = baseSrc + 'resource/image_native/memoria/memoria_' + img.dataset['id'] + '_s.png';
                }
            }
        }
    }
}



//#region 刷选
/** 隐藏详细信息 */
function pieceList_ShowHide_info(hide) {
    document.querySelector('#pieceList').className = hide ? 'hide-info' : '';
}

var _show_piece_rank = 0;
var _show_piece_pieceType = '';
function select_show_piece_rank(rank) {
    _show_piece_rank = rank;
    select_show_piece();
}

function select_show_piece_pieceType(pieceType) {
    _show_piece_pieceType = pieceType;
    select_show_piece();
    //  ability
    //  skill
}

function select_show_piece() {
    var list = document.querySelectorAll('#pieceList > div');
    list.forEach(function (elem) {
        if (elem.dataset && elem.dataset.rank) {
            var show = true;;
            if (_show_piece_rank != '0')
                show = (elem.dataset.rank == _show_piece_rank);
            if (_show_piece_pieceType != '' && show)
                show = (elem.dataset.piecetype == _show_piece_pieceType);

            elem.style.display = show ? '' : "none";
        }
    });
}
//#endregion

//#region local 处理
/** 打开页面时加载小图片 */
var _miniImg = false;
/** 本地模式 */
var _localMode = (location.protocol == 'file:');
if (_localMode) {
    document.body.classList.add('local');
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'pieceLocal.js';
    document.head.appendChild(script)
}
else {
    _miniImg = _config.imgMini;
    lib_xhrGet('./itemGiftPieceList.json', function (txt) {
        var div = document.querySelector('#pieceList');
        if (txt) {
            var jsonObj;
            try {
                jsonObj = JSON.parse(txt);
            } catch (err) {
                div.innerHTML = '异常<br>' + txt;
            }
            if (jsonObj) initRun(jsonObj);
            else {
                div.innerHTML = '没有内容？<br>' + txt;
            }
        }
        else {
            div.innerText = '获取失败！';
        }
    });
}
//#endregion

if (_miniImg) {
    document.body.classList.add('miniImg');
}
if (_localMode) {
    document.querySelector('.sendServerCon').style.display = '';
    document.querySelector('.baseSrcCon').style.display = 'none';
}


//#region 错误事件:处理找不到的图片
/** 错误事件 */
document.addEventListener("error", function (e) {
    if (!_localMode) return;
    var elem = e.target;
    if (elem.tagName.toLowerCase() == 'img') {
        if (elem.src && elem.src.match(/memoria_[0-9]{4}_c.png/)) {
            console.log(elem);
            _changeImgSrc(elem);
        }
    }
}, true);

var _ImageServerAddress = "http://127.0.0.1:8008";
/** 处理找不到的图片 */
function _changeImgSrc(elem) {
    if (!elem.dataset) return;
    if (elem.dataset.issetget) return;
    elem.dataset.issetget = '1';
    var src = elem.dataset['src'];
    if (src.indexOf("image_web/") >= 0) {
        console.info("WEB", src);
        src = _ImageServerAddress + '/' + src;  // 连接 文件获取服务器
        // console.info(src);
        // elem.src = src;
    }
    else if (src.indexOf("image_native/") >= 0) {
        console.info("本地", src);
        src = _ImageServerAddress + '/' + src;
        // console.info(src);
        // elem.src = src;
    }
    else console.warn("不明", src);
}
//#endregion

